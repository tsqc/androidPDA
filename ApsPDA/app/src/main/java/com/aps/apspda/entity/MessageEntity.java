package com.aps.apspda.entity;

import com.aps.apspda.utils.AppUtils;

import java.io.Serializable;

/**
 * @author lx
 * @date 2018/4/10
 * @desc
 */

public class MessageEntity implements Serializable {

    /**
     * MESSAGE : 08:44:231FS08{Info:1FS08SECS通讯状态为断开!
     * EQPID : 1FS08
     * ISEQPSTATUS : false
     * msgId : ID:xxd-glory-59578-1523850276372-5:1:1:1:39
     * ISALERT : false
     * COLOR : red
     */

    private Object MESSAGE;
    private Object EQPID;
    private Object ISEQPSTATUS;
    private Object msgId;
    private Object ISALERT;
    private Object COLOR;
    private Object EQPSTATUS;

    public String getEQPSTATUS() {
        return EQPSTATUS == null ? "" : String.valueOf(EQPSTATUS);
    }

    public void setEQPSTATUS(Object EQPSTATUS) {
        this.EQPSTATUS = EQPSTATUS;
    }

    public String getMESSAGE() {
        return MESSAGE == null ? "" : String.valueOf(MESSAGE);
    }

    public void setMESSAGE(Object MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public String getEQPID() {
        return AppUtils.getFormatStr(EQPID);
    }

    public void setEQPID(Object EQPID) {
        this.EQPID = EQPID;
    }

    public boolean getISEQPSTATUS() {
        return ISEQPSTATUS != null && (Boolean) ISEQPSTATUS;
    }

    public void setISEQPSTATUS(Object ISEQPSTATUS) {
        this.ISEQPSTATUS = ISEQPSTATUS;
    }

    public String getMsgId() {
        return AppUtils.getFormatStr(msgId);
    }

    public void setMsgId(Object msgId) {
        this.msgId = msgId;
    }

    public boolean getISALERT() {
        return ISALERT != null && (Boolean) ISALERT;
    }

    public void setISALERT(Object ISALERT) {
        this.ISALERT = ISALERT;
    }

    public String getCOLOR() {
        return COLOR == null ? "" : String.valueOf(COLOR);
    }

    public void setCOLOR(Object COLOR) {
        this.COLOR = COLOR;
    }


}
