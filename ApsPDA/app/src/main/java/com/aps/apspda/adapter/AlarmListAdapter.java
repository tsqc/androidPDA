package com.aps.apspda.adapter;

import android.support.annotation.Nullable;

import com.aps.apspda.R;
import com.aps.apspda.entity.AlarmEntity;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * @author anyang
 * @date 2017/9/11
 * @desc
 */

public class AlarmListAdapter extends BaseQuickAdapter<AlarmEntity, BaseViewHolder> {

    public AlarmListAdapter(@Nullable List<AlarmEntity> data) {
        super(R.layout.adapter_alarm_list, data);
    }


    @Override
    protected void convert(BaseViewHolder helper, AlarmEntity item) {
        helper.setText(R.id.tvTitle, item.getAlarmTitle());
        helper.setText(R.id.tvContent, item.getAlarmNum());
    }
}
