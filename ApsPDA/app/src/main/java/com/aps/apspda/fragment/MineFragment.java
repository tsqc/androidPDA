package com.aps.apspda.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.aps.apspda.R;
import com.aps.apspda.callback.DialogEntityCallBack;
import com.aps.apspda.callback.EntityCallBack;
import com.aps.apspda.entity.BaseEntity;
import com.aps.apspda.entity.UserInfoEntity;
import com.aps.apspda.utils.AppUtils;
import com.aps.apspda.utils.NetUtils;
import com.aps.apspda.utils.StaticMembers;
import com.aps.apspda.utils.ToastUtils;
import com.google.gson.reflect.TypeToken;
import com.lzy.okgo.model.Response;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class MineFragment extends Fragment {

    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvNumber)
    TextView tvNumber;
    @BindView(R.id.tvPhone)
    TextView tvPhone;
    @BindView(R.id.tvEmail)
    TextView tvEmail;
    @BindView(R.id.tvJob)
    TextView tvJob;
    @BindView(R.id.tvClass)
    TextView tvClass;
    private Unbinder unbinder;


    public MineFragment() {
        // Required empty public constructor
    }

    public static MineFragment createNewInstance() {
        return new MineFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mine, container, false);
        unbinder = ButterKnife.bind(this, view);
        requestUserInfo();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void requestUserInfo() {
        EntityCallBack<BaseEntity<UserInfoEntity>> callBack = new DialogEntityCallBack<BaseEntity<UserInfoEntity>>
                (new TypeToken<BaseEntity<UserInfoEntity>>() {
                }.getType(), getFragmentManager(), this) {

            @Override
            public void onSuccess
                    (final Response<BaseEntity<UserInfoEntity>> response) {
                if (response.body().isSuccess(getActivity())) {
                    UserInfoEntity userInfoEntity = response.body().getData();
                    tvName.setText(userInfoEntity.getName());
                    tvNumber.setText(userInfoEntity.getWorkNo());
                    tvPhone.setText(userInfoEntity.getPhone());
                    tvEmail.setText(userInfoEntity.getEmail());
                    tvJob.setText(userInfoEntity.getRolename());
                    tvClass.setText(userInfoEntity.getClass1());
                } else {
                    ToastUtils.showFreeToast(response.body().getMessage(),
                            getActivity(), false, Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onError
                    (Response<BaseEntity<UserInfoEntity>> response) {
                super.onError(response);
                AppUtils.saveErrorMessages(response.getException(), "SearchUserInfo");
                ToastUtils.showFreeToast("获取用户信息失败",
                       getActivity(), false, Toast.LENGTH_SHORT);
//                loadError();
            }
        };

        Map<String, String> map = new HashMap<>();
        map.put("WorkNo", StaticMembers.CUR_USER.getUSERNAME());
        NetUtils.requestNet(this, "/SearchUserInfo", map, callBack);
    }

}
