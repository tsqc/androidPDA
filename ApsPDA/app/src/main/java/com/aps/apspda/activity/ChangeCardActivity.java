package com.aps.apspda.activity;

import android.view.View;
import android.widget.Toast;

import com.aps.apspda.R;
import com.aps.apspda.base.BaseActivity;
import com.aps.apspda.callback.DialogEntityCallBack;
import com.aps.apspda.callback.EntityCallBack;
import com.aps.apspda.entity.BaseEntity;
import com.aps.apspda.entity.FinishLotEntity;
import com.aps.apspda.myview.ClearEditText;
import com.aps.apspda.myview.TitleView;
import com.aps.apspda.utils.AntiShake;
import com.aps.apspda.utils.NetUtils;
import com.aps.apspda.utils.StaticMembers;
import com.aps.apspda.utils.ToastUtils;
import com.google.gson.reflect.TypeToken;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class ChangeCardActivity extends BaseActivity {

    @BindView(R.id.topView)
    TitleView topView;
    @BindView(R.id.etCardNum)
    ClearEditText etCardNum;

    private String blockNum = "";

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_change_card;
    }

    @Override
    protected void initView() {
        topView.setTitle("更换卡环(" + StaticMembers.CUR_USER.getUSERNAME() + ")");
        topView.setTitleMode(TitleView.NORMAL_TEXT_MODE);
        topView.setRightListener("确定更换", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AntiShake.check(v.getId())) {    //判断是否多次点击
                    ToastUtils.showFreeToast("请勿重复点击",
                            ChangeCardActivity.this, false, Toast.LENGTH_SHORT);
                    return;
                }
                if (etCardNum.getText().toString().trim().length() == 0) {
                    ToastUtils.showFreeToast("请填写片号",
                            ChangeCardActivity.this, false, Toast.LENGTH_SHORT);
                    return;
                }
                if (blockNum.length() == 0) {
                    ToastUtils.showFreeToast("请选择Block数量",
                            ChangeCardActivity.this, false, Toast.LENGTH_SHORT);
                    return;
                }
                requestData();
            }
        });
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        OkGo.getInstance().cancelTag(this);
    }

    private void requestData() {
        EntityCallBack<BaseEntity<FinishLotEntity>> callBack = new DialogEntityCallBack<BaseEntity<FinishLotEntity>>
                (new TypeToken<BaseEntity<FinishLotEntity>>() {
                }.getType(), getSupportFragmentManager(), this) {

            @Override
            public void onSuccess
                    (final Response<BaseEntity<FinishLotEntity>> response) {

                if (response.body().isSuccess(ChangeCardActivity.this)) {
                    ToastUtils.showFreeToast("更换成功",
                            ChangeCardActivity.this, true, Toast.LENGTH_SHORT);
                    ChangeCardActivity.this.finish();
                } else {
                    ToastUtils.showFreeToast(response.body().getMessage(),
                            ChangeCardActivity.this, false, Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onError
                    (Response<BaseEntity<FinishLotEntity>> response) {
                super.onError(response);
                loadError(response.getException(), "ChangeBlockC2E");
            }
        };

        Map<String, String> map = new HashMap<>();
        map.put("EQPID", getEqpID());
        map.put("Block", blockNum);
        map.put("PIECENO", etCardNum.getText().toString().trim());
        NetUtils.requestNet(this, "/ChangeBlockC2E", map, callBack);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @OnClick({R.id.rbOne, R.id.rbTwo, R.id.rbFour})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rbOne:
                blockNum = "1";
                break;
            case R.id.rbTwo:
                blockNum = "2";
                break;
            case R.id.rbFour:
                blockNum = "4";
                break;
        }
    }
}
