package com.aps.apspda.entity;

import com.aps.apspda.utils.AppUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lx
 * @date 2018/4/8
 * @desc
 */

public class MaterialInfo2Entity implements Serializable {

    /**
     * Materials : {"MesMaterial":[{"Qty":0,"SeqNo":"H180206KY010","SawThickness":0.024,"SawLength":0.57,"InternalDiameter":"undefined","ExternalDiameter":"undefined","Type":"WaferSaw","MaterialPartNo":"ZH05-SD3000-N1-50BB","ID":"H180206KY010","EqpID":"1FS05","Size":"undefined"}]}
     * EqpId : 1FS05
     */

    private MaterialsBean Materials;
    private Object EqpId;

    public MaterialsBean getMaterials() {
        return Materials == null ? new MaterialsBean() : Materials;
    }

    public void setMaterials(MaterialsBean materials) {
        Materials = materials;
    }

    public String getEqpId() {
        return AppUtils.getFormatStr(EqpId);
    }

    public void setEqpId(Object eqpId) {
        EqpId = eqpId;
    }

    public static class MaterialsBean {
        private Object MesMaterial;

        public List<MaterialInfoBean> getMesMaterial() {
            if (MesMaterial == null) {
                return new ArrayList<>();
            } else {
                Gson gson = new Gson();
                String json = gson.toJson(MesMaterial);
                List<MaterialInfoBean> list = new ArrayList<>();
                String firstChar = json.substring(0, 1);
                if (firstChar.equals("[")) {
                    list = gson.fromJson(json, new TypeToken<List<MaterialInfoBean>>() {
                    }.getType());
                } else if (firstChar.equals("{")) {
                    list.add((MaterialInfoBean) gson.fromJson(json, new TypeToken<MaterialInfoBean>() {
                    }.getType()));
                }
                return list;
            }
        }

        public void setMesMaterial(List<MaterialInfoBean> MesMaterial) {
            this.MesMaterial = MesMaterial;
        }
    }
}
