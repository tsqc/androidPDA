package com.aps.apspda.callback;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.aps.apspda.activity.DialogActivity;
import com.aps.apspda.activity.LoginActivity;
import com.aps.apspda.utils.ActivityManager;
import com.aps.apspda.utils.ToastUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * @author lx
 * @date 2018/4/24
 * @desc
 */

public class TimeChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(Intent.ACTION_TIME_TICK)) {

            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);
            if (hour == 8 || hour == 20) {
                if (minute == 13) {
                    Intent in = new Intent(context, DialogActivity.class);
                    in.putExtra("hour", hour);
                    context.startActivity(in);
                } else if (minute == 15) {
                    context.startActivity(new Intent(context, LoginActivity.class));
                    ActivityManager.getActivityManager().AppExit(context);
                    ToastUtils.showFreeToast("请重新登录", context,
                            false, Toast.LENGTH_LONG);
                }
            }
        }
    }
}
