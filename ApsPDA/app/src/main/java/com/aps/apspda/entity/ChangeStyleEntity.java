package com.aps.apspda.entity;

import com.aps.apspda.utils.AppUtils;

import java.io.Serializable;

/**
 * @author lx
 * @date 2018/4/10
 * @desc
 */

public class ChangeStyleEntity implements Serializable {


    private Object EQPID;
    private Object LOTID;
    private Object DIESEQ;
    private Object RECIPEID;
    private Object RECIPENO;
    private Object MESSAGEINFO;

    public String getEQPID() {
        return AppUtils.getFormatStr(EQPID);
    }

    public void setEQPID(Object EQPID) {
        this.EQPID = EQPID;
    }

    public String getLOTID() {
        return AppUtils.getFormatStr(LOTID);
    }

    public void setLOTID(Object LOTID) {
        this.LOTID = LOTID;
    }

    public String getDIESEQ() {
        return AppUtils.getFormatStr(DIESEQ);
    }

    public void setDIESEQ(Object DIESEQ) {
        this.DIESEQ = DIESEQ;
    }

    public String getRECIPEID() {
        return AppUtils.getFormatStr(RECIPEID);
    }

    public void setRECIPEID(Object RECIPEID) {
        this.RECIPEID = RECIPEID;
    }

    public String getRECIPENO() {
        return AppUtils.getFormatStr(RECIPENO);
    }

    public void setRECIPENO(Object RECIPENO) {
        this.RECIPENO = RECIPENO;
    }

    public String getMESSAGEINFO() {
        return MESSAGEINFO == null ? "" : String.valueOf(MESSAGEINFO);
    }

    public void setMESSAGEINFO(Object MESSAGEINFO) {
        this.MESSAGEINFO = MESSAGEINFO;
    }


}
