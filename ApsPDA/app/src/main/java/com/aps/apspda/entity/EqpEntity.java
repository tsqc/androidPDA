package com.aps.apspda.entity;

import com.aps.apspda.utils.AppUtils;

import java.io.Serializable;

/**
 * @author lx
 * @date 2018/4/14
 * @desc
 */

public class EqpEntity implements Serializable {


    /**
     * MATERIALINFO : {"MATERIALINFO":{"SawThickness":[{},24],"Device":{},"Type":"WaferSaw","RemainTime":{},"WaferSource":{},"Diagram":{},"Qty":0,"SeqNo":"H20180316BB33X","ThawEndTime":{},"SawLength":[{},570],"InternalDiameter":{},"LiftTime":{},"ExternalDiameter":{},"MaterialPartNo":"ZH05-SD3000-N1-50BBR1","ID":"H20180316BB33X","StartTime":{},"EqpID":"1FS05","Size":{}}}
     * CONNECTION : true
     * EQPID : 1FS05
     * CONTROLSTATUS : LOCAL
     * JOBSTATUS : false
     */

    private MaterialInfoEntity MATERIALINFO;
    private InputDataEntity INPUTDATALIST;
    private Object CONNECTION;
    private Object EQPID;
    private Object CONTROLSTATUS;
    private Object JOBSTATUS;
    private LotInfoEntity LOTINFO;
    private Object EQPTYPE;

    public InputDataEntity getINPUTDATALIST() {
        return INPUTDATALIST == null ? new InputDataEntity() : INPUTDATALIST;
    }

    public void setINPUTDATALIST(InputDataEntity INPUTDATALIST) {
        this.INPUTDATALIST = INPUTDATALIST;
    }

    public String getEQPTYPE() {
        return EQPTYPE == null ? "" : String.valueOf(EQPTYPE);
    }

    public void setEQPTYPE(Object EQPTYPE) {
        this.EQPTYPE = EQPTYPE;
    }

    public MaterialInfoEntity getMATERIALINFO() {
        return MATERIALINFO == null ? new MaterialInfoEntity() : MATERIALINFO;
    }

    public void setMATERIALINFO(MaterialInfoEntity MATERIALINFO) {
        this.MATERIALINFO = MATERIALINFO;
    }

    public boolean getCONNECTION() {
        return CONNECTION != null && (boolean) CONNECTION;
    }

    public void setCONNECTION(Object CONNECTION) {
        this.CONNECTION = CONNECTION;
    }

    public String getEQPID() {
        return AppUtils.getFormatStr(EQPID);
    }

    public void setEQPID(Object EQPID) {
        this.EQPID = EQPID;
    }

    public String getCONTROLSTATUS() {
        return CONTROLSTATUS == null ? "" : String.valueOf(CONTROLSTATUS);
    }

    public void setCONTROLSTATUS(Object CONTROLSTATUS) {
        this.CONTROLSTATUS = CONTROLSTATUS;
    }

    public boolean getJOBSTATUS() {
        return JOBSTATUS != null && (boolean) JOBSTATUS;
    }

    public void setJOBSTATUS(Object JOBSTATUS) {
        this.JOBSTATUS = JOBSTATUS;
    }

    public LotInfoEntity getLOTINFO() {
        return LOTINFO;
    }

    public void setLOTINFO(LotInfoEntity LOTINFO) {
        this.LOTINFO = LOTINFO;
    }


}
