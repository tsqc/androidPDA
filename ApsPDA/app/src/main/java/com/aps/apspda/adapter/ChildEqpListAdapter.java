package com.aps.apspda.adapter;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.aps.apspda.R;
import com.aps.apspda.callback.DbChildCallback;
import com.aps.apspda.entity.ChildEqpEntity;
import com.aps.apspda.utils.AntiShake;
import com.aps.apspda.utils.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * @author
 * @date 2017/9/11
 * @desc
 */

public class ChildEqpListAdapter extends BaseQuickAdapter<ChildEqpEntity.EqpObject, BaseViewHolder> {
    private DbChildCallback dbChildCallback;
    private List<ChildEqpEntity.EqpObject> dataList;

    public ChildEqpListAdapter(@Nullable List<ChildEqpEntity.EqpObject> data, DbChildCallback dbChildCallback) {
        super(R.layout.adapter_child_eqp_list, data);
        this.dbChildCallback = dbChildCallback;
        this.dataList = data;
    }


    @Override
    protected void convert(final BaseViewHolder helper, final ChildEqpEntity.EqpObject item) {
        TextView tvStart = helper.getView(R.id.tvStart);
        helper.setText(R.id.tvDeviceNo, item.getEQPID());
        helper.setText(R.id.tvStatus, item.getCONTROLSTATUS());
        helper.setText(R.id.tvRecipeName, item.getEQPRECIPEID());
        helper.setText(R.id.tvLotRecipeName, item.getEQPLOTRECIPEID());
        helper.setText(R.id.tvLotId, item.getEQPLOTID());
        TextView tvChangeStyle = helper.getView(R.id.tvChangeStyle);
        String end = item.getRESERVEFLAG().substring(1, 2);
        switch (end) {
            case "2":
                tvChangeStyle.setEnabled(false);
                tvChangeStyle.setBackgroundResource(R.drawable.shape_right_top_btn_enabled);
                tvChangeStyle.setTextColor(mContext.getResources().getColor(R.color.text_gray));
                tvStart.setEnabled(true);
                tvStart.setBackgroundResource(R.drawable.selector_right_top_btn_click);
                tvStart.setTextColor(mContext.getResources().getColorStateList(R.color.selector_title_right_text));
                break;
            case "1":
                tvChangeStyle.setEnabled(true);
                tvChangeStyle.setBackgroundResource(R.drawable.selector_right_top_btn_click);
                tvChangeStyle.setTextColor(mContext.getResources().getColorStateList(R.color.selector_title_right_text));
                tvStart.setEnabled(false);
                tvStart.setBackgroundResource(R.drawable.shape_right_top_btn_enabled);
                tvStart.setTextColor(mContext.getResources().getColor(R.color.text_gray));
                break;
            default:
                tvChangeStyle.setEnabled(false);
                tvChangeStyle.setBackgroundResource(R.drawable.shape_right_top_btn_enabled);
                tvChangeStyle.setTextColor(mContext.getResources().getColor(R.color.text_gray));
                tvStart.setEnabled(false);
                tvStart.setBackgroundResource(R.drawable.shape_right_top_btn_enabled);
                tvStart.setTextColor(mContext.getResources().getColor(R.color.text_gray));
                break;
        }
        tvStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AntiShake.check(v.getId())) {    //判断是否多次点击
                    ToastUtils.showFreeToast("请勿重复点击",
                            mContext, false, Toast.LENGTH_SHORT);
                    return;
                }
                if (helper.getAdapterPosition() != 0) {
                    ChildEqpEntity.EqpObject lastObject = dataList.get(helper.getAdapterPosition() - 1);
                    String end = lastObject.getRESERVEFLAG().substring(1, 2);
                    if (end.equals("2")) {
                        ToastUtils.showFreeToast("请先开始上一个设备",
                                mContext, false, Toast.LENGTH_SHORT);
                    } else {
                        dbChildCallback.onStart(item);
                    }
                } else {
                    dbChildCallback.onStart(item);
                }
            }
        });
        tvChangeStyle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AntiShake.check(v.getId())) {    //判断是否多次点击
                    ToastUtils.showFreeToast("请勿重复点击",
                            mContext, false, Toast.LENGTH_SHORT);
                    return;
                }
                dbChildCallback.onChange(helper.getAdapterPosition());
            }
        });
    }
}
