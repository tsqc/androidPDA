package com.aps.apspda.entity;

import com.aps.apspda.utils.AppUtils;

import java.io.Serializable;

/**
 * @author lx
 * @date 2018/4/14
 * @desc
 */

public class MaterialInfoBean implements Serializable {

    /**
     * Qty : 0
     * SeqNo : H20171213BB15T
     * SawThickness : 20
     * SawLength : 360
     * InternalDiameter : {}
     * ExternalDiameter : {}
     * Type : WaferSaw
     * MaterialPartNo : ZH05-SD3000-N1-50 BB R1
     * ID : H20171213BB15T
     * EqpID : 2FS07
     * Size : {}
     */

    private Object Qty;
    private Object SeqNo;
    private Object SawThickness;
    private Object SawLength;
    private Object InternalDiameter;
    private Object ExternalDiameter;
    private Object Type;
    private Object MaterialPartNo;
    private Object MaterialLotNo;
    private Object ID;
    private Object EqpID;
    private Object Size;
    private Object Device;
    private Object WaferSource;
    private Object Diagram;
    private Object ThawEndTime;
    private Object RemainTime;
    private Object LiftTime;
    private Object StartTime;
    private Object EndTime;

    public String getEndTime() {
        return AppUtils.getFormatStr(EndTime);
    }

    public void setEndTime(Object endTime) {
        EndTime = endTime;
    }

    public String getMaterialLotNo() {
        return AppUtils.getFormatStr(MaterialLotNo);
    }

    public void setMaterialLotNo(Object materialLotNo) {
        MaterialLotNo = materialLotNo;
    }

    public String getQty() {
        return AppUtils.getFormatStr(Qty);
    }

    public void setQty(Object qty) {
        Qty = qty;
    }

    public String getSeqNo() {
        return AppUtils.getFormatStr(SeqNo);
    }

    public void setSeqNo(Object seqNo) {
        SeqNo = seqNo;
    }

    public String getSawThickness() {
        return SawThickness == null ? "" : String.valueOf(SawThickness);
    }

    public void setSawThickness(Object sawThickness) {
        SawThickness = sawThickness;
    }

    public String getSawLength() {
        return SawLength == null ? "" : String.valueOf(SawLength);
    }

    public void setSawLength(Object sawLength) {
        SawLength = sawLength;
    }

    public String getInternalDiameter() {
        return InternalDiameter == null ? "" : String.valueOf(InternalDiameter);
    }

    public void setInternalDiameter(Object internalDiameter) {
        InternalDiameter = internalDiameter;
    }

    public String getExternalDiameter() {
        return ExternalDiameter == null ? "" : String.valueOf(ExternalDiameter);
    }

    public void setExternalDiameter(Object externalDiameter) {
        ExternalDiameter = externalDiameter;
    }

    public String getType() {
        return Type == null ? "" : String.valueOf(Type);
    }

    public void setType(Object type) {
        Type = type;
    }

    public String getMaterialPartNo() {
        return AppUtils.getFormatStr(MaterialPartNo);
    }

    public void setMaterialPartNo(Object materialPartNo) {
        MaterialPartNo = materialPartNo;
    }

    public String getID() {
        return AppUtils.getFormatStr(ID);
    }

    public void setID(Object ID) {
        this.ID = ID;
    }

    public String getEqpID() {
        return AppUtils.getFormatStr(EqpID);
    }

    public void setEqpID(Object eqpID) {
        EqpID = eqpID;
    }

    public String getSize() {
        return Size == null ? "" : String.valueOf(Size);
    }

    public void setSize(Object size) {
        Size = size;
    }

    public String getDevice() {
        return Device == null ? "" : String.valueOf(Device);
    }

    public void setDevice(Object device) {
        Device = device;
    }

    public String getWaferSource() {
        return AppUtils.getFormatStr(WaferSource);
    }

    public void setWaferSource(Object waferSource) {
        WaferSource = waferSource;
    }

    public String getDiagram() {
        return AppUtils.getFormatStr(Diagram);
    }

    public void setDiagram(Object diagram) {
        Diagram = diagram;
    }

    public String getThawEndTime() {
        return AppUtils.getFormatStr(ThawEndTime);
    }

    public void setThawEndTime(Object thawEndTime) {
        ThawEndTime = thawEndTime;
    }

    public String getRemainTime() {
        return RemainTime == null ? "" : String.valueOf(RemainTime);
    }

    public void setRemainTime(Object remainTime) {
        RemainTime = remainTime;
    }

    public String getLiftTime() {
        return LiftTime == null ? "" : String.valueOf(LiftTime);
    }

    public void setLiftTime(Object liftTime) {
        LiftTime = liftTime;
    }

    public String getStartTime() {
        return AppUtils.getFormatStr(StartTime);
    }

    public void setStartTime(Object startTime) {
        StartTime = startTime;
    }


}
