package com.aps.apspda.adapter;

import android.support.annotation.Nullable;

import com.aps.apspda.R;
import com.aps.apspda.entity.MessageEntity;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * @author anyang
 * @date 2017/9/11
 * @desc
 */

public class MessageListAdapter extends BaseQuickAdapter<MessageEntity, BaseViewHolder> {

    public MessageListAdapter(@Nullable List<MessageEntity> data) {
        super(R.layout.adapter_messages_list, data);
    }


    @Override
    protected void convert(BaseViewHolder helper, MessageEntity item) {
        switch (item.getCOLOR().toLowerCase()) {
            case "green":
                helper.setTextColor(R.id.tvContent, mContext.getResources().getColor(R.color.colorGreen));
                break;
            case "black":
                helper.setTextColor(R.id.tvContent, mContext.getResources().getColor(R.color.white));
                break;
            case "red":
                helper.setTextColor(R.id.tvContent, mContext.getResources().getColor(R.color.colorRed));
                break;
        }
        helper.setText(R.id.tvContent, item.getMESSAGE());
    }
}
