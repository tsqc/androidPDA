package com.aps.apspda.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.aps.apspda.R;
import com.aps.apspda.activity.DbStartFunctionActivity;
import com.aps.apspda.activity.EndFunctionActivity;
import com.aps.apspda.activity.EqpStatusChangeActivity;
import com.aps.apspda.activity.StartFunctionActivity;
import com.aps.apspda.activity.ThingChangeListActivity;
import com.aps.apspda.callback.StringCommonCallBack;
import com.aps.apspda.dialog.GeneralFragmentDialog;
import com.aps.apspda.utils.AntiShake;
import com.aps.apspda.utils.StaticMembers;
import com.aps.apspda.utils.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class FunctionFragment extends Fragment {

    @BindView(R.id.tvStart)
    TextView tvStart;
    @BindView(R.id.tvEnd)
    TextView tvEnd;
    @BindView(R.id.tvChange)
    TextView tvChange;
    @BindView(R.id.tvEqpStatus)
    TextView tvEqpStatus;
    @BindView(R.id.tvDbStart)
    TextView tvDbStart;
    private Unbinder unbinder;
    private GeneralFragmentDialog dialog;

    public FunctionFragment() {
        // Required empty public constructor
    }

    public static FunctionFragment createNewInstance() {
        return new FunctionFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_function, container, false);
        unbinder = ButterKnife.bind(this, view);
        tvStart.setVisibility(StaticMembers.CUR_USER.getFEATURESCODE().contains("F0") ? View.VISIBLE : View.GONE);
        tvEnd.setVisibility(StaticMembers.CUR_USER.getFEATURESCODE().contains("F1") ? View.VISIBLE : View.GONE);
        tvChange.setVisibility(StaticMembers.CUR_USER.getFEATURESCODE().contains("F2") ? View.VISIBLE : View.GONE);
        tvEqpStatus.setVisibility(StaticMembers.CUR_USER.getFEATURESCODE().contains("F3") ? View.VISIBLE : View.GONE);
        tvDbStart.setVisibility(StaticMembers.CUR_USER.getFEATURESCODE().contains("F4") ? View.VISIBLE : View.GONE);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.tvDbStart, R.id.tvStart, R.id.tvEnd, R.id.tvChange, R.id.tvEqpStatus})
    public void onClick(View view) {
        if (AntiShake.check(view.getId())) {    //判断是否多次点击
            ToastUtils.showFreeToast("请勿重复点击",
                    getActivity(), false, Toast.LENGTH_SHORT);
            return;
        }
        dialog = new GeneralFragmentDialog();
        switch (view.getId()) {
            case R.id.tvDbStart:
                dialog.setCallBackAndWhere(new StringCommonCallBack() {
                    @Override
                    public void onCallback(String str) {
                        Intent intent = new Intent(getActivity(), DbStartFunctionActivity.class);
                        intent.putExtra("eqpID", str);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                }, 5);
                dialog.show(getFragmentManager(), "eqp_start_dialog");
                break;
            case R.id.tvStart:
                dialog.setCallBackAndWhere(new StringCommonCallBack() {
                    @Override
                    public void onCallback(String str) {
                        if (str.toLowerCase().contains("fb") ||
                                str.toLowerCase().contains("fd")) {
                            Intent intent = new Intent(getActivity(), DbStartFunctionActivity.class);
                            intent.putExtra("eqpID", str);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(getActivity(), StartFunctionActivity.class);
                            intent.putExtra("eqpID", str);
                            startActivity(intent);
                        }
                        dialog.dismiss();
                    }
                }, 1);
                dialog.show(getFragmentManager(), "eqp_start_dialog");
                break;
            case R.id.tvEnd:
                dialog.setCallBackAndWhere(new StringCommonCallBack() {
                    @Override
                    public void onCallback(String str) {
                        Intent intent = new Intent(getActivity(), EndFunctionActivity.class);
                        intent.putExtra("eqpID", str);
                        startActivityForResult(intent, 1);
                        dialog.dismiss();
                    }
                }, 2);
                dialog.show(getFragmentManager(), "eqp_end_dialog");
                break;
            case R.id.tvChange:
                dialog.setCallBackAndWhere(new StringCommonCallBack() {
                    @Override
                    public void onCallback(String str) {
                        Intent intent = new Intent(getActivity(), ThingChangeListActivity.class);
                        intent.putExtra("eqpID", str);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                }, 3);
                dialog.show(getFragmentManager(), "eqp_change_dialog");
                break;
            case R.id.tvEqpStatus:
                dialog.setCallBackAndWhere(new StringCommonCallBack() {
                    @Override
                    public void onCallback(String str) {
                        Intent intent = new Intent(getActivity(), EqpStatusChangeActivity.class);
                        intent.putExtra("eqpID", str);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                }, 4);
                dialog.show(getFragmentManager(), "eqp_status_change_dialog");
                break;
        }
    }
}
