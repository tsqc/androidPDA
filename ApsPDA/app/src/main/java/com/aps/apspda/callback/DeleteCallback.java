package com.aps.apspda.callback;

import com.aps.apspda.entity.MaterialInfoBean;

/**
 * @author jaxhuang
 * @date 2017/12/11
 * @desc
 */

public interface DeleteCallback {
    void onSuccess(MaterialInfoBean bean);
}
