package com.aps.apspda.entity;

import java.io.Serializable;

/**
 * @author lx
 * @date 2018/4/20
 * @desc
 */

public class WaferSawEntity implements Serializable {

    /**
     * MATERIALPARTNO : ZH05-SD4500-N1-50 BB
     * SAWLENGTH : 0.57
     * SAWTHICKNESS : 0.025
     * SEQNO : H1712054Z110
     */

    private String MATERIALPARTNO;
    private String SAWLENGTH;
    private String SAWTHICKNESS;
    private String SEQNO;
    private String INTERNALDIAMETER;
    private String EXTERNALDIAMETER;

    public String getINTERNALDIAMETER() {
        return INTERNALDIAMETER == null ? "" : String.valueOf(INTERNALDIAMETER);
    }

    public void setINTERNALDIAMETER(String INTERNALDIAMETER) {
        this.INTERNALDIAMETER = INTERNALDIAMETER;
    }

    public String getEXTERNALDIAMETER() {
        return EXTERNALDIAMETER == null ? "" : String.valueOf(EXTERNALDIAMETER);
    }

    public void setEXTERNALDIAMETER(String EXTERNALDIAMETER) {
        this.EXTERNALDIAMETER = EXTERNALDIAMETER;
    }

    public String getMATERIALPARTNO() {
        return MATERIALPARTNO == null ? "" : String.valueOf(MATERIALPARTNO);
    }

    public void setMATERIALPARTNO(String MATERIALPARTNO) {
        this.MATERIALPARTNO = MATERIALPARTNO;
    }

    public String getSAWLENGTH() {
        return SAWLENGTH == null ? "" : String.valueOf(SAWLENGTH);
    }

    public void setSAWLENGTH(String SAWLENGTH) {
        this.SAWLENGTH = SAWLENGTH;
    }

    public String getSAWTHICKNESS() {
        return SAWTHICKNESS == null ? "" : String.valueOf(SAWTHICKNESS);
    }

    public void setSAWTHICKNESS(String SAWTHICKNESS) {
        this.SAWTHICKNESS = SAWTHICKNESS;
    }

    public String getSEQNO() {
        return SEQNO == null ? "" : String.valueOf(SEQNO);
    }

    public void setSEQNO(String SEQNO) {
        this.SEQNO = SEQNO;
    }


}
