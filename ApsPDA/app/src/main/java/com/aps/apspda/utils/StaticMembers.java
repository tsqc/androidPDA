package com.aps.apspda.utils;

import android.os.Environment;

import com.aps.apspda.entity.LoginEntity;

/**
 * @author jaxhuang
 * @date 2017/9/15
 * @desc
 */

public class StaticMembers {
    //请求超时时间
    public final static long CONNECT_OUT_TIME = 30 * 1000;

    // 每页显示数量
    public final static String PAGE_SIZE = "12";
    // 版本标识
    public final static int VERSION_CODE = 25;
    public final static String UNIT_TIME = "0.5";
    //时间计时
    public final static long TIME_TOTAL = 61000;
    public final static long TIME_PER = 1000;
    public static final String ROOT_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
    public static final String DOWN_PATH = "apk_download";
//    public static String NET_URL = "http://192.168.44.90:8080/aps.2.0";
//    public static String PIC_URL = "http://192.168.44.90:8080/file/";

    public static String NET_URL = "http://0.0.0.0:8099/aps.2.0";
    public static String PIC_URL = "http://0.0.0.0:8099/file/";
    public static String LOG_EQP_ID = "";
    public static LoginEntity CUR_USER = new LoginEntity();
    public static boolean IS_LOGIN = false;
    public static String Lan = "zh";
    public static String logRootPath = Environment.getExternalStorageDirectory().getPath() + "/crash/";
    //屏幕宽高
    public static int SCREEN_WIDTH;
    public static int SCREEN_HEIGHT;
    //状态栏高度
    public static int STATUS_HEIGHT;

}
