package com.aps.apspda.activity;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.aps.apspda.R;
import com.aps.apspda.base.BaseActivity;
import com.aps.apspda.callback.StringCommonCallBack;
import com.aps.apspda.dialog.InputFragmentDialog;
import com.aps.apspda.myview.TitleView;
import com.aps.apspda.utils.AntiShake;
import com.aps.apspda.utils.AppUtils;
import com.aps.apspda.utils.StaticMembers;
import com.aps.apspda.utils.ToastUtils;

import butterknife.BindView;
import butterknife.OnClick;

public class SettingActivity extends BaseActivity {

    @BindView(R.id.topView)
    TitleView topView;
    @BindView(R.id.tvIP)
    TextView tvIP;
    @BindView(R.id.tvPort)
    TextView tvPort;
    @BindView(R.id.tvVersion)
    TextView tvVersion;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_setting;
    }

    @Override
    protected void initView() {
        topView.setTitle("设置");
    }

    @Override
    protected void initData() {
        tvVersion.setText(AppUtils.getVerName());
        tvIP.setText(StaticMembers.NET_URL);
        tvPort.setText(StaticMembers.PIC_URL);
    }

    @OnClick({R.id.rlIP, R.id.rlPort})
    public void onClick(View view) {
        if (AntiShake.check(view.getId())) {    //判断是否多次点击
            ToastUtils.showFreeToast("请勿重复点击",
                    SettingActivity.this, false, Toast.LENGTH_SHORT);
            return;
        }
        switch (view.getId()) {
            case R.id.rlIP:
                final InputFragmentDialog inputFragmentDialog = new InputFragmentDialog();
                inputFragmentDialog.setCallBackAndWhere("请输入IP地址", new StringCommonCallBack() {
                    @Override
                    public void onCallback(String str) {
                        inputFragmentDialog.dismiss();
                        StaticMembers.NET_URL = str;
                        tvIP.setText(StaticMembers.NET_URL);
                        ToastUtils.showFreeToast("保存成功", SettingActivity.this, true, Toast.LENGTH_SHORT);
                        AppUtils.setSharePre("URL_FILE", "URL_IP", str);
                    }
                }, tvIP.getText().toString());
                inputFragmentDialog.show(getSupportFragmentManager(), "URL_IP_DIALOG");
                break;
            case R.id.rlPort:
                final InputFragmentDialog inputFragmentDialog2 = new InputFragmentDialog();
                inputFragmentDialog2.setCallBackAndWhere("请输入IP地址", new StringCommonCallBack() {
                    @Override
                    public void onCallback(String str) {
                        StaticMembers.PIC_URL = str;
                        tvPort.setText(StaticMembers.PIC_URL);
                        inputFragmentDialog2.dismiss();
                        ToastUtils.showFreeToast("保存成功", SettingActivity.this, true, Toast.LENGTH_SHORT);
                        AppUtils.setSharePre("URL_FILE", "FILE_IP", str);
                    }
                }, tvPort.getText().toString());
                inputFragmentDialog2.show(getSupportFragmentManager(), "FILE_IP_DIALOG");
                break;
        }
    }
}
