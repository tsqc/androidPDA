package com.aps.apspda.callback;

/**
 * @author jaxhuang
 * @date 2017/12/11
 * @desc
 */

public interface SuccessAndFailCallback {
    void onSuccess();
    void onFail();
}
