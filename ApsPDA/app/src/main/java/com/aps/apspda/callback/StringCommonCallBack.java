package com.aps.apspda.callback;

/**
 * @author lx
 * @date 2018/4/10
 * @desc
 */

public interface StringCommonCallBack {
    void onCallback(String str);
}