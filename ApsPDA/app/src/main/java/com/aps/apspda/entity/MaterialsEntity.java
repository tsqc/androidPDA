package com.aps.apspda.entity;

import com.aps.apspda.utils.AppUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lx
 * @date 2018/4/8
 * @desc
 */

public class MaterialsEntity implements Serializable {
    private Object MesMaterial;

    public List<MesMaterialBean> getMesMaterial() {

        if (MesMaterial == null) {
            return new ArrayList<>();
        } else {
            Gson gson = new Gson();
            String json = gson.toJson(MesMaterial);
            List<MesMaterialBean> list = new ArrayList<>();
            String firstChar = json.substring(0, 1);
            if (firstChar.equals("[")) {
                list = gson.fromJson(json, new TypeToken<List<MesMaterialBean>>() {
                }.getType());
            } else if (firstChar.equals("{")) {
                list.add((MesMaterialBean) gson.fromJson(json, new TypeToken<MesMaterialBean>() {
                }.getType()));
            }
            return list;
        }
    }

    public void setMesMaterial(List<MesMaterialBean> mesMaterial) {
        MesMaterial = mesMaterial;
    }


    public static class MesMaterialBean {
        /**
         * Qty : 0
         * ChName : 刀片编号
         * Type : wafersaw
         * MaterialPartNo : ZH05-SD3000-N1-50 BB R1 A
         */

        private Object Qty;
        private Object ChName;
        private Object Type;
        private Object MaterialPartNo;

        public String getQty() {
            return AppUtils.getFormatStr(Qty);
        }

        public void setQty(Object qty) {
            Qty = qty;
        }

        public String getChName() {
            return ChName == null ? "" : String.valueOf(ChName);
        }

        public void setChName(Object chName) {
            ChName = chName;
        }

        public String getType() {
            return Type == null ? "" : String.valueOf(Type);
        }

        public void setType(Object type) {
            Type = type;
        }

        public String getMaterialPartNo() {
            return AppUtils.getFormatStr(MaterialPartNo);
        }

        public void setMaterialPartNo(Object materialPartNo) {
            MaterialPartNo = materialPartNo;
        }


    }
}
