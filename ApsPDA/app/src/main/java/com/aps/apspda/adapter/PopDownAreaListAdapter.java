package com.aps.apspda.adapter;

import android.support.annotation.Nullable;

import com.aps.apspda.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * @author anyang
 * @date 2017/9/11
 * @desc
 */

public class PopDownAreaListAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    private int index;

    public PopDownAreaListAdapter(@Nullable List<String> data) {
        super(R.layout.adapter_pop_area_list, data);
    }

    public void setCurrentSelect(int index) {
        this.index = index;
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        if (helper.getLayoutPosition() == index) {
            helper.setTextColor(R.id.tvContent, mContext.getResources().getColor(R.color.theme_gold));
        } else {
            helper.setTextColor(R.id.tvContent, mContext.getResources().getColor(R.color.white));
        }
        helper.setText(R.id.tvContent, item);
    }
}
