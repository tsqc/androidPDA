package com.aps.apspda.callback;


import android.support.v4.app.FragmentManager;

import com.aps.apspda.dialog.LoadingDialog;
import com.lzy.okgo.request.base.Request;

import java.lang.reflect.Type;

/**
 * @author anyang
 * @date 2017/9/12
 * @desc
 */

public abstract class DialogEntityCallBack<T> extends EntityCallBack<T> {
    private LoadingDialog dialog;
    private FragmentManager fragmentManager;


    public DialogEntityCallBack(Type type, FragmentManager fragmentManager, final Object okgoTag) {
        super(type);
        dialog = new LoadingDialog();
        dialog.setRequestTag(okgoTag);
        this.fragmentManager = fragmentManager;
    }


    @Override
    public void onStart(Request request) {
        if (dialog != null) {
            dialog.show(fragmentManager, "loading");
        }
    }


    @Override
    public void onFinish() {
        //网络请求结束后关闭对话框
        if (dialog != null) {
            dialog.dismissAllowingStateLoss();
        }
    }
}
