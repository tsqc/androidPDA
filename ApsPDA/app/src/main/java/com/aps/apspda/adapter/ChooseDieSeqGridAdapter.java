package com.aps.apspda.adapter;

import android.support.annotation.Nullable;
import android.widget.TextView;

import com.aps.apspda.R;
import com.aps.apspda.entity.DieSeqGridEntity;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * @author anyang
 * @date 2017/9/11
 * @desc
 */

public class ChooseDieSeqGridAdapter extends BaseQuickAdapter<DieSeqGridEntity, BaseViewHolder> {

    public ChooseDieSeqGridAdapter(@Nullable List<DieSeqGridEntity> data) {
        super(R.layout.adapter_tab_choose_dieseq, data);
    }


    @Override
    protected void convert(BaseViewHolder helper, final DieSeqGridEntity item) {
        TextView tvDieSeq = helper.getView(R.id.tvDieSeq);
        if (item.isSelect()) {
            tvDieSeq.setTextColor(mContext.getResources().getColor(R.color.white));
            tvDieSeq.setBackgroundColor(mContext.getResources().getColor(R.color.theme_rec_gold));
        } else {
            tvDieSeq.setTextColor(mContext.getResources().getColor(R.color.text_gray));
            tvDieSeq.setBackgroundColor(mContext.getResources().getColor(R.color.room_gold_line));
        }
        tvDieSeq.setText(item.getDieSeq());
    }
}
