package com.aps.apspda.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.aps.apspda.R;
import com.aps.apspda.adapter.ChooseDieSeqGridAdapter;
import com.aps.apspda.callback.StringCommonCallBack;
import com.aps.apspda.entity.DieSeqGridEntity;
import com.aps.apspda.utils.AntiShake;
import com.aps.apspda.utils.StaticMembers;
import com.aps.apspda.utils.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;

import java.util.List;


/**
 * @author huangxin
 * @date 2015/11/10
 * @desc 通用dialog
 */
public class ChooseDieSeqFragmentDialog extends AppCompatDialogFragment {
    String selectDieSeq = "";
    private StringCommonCallBack commonCallBack;
    private List<DieSeqGridEntity> list;

    @Override
    public void show(FragmentManager manager, String tag) {
        if (!isAdded()) {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitAllowingStateLoss();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout((int) (StaticMembers.SCREEN_WIDTH * 0.8), ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }


    public void setData(StringCommonCallBack commonCallBack, List<DieSeqGridEntity> list) {
        this.commonCallBack = commonCallBack;
        this.list = list;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_choose_dieseq, null);
        getDialog().getWindow().setGravity(Gravity.CENTER); // 非常重要：设置对话框弹出的位置
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().getWindow().setWindowAnimations(R.style.BottomDialog); // 添加动画
        getDialog().setCancelable(false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Button btnConfirm = view.findViewById(R.id.btnConfirm);//确认按钮
        Button btnCancel = view.findViewById(R.id.btnCancel);//取消按钮
        RecyclerView rvDieSeq = view.findViewById(R.id.rvDieSeq);
        rvDieSeq.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        final ChooseDieSeqGridAdapter chooseDieSeqGridAdapter = new ChooseDieSeqGridAdapter(list);
        chooseDieSeqGridAdapter.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
        chooseDieSeqGridAdapter.isFirstOnly(true);
        rvDieSeq.setAdapter(chooseDieSeqGridAdapter);
        chooseDieSeqGridAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                selectDieSeq = list.get(position).getDieSeq();
                for (int i = 0; i < list.size(); i++) {
                    if (i == position) {
                        list.get(i).setSelect(true);
                    } else {
                        list.get(i).setSelect(false);
                    }
                }
                chooseDieSeqGridAdapter.notifyDataSetChanged();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AntiShake.check(v.getId())) {    //判断是否多次点击
                    ToastUtils.showFreeToast("请勿重复点击",
                            getActivity(), false, Toast.LENGTH_SHORT);
                    return;
                }
                if (selectDieSeq.length() == 0) {
                    ToastUtils.showFreeToast("请选择DIESEQ", getActivity(), false, Toast.LENGTH_SHORT);
                    return;
                }
                commonCallBack.onCallback(selectDieSeq);
                dismiss();
            }
        });
        super.onViewCreated(view, savedInstanceState);
    }
}
