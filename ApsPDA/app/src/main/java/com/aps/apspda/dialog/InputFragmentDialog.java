package com.aps.apspda.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aps.apspda.R;
import com.aps.apspda.callback.StringCommonCallBack;
import com.aps.apspda.utils.AntiShake;
import com.aps.apspda.utils.StaticMembers;
import com.aps.apspda.utils.ToastUtils;


/**
 * @author huangxin
 * @date 2015/11/10
 * @desc 通用dialog
 */
public class InputFragmentDialog extends AppCompatDialogFragment {
    private String tips, leftStr, rightStr, text;
    private View.OnClickListener onClickListener;
    private StringCommonCallBack callBack;

    @Override
    public void show(FragmentManager manager, String tag) {
        if (!isAdded()) {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitAllowingStateLoss();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout((int) (StaticMembers.SCREEN_WIDTH * 0.8), ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    public void setCallBackAndWhere(String tips, StringCommonCallBack callBack, String text) {
        this.callBack = callBack;
        this.tips = tips;
        this.text = text;
    }

    public void setData(String tips, View.OnClickListener confirmListener) {
        this.tips = tips;
        this.onClickListener = confirmListener;
    }

    public void setData(String tips, String leftStr, String rightStr, View.OnClickListener confirmListener) {
        this.tips = tips;
        this.onClickListener = confirmListener;
        this.leftStr = leftStr;
        this.rightStr = rightStr;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_genaral, null);
        getDialog().getWindow().setGravity(Gravity.CENTER); // 非常重要：设置对话框弹出的位置
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().getWindow().setWindowAnimations(R.style.BottomDialog); // 添加动画
        getDialog().setCancelable(false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Button btnConfirm = view.findViewById(R.id.btnConfirm);//确认按钮
        Button btnCancel = view.findViewById(R.id.btnCancel);//取消按钮
        final EditText etRemark = view.findViewById(R.id.etRemark);//dialog内容
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(tips);
        etRemark.setText(text);
        etRemark.setSelection(etRemark.getText().length());
        if (leftStr != null && leftStr.length() > 0) {
            btnCancel.setText(leftStr);
        }
        if (rightStr != null && rightStr.length() > 0) {
            btnConfirm.setText(rightStr);
        }
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AntiShake.check(v.getId())) {    //判断是否多次点击
                    ToastUtils.showFreeToast("请勿重复点击",
                            getActivity(), false, Toast.LENGTH_SHORT);
                    return;
                }
                if (etRemark.getText().toString().trim().length() == 0) {
                    ToastUtils.showFreeToast("请输入IP地址", getActivity(), false, Toast.LENGTH_SHORT);
                } else {
                    callBack.onCallback(etRemark.getText().toString().trim());
                }
            }
        });
        super.onViewCreated(view, savedInstanceState);
    }
}
