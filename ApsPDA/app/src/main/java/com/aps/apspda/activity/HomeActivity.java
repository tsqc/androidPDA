package com.aps.apspda.activity;

import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.aps.apspda.R;
import com.aps.apspda.base.BaseActivity;
import com.aps.apspda.callback.TimeChangeReceiver;
import com.aps.apspda.dialog.ExitPop;
import com.aps.apspda.fragment.FunctionFragment;
import com.aps.apspda.fragment.InfoFragment;
import com.aps.apspda.fragment.MineFragment;
import com.aps.apspda.myview.TitleView;
import com.aps.apspda.utils.ActivityManager;
import com.aps.apspda.utils.AntiShake;
import com.aps.apspda.utils.StaticMembers;
import com.aps.apspda.utils.ToastUtils;

import butterknife.BindView;
import butterknife.OnClick;

public class HomeActivity extends BaseActivity {
    @BindView(R.id.topView)
    TitleView topView;
    @BindView(R.id.llMain)
    LinearLayout llMain;
    @BindView(R.id.rbFunction)
    RadioButton rbFunction;
    @BindView(R.id.rbInfo)
    RadioButton rbInfo;
    @BindView(R.id.rbMine)
    RadioButton rbMine;

    private Fragment[] mFragments;
    private int mIndex = 0;
    // 双击back退出程序
    private long mLastBackTime = 0;

    private ExitPop exitPop;
    private TimeChangeReceiver receiver;
    /**
     * 底部弹出框点击事件
     */
    private View.OnClickListener itemsOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (AntiShake.check(v.getId())) {    //判断是否多次点击
                ToastUtils.showFreeToast("请勿重复点击",
                        HomeActivity.this, false, Toast.LENGTH_SHORT);
                return;
            }
            exitPop.dismiss();
            switch (v.getId()) {
                case R.id.tvOutLogin:
                    startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                    ActivityManager.getActivityManager().AppExit(HomeActivity.this);
                    break;
                case R.id.tvExit:
                    ActivityManager.getActivityManager().AppExit(HomeActivity.this);
                    break;
            }
        }
    };

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_home;
    }

    @Override
    protected void initView() {
        topView.setTitle("功能管理(" + StaticMembers.CUR_USER.getUSERNAME() + ")");
        topView.setTitleMode(TitleView.RIGHT_IMG_NO_BACK);
        topView.setRightListener(R.drawable.close, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (exitPop == null) {
                    exitPop = new ExitPop(HomeActivity.this, itemsOnClick);
                }
                exitPop.showAtLocation(llMain,
                        Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
            }
        });


    }

    @Override
    protected void initData() {
        FunctionFragment functionFragment = FunctionFragment.createNewInstance();
        InfoFragment infoFragment = InfoFragment.createNewInstance();
        MineFragment mineFragment = MineFragment.createNewInstance();
        //添加到数组
        mFragments = new Fragment[]{functionFragment,
                infoFragment, mineFragment};
        //开启事务
        FragmentTransaction ft =
                getSupportFragmentManager().beginTransaction();
        //添加首页
        ft.add(R.id.content, functionFragment).commit();
        //默认设置为第0个
        setIndexSelected(mIndex);

        receiver = new TimeChangeReceiver();
        registerReceiver(receiver, new IntentFilter(Intent.ACTION_TIME_TICK));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            long TIME_DIFF = 2 * 1000;
            // 双击back,退出程序
            long now = System.currentTimeMillis();
            if (now - mLastBackTime < TIME_DIFF) {
                ActivityManager.getActivityManager().AppExit(this);
                return super.onKeyDown(keyCode, event);
            } else {
                mLastBackTime = now;
                Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
            }
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==110){
            setIndexSelected(0);
            topView.setTitle("功能管理(" + StaticMembers.CUR_USER.getUSERNAME() + ")");
        }
    }

    private void setIndexSelected(int index) {
        if (mIndex == index) {
            return;
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        //隐藏
        ft.hide(mFragments[mIndex]);
        //判断是否添加
        if (!mFragments[index].isAdded()) {
            ft.add(R.id.content, mFragments[index]).show(mFragments[index]);
        } else {
            ft.show(mFragments[index]);
        }
        ft.commit();
        //再次赋值
        mIndex = index;
    }

    @OnClick({R.id.rbFunction, R.id.rbInfo, R.id.rbMine})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rbFunction:
                setIndexSelected(0);
                topView.setTitle("功能管理(" + StaticMembers.CUR_USER.getUSERNAME() + ")");
                break;
            case R.id.rbInfo:
                setIndexSelected(1);
                topView.setTitle("信息查询(" + StaticMembers.CUR_USER.getUSERNAME() + ")");
                break;
            case R.id.rbMine:
                setIndexSelected(2);
                topView.setTitle("我的信息(" + StaticMembers.CUR_USER.getUSERNAME() + ")");
                break;
        }
    }

}
