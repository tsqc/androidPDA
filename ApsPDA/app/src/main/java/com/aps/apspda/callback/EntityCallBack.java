package com.aps.apspda.callback;

import com.aps.apspda.utils.AppUtils;
import com.aps.apspda.utils.LogUtil;
import com.google.gson.Gson;
import com.lzy.okgo.callback.AbsCallback;
import com.lzy.okgo.convert.StringConvert;

import java.lang.reflect.Type;

/**
 * @author anyang
 * @date 2017/9/12
 * @desc
 */

public abstract class EntityCallBack<T> extends AbsCallback<T> {
    private StringConvert convert;
    private Type type;

    public EntityCallBack(Type type) {
        convert = new StringConvert();
        this.type = type;
    }


    @Override
    public T convertResponse(okhttp3.Response response) throws Throwable {
        String s = convert.convertResponse(response);
        LogUtil.e("json", s);
        AppUtils.saveErrorMessages(s, "返回JSON");
        T entity = new Gson().fromJson(s, type);
        response.close();
        return entity;
    }
}
