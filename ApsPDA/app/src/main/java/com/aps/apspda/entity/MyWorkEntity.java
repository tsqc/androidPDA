package com.aps.apspda.entity;

import com.aps.apspda.utils.AppUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lx
 * @date 2018/4/9
 * @desc
 */

public class MyWorkEntity implements Serializable {

    /**
     * SendQtyList : {"SendQty":[{"EQPID":"1FW0801","DoneQty":17700},{"EQPID":"1FW0802","DoneQty":17700}]}
     */

    private SendQtyListBean SendQtyList;

    public SendQtyListBean getSendQtyList() {
        return SendQtyList;
    }

    public void setSendQtyList(SendQtyListBean SendQtyList) {
        this.SendQtyList = SendQtyList;
    }

    public static class SendQtyListBean {
        private Object SendQty;

        public List<SendQtyBean> getSendQty() {
            if (SendQty == null) {
                return new ArrayList<>();
            } else {
                Gson gson = new Gson();
                String json = gson.toJson(SendQty);
                List<SendQtyBean> list = new ArrayList<>();
                String firstChar = json.substring(0, 1);
                if (firstChar.equals("[")) {
                    list = gson.fromJson(json, new TypeToken<List<SendQtyBean>>() {
                    }.getType());
                } else if (firstChar.equals("{")) {
                    list.add((SendQtyBean) gson.fromJson(json, new TypeToken<SendQtyBean>() {
                    }.getType()));
                }
                return list;
            }

        }

        public void setSendQty(List<SendQtyBean> SendQty) {
            this.SendQty = SendQty;
        }

        public static class SendQtyBean {
            private Object EQPID;
            private Object DoneQty;

            public String getEQPID() {
                return AppUtils.getFormatStr(EQPID);
            }

            public void setEQPID(Object EQPID) {
                this.EQPID = EQPID;
            }

            public String getDoneQty() {
                return AppUtils.getFormatStr(DoneQty);
            }

            public void setDoneQty(Object doneQty) {
                DoneQty = doneQty;
            }

        }
    }
}
