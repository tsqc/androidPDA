package com.aps.apspda.entity;

import com.aps.apspda.utils.AppUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lx
 * @date 2018/7/6
 * @desc
 */

public class InputDataEntity implements Serializable {

    private Object INPUTDATA;

    public List<InputObject> getINPUTDATA() {
        if (INPUTDATA == null) {
            return new ArrayList<>();
        } else {
            Gson gson = new Gson();
            String json = gson.toJson(INPUTDATA);
            List<InputObject> list = new ArrayList<>();
            String firstChar = json.substring(0, 1);
            if (firstChar.equals("[")) {
                list = gson.fromJson(json, new TypeToken<List<InputObject>>() {
                }.getType());
            } else if (firstChar.equals("{")) {
                list.add((InputObject) gson.fromJson(json, new TypeToken<InputObject>() {
                }.getType()));
            }
            return list;
        }

    }

    public void setINPUTDATA(Object INPUTDATA) {
        this.INPUTDATA = INPUTDATA;
    }

    public static class InputObject {

        private Object EQPID;
        private Object DATACNNAME;
        private Object ISNULL;
        private Object DATAVALUE;
        private Object DATANAME;
        private Object DATATYPE;
        private Object DATADETAIL;

        public String getDATAVALUE() {
            return AppUtils.getFormatStr(DATAVALUE);
        }

        public void setDATAVALUE(Object DATAVALUE) {
            this.DATAVALUE = DATAVALUE;
        }

        public String getDATATYPE() {
            return AppUtils.getFormatStr(DATATYPE);
        }

        public void setDATATYPE(Object DATATYPE) {
            this.DATATYPE = DATATYPE;
        }


        public String getEQPID() {
            return EQPID == null ? "" : String.valueOf(EQPID);
        }

        public void setEQPID(Object EQPID) {
            this.EQPID = EQPID;
        }

        public String getDATADETAIL() {
            return DATADETAIL == null ? "" : String.valueOf(DATADETAIL);
        }

        public void setDATADETAIL(Object DATADETAIL) {
            this.DATADETAIL = DATADETAIL;
        }

        public String getDATANAME() {
            return DATANAME == null ? "" : String.valueOf(DATANAME);
        }

        public void setDATANAME(Object DATANAME) {
            this.DATANAME = DATANAME;
        }

        public String getDATACNNAME() {
            return DATACNNAME == null ? "" : String.valueOf(DATACNNAME);
        }

        public void setDATACNNAME(Object DATACNNAME) {
            this.DATACNNAME = DATACNNAME;
        }

        public String getISNULL() {
            return ISNULL == null ? "" : String.valueOf(ISNULL);
        }

        public void setISNULL(Object ISNULL) {
            this.ISNULL = ISNULL;
        }

    }
}
