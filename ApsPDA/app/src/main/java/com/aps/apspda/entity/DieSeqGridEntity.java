package com.aps.apspda.entity;

import java.io.Serializable;

/**
 * @author lx
 * @date 2018/7/27
 * @desc
 */

public class DieSeqGridEntity implements Serializable {
    private String dieSeq;
    private boolean isSelect;

    public DieSeqGridEntity(String dieSeq){
        this.dieSeq =dieSeq;
    }

    public String getDieSeq() {
        return dieSeq == null ? "" : String.valueOf(dieSeq);
    }

    public void setDieSeq(String dieSeq) {
        this.dieSeq = dieSeq;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }
}
