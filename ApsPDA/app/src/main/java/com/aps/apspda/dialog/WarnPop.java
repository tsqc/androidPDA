package com.aps.apspda.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.aps.apspda.R;
import com.aps.apspda.utils.StaticMembers;


/**
 * @author jaxhuang
 * @date 2017/9/27
 * @desc
 */

public class WarnPop extends PopupWindow {

    private TextView tvDesc;
    private Button btnConfirm;

    public void setOnlickListener(View.OnClickListener onclickListener){
        btnConfirm.setOnClickListener(onclickListener);
    }

    public WarnPop(Activity context, String string) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mView = inflater.inflate(R.layout.dialog_warn, null);
        tvDesc = mView.findViewById(R.id.tvDesc);
        btnConfirm = mView.findViewById(R.id.btnConfirm);

        // 设置按钮监听
        tvDesc.setText(string);

        //设置PopupWindow的View
        this.setContentView(mView);
        //设置PopupWindow弹出窗体的宽
        this.setWidth((int) (StaticMembers.SCREEN_WIDTH * 0.8));
        //设置PopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置PopupWindow弹出窗体可点击
        this.setFocusable(false);
        this.setOutsideTouchable(false);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.BottomDialog);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
    }
}
