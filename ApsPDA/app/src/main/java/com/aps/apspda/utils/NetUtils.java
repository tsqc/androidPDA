package com.aps.apspda.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;

import com.aps.apspda.base.App;
import com.aps.apspda.callback.EntityCallBack;
import com.aps.apspda.entity.BaseEntity;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.cache.CacheMode;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

/**
 * @author jaxhuang
 * @date 2017/9/18
 * @desc
 */

public class NetUtils {
    public static InputStream is = null;

    /**
     * 检测网络是否可用
     *
     * @return
     */
    public static boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) App.appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnectedOrConnecting();
    }

    /**
     *
     * @param tag 请求ID
     * @param method 请求的方法名称
     * @param map  参数
     * @param callBack 回调
     * @param <T>
     */
    public static <T> void requestNet(Object tag,
                                      String method, Map<String, String> map,
                                      EntityCallBack<BaseEntity<T>> callBack) {
        OkGo.<BaseEntity<T>>
                post(StaticMembers.NET_URL + method)
                .tag(tag)
                .params(map, true)
                .execute(callBack);
    }

    public static <T> void requestNetWithCache(Object tag, String module,
                                               String method, Map<String, String> map,
                                               EntityCallBack<BaseEntity<T>> callBack,
                                               CacheMode cacheMode, String cacheKey) {
        OkGo.<BaseEntity<T>>
                post(StaticMembers.NET_URL + module + method)
                .cacheMode(cacheMode)
                .cacheKey(cacheKey)
                .tag(tag)
                .params(map, true)
                .execute(callBack);
    }

    public static void getInputStreamFromUrl(final String urlStr, final Handler handler) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(urlStr);
                    HttpURLConnection connection = (HttpURLConnection)
                            url.openConnection();
                    connection.setRequestMethod("GET");//试过POST 可能报错
                    connection.setDoInput(true);
                    connection.setConnectTimeout(10000);
                    connection.setReadTimeout(10000);
                    //实现连接
                    connection.connect();
                    if (connection.getResponseCode() == 200) {
                        is = connection.getInputStream();
                        //这里给过去就行了
                        Message message = new Message();
                        message.what = 0;
                        message.obj = is;
                        handler.sendMessage(message);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
