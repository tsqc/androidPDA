package com.aps.apspda.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.aps.apspda.R;
import com.aps.apspda.activity.CurrentEqpPercentActivity;
import com.aps.apspda.activity.MyWorkActivity;
import com.aps.apspda.activity.SearchInfoActivity;
import com.aps.apspda.utils.AntiShake;
import com.aps.apspda.utils.StaticMembers;
import com.aps.apspda.utils.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class InfoFragment extends Fragment {

    @BindView(R.id.tvInfo)
    TextView tvInfo;
    @BindView(R.id.tvEqp)
    TextView tvEqp;
    @BindView(R.id.tvMe)
    TextView tvMe;
    private Unbinder unbinder;


    public InfoFragment() {
        // Required empty public constructor
    }

    public static InfoFragment createNewInstance() {
        return new InfoFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        unbinder = ButterKnife.bind(this, view);
        tvInfo.setVisibility(StaticMembers.CUR_USER.getFEATURESCODE().contains("I0") ? View.VISIBLE : View.GONE);
        tvEqp.setVisibility(StaticMembers.CUR_USER.getFEATURESCODE().contains("I1") ? View.VISIBLE : View.GONE);
        tvMe.setVisibility(StaticMembers.CUR_USER.getFEATURESCODE().contains("I2") ? View.VISIBLE : View.GONE);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.tvInfo, R.id.tvEqp, R.id.tvMe})
    public void onClick(View view) {
        if (AntiShake.check(view.getId())) {    //判断是否多次点击
            ToastUtils.showFreeToast("请勿重复点击",
                    getActivity(), false, Toast.LENGTH_SHORT);
            return;
        }
        switch (view.getId()) {
            case R.id.tvInfo:
                startActivity(new Intent(getActivity(), SearchInfoActivity.class));
                break;
            case R.id.tvEqp:
                startActivity(new Intent(getActivity(), CurrentEqpPercentActivity.class));
                break;
            case R.id.tvMe:
                startActivity(new Intent(getActivity(), MyWorkActivity.class));
                break;
        }
    }
}
