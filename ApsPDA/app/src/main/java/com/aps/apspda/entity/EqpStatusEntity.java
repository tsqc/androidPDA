package com.aps.apspda.entity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lx
 * @date 2018/5/16
 * @desc
 */

public class EqpStatusEntity implements Serializable {


    private String EQPSTATUS;
    private StatusListBean StatusList;
    private String MAINSTATUS;
    private String EQPID;
    private String SUBSTATUS;
    private String LASTHISTSEQ;

    public String getEQPSTATUS() {
        return EQPSTATUS == null ? "" : String.valueOf(EQPSTATUS);
    }

    public void setEQPSTATUS(String EQPSTATUS) {
        this.EQPSTATUS = EQPSTATUS;
    }

    public StatusListBean getStatusList() {
        return StatusList == null ? new StatusListBean() : StatusList;
    }

    public void setStatusList(StatusListBean statusList) {
        StatusList = statusList;
    }

    public String getMAINSTATUS() {
        return MAINSTATUS == null ? "" : String.valueOf(MAINSTATUS);
    }

    public void setMAINSTATUS(String MAINSTATUS) {
        this.MAINSTATUS = MAINSTATUS;
    }

    public String getEQPID() {
        return EQPID == null ? "" : String.valueOf(EQPID);
    }

    public void setEQPID(String EQPID) {
        this.EQPID = EQPID;
    }

    public String getSUBSTATUS() {
        return SUBSTATUS == null ? "" : String.valueOf(SUBSTATUS);
    }

    public void setSUBSTATUS(String SUBSTATUS) {
        this.SUBSTATUS = SUBSTATUS;
    }

    public String getLASTHISTSEQ() {
        return LASTHISTSEQ == null ? "" : String.valueOf(LASTHISTSEQ);
    }

    public void setLASTHISTSEQ(String LASTHISTSEQ) {
        this.LASTHISTSEQ = LASTHISTSEQ;
    }

    public static class StatusListBean {
        private Object Status;

        public List<StatusBean> getStatus() {
            if (Status == null) {
                return new ArrayList<>();
            } else {
                Gson gson = new Gson();
                String json = gson.toJson(Status);
                List<StatusBean> list = new ArrayList<>();
                String firstChar = json.substring(0, 1);
                if (firstChar.equals("[")) {
                    list = gson.fromJson(json, new TypeToken<List<StatusBean>>() {
                    }.getType());
                } else if (firstChar.equals("{")) {
                    list.add((StatusBean) gson.fromJson(json, new TypeToken<StatusBean>() {
                    }.getType()));
                }
                return list;
            }
        }

        public void setStatus(List<StatusBean> Status) {
            this.Status = Status;
        }

        public static class StatusBean {

            private String MAINSTATUS;
            private String SUBSTATUS;
            private String TRANTYPE;
            private String TRANCODE;

            public String getMAINSTATUS() {
                return MAINSTATUS == null ? "" : String.valueOf(MAINSTATUS);
            }

            public void setMAINSTATUS(String MAINSTATUS) {
                this.MAINSTATUS = MAINSTATUS;
            }

            public String getSUBSTATUS() {
                return SUBSTATUS == null ? "" : String.valueOf(SUBSTATUS);
            }

            public void setSUBSTATUS(String SUBSTATUS) {
                this.SUBSTATUS = SUBSTATUS;
            }

            public String getTRAN_TYPE() {
                return TRANTYPE == null ? "" : String.valueOf(TRANTYPE);
            }

            public void setTRAN_TYPE(String TRAN_TYPE) {
                this.TRANTYPE = TRAN_TYPE;
            }

            public String getTRANCODE() {
                return TRANCODE == null ? "" : String.valueOf(TRANCODE);
            }

            public void setTRANCODE(String TRANCODE) {
                this.TRANCODE = TRANCODE;
            }

        }
    }
}
