package com.aps.apspda.entity;

import com.aps.apspda.utils.AppUtils;

import java.io.Serializable;

/**
 * @author lx
 * @date 2018/4/20
 * @desc
 */

public class CTsensorEntity implements Serializable {

    private Object EQPID;
    private Object Type;
    private Object Before;
    private Object After;
    private Object RequireBtn;
    private Object ConfirmBtn;
    private Object ISOK;
    private Object MSG;

    public String getEQPID() {
        return AppUtils.getFormatStr(EQPID);
    }

    public void setEQPID(Object EQPID) {
        this.EQPID = EQPID;
    }

    public String getType() {
        return Type == null ? "" : String.valueOf(Type);
    }

    public void setType(Object type) {
        Type = type;
    }

    public String getBefor() {
        return AppUtils.getFormatStr(Before);
    }

    public void setBefor(Object befor) {
        Before = befor;
    }

    public String getAfter() {
        return AppUtils.getFormatStr(After);
    }

    public void setAfter(Object after) {
        After = after;
    }

    public String getRequireBtn() {
        return RequireBtn == null ? "" : String.valueOf(RequireBtn);
    }

    public void setRequireBtn(Object requireBtn) {
        RequireBtn = requireBtn;
    }

    public String getConfirmBtn() {
        return ConfirmBtn == null ? "" : String.valueOf(ConfirmBtn);
    }

    public void setConfirmBtn(Object confirmBtn) {
        ConfirmBtn = confirmBtn;
    }

    public String getISOK() {
        return ISOK == null ? "" : String.valueOf(ISOK);
    }

    public void setISOK(Object ISOK) {
        this.ISOK = ISOK;
    }

    public String getMSG() {
        return MSG == null ? "" : String.valueOf(MSG);
    }

    public void setMSG(Object MSG) {
        this.MSG = MSG;
    }


}
