package com.aps.apspda.entity;

import com.aps.apspda.utils.AppUtils;

import java.io.Serializable;

/**
 * @author lx
 * @date 2018/4/16
 * @desc
 */

public class UserInfoEntity implements Serializable {


    /**
     * Rolename : Administrator
     * <p>
     * Name : 吕康乐
     * Phone : 13888888888
     * Email : lvkl@glorysoft.com
     * Class : A
     * WorkNo : 180
     */

    private Object Rolename;
    private Object Name;
    private Object Phone;
    private Object Email;
    private Object Class;
    private Object WorkNo;

    public String getRolename() {
        return Rolename == null ? "" : String.valueOf(Rolename);
    }

    public void setRolename(Object rolename) {
        Rolename = rolename;
    }

    public String getName() {
        return Name == null ? "" : String.valueOf(Name);
    }

    public void setName(Object name) {
        Name = name;
    }

    public String getPhone() {
        return AppUtils.getFormatStr(Phone);
    }

    public void setPhone(Object phone) {
        Phone = phone;
    }

    public String getEmail() {
        return Email == null ? "" : String.valueOf(Email);
    }

    public void setEmail(Object email) {
        Email = email;
    }

    public String getClass1() {
        return Class == null ? "" : String.valueOf(Class);
    }

    public void setClass(Object aClass) {
        Class = aClass;
    }

    public String getWorkNo() {
        return AppUtils.getFormatStr(WorkNo);
    }

    public void setWorkNo(Object workNo) {
        WorkNo = workNo;
    }


}
