package com.aps.apspda.entity;

import java.io.Serializable;

/**
 * @author lx
 * @date 2018/4/24
 * @desc
 */

public class AlarmEntity implements Serializable {
    private String alarmTitle;
    private String alarmNum;

    public AlarmEntity(String alarmTitle, String alarmNum) {
        this.alarmTitle = alarmTitle;
        this.alarmNum = alarmNum;
    }

    public String getAlarmTitle() {
        return alarmTitle == null ? "" : String.valueOf(alarmTitle);
    }

    public void setAlarmTitle(String alarmTitle) {
        this.alarmTitle = alarmTitle;
    }

    public String getAlarmNum() {
        return alarmNum == null ? "" : String.valueOf(alarmNum);
    }

    public void setAlarmNum(String alarmNum) {
        this.alarmNum = alarmNum;
    }
}
