package com.aps.apspda.entity;

import com.aps.apspda.utils.AppUtils;

import java.io.Serializable;

/**
 * @author lx
 * @date 2018/4/14
 * @desc
 */

public class NewEqpEntity implements Serializable {

    private Object EQPID;
    private ChildEqpEntity EQPINFOLIST;
    private LotInfoEntity LOTINFO;
    private LotInfoEntity RESERVELOTINFO;


    public ChildEqpEntity getEQPINFOLIST() {
        return EQPINFOLIST == null ? new ChildEqpEntity() : EQPINFOLIST;
    }

    public void setEQPINFOLIST(ChildEqpEntity EQPINFOLIST) {
        this.EQPINFOLIST = EQPINFOLIST;
    }

    public LotInfoEntity getRESERVELOTINFO() {
        return RESERVELOTINFO;
    }

    public void setRESERVELOTINFO(LotInfoEntity RESERVELOTINFO) {
        this.RESERVELOTINFO = RESERVELOTINFO;
    }

    public String getEQPID() {
        return AppUtils.getFormatStr(EQPID);
    }

    public void setEQPID(Object EQPID) {
        this.EQPID = EQPID;
    }


    public LotInfoEntity getLOTINFO() {
        return LOTINFO;
    }

    public void setLOTINFO(LotInfoEntity LOTINFO) {
        this.LOTINFO = LOTINFO;
    }


}
