package com.aps.apspda.callback;

import com.aps.apspda.entity.MaterialInfoBean;

/**
 * @author lx
 * @date 2018/4/10
 * @desc
 */

public interface ThingChangeCallBack {
    void onCallback(MaterialInfoBean data);
}