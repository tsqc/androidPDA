package com.aps.apspda.callback;

import com.aps.apspda.entity.ChildEqpEntity;

/**
 * @author jaxhuang
 * @date 2017/12/11
 * @desc
 */

public interface DbChildCallback {
    void onStart(ChildEqpEntity.EqpObject eqpObject);

    void onChange(int position);
}
