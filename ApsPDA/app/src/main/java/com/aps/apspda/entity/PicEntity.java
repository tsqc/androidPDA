package com.aps.apspda.entity;

import java.io.Serializable;

/**
 * @author lx
 * @date 2018/4/18
 * @desc
 */

public class PicEntity implements Serializable {

    /**
     * retureFileName : ytts_31855\23231288ASB02473L-A.png
     */

    private String retureFileName;

    public String getRetureFileName() {
        return retureFileName;
    }

    public void setRetureFileName(String retureFileName) {
        this.retureFileName = retureFileName;
    }
}
