package com.aps.apspda.entity;

import com.aps.apspda.utils.AppUtils;

import java.io.Serializable;

/**
 * @author lx
 * @date 2018/4/16
 * @desc
 */

public class EqpWorkEntity implements Serializable {
    /**
     * LotType : Production
     * RecipeId : TESTRecipeName
     * TrackOutTime : 2018-01-25
     * TrackInTime : 2018-01-25
     * DoneQty : 0
     * WaferSource : TESTWaferID
     * Tablets : 0
     * LotId : 1231
     * ProcessStartTime : 2018-01-25
     * TotalQty : 109900
     * Diagram : 882
     * LotStatus : Initialized
     * WaferSourceVersion : D
     * DiagramVersion : D
     * ProcessStatus : UNKNOWN
     * EqpId : 1FW0801
     * ProcessEndTime : 2018-01-25
     * DoneTablets : 0
     */

    private Object LotType;
    private Object RecipeId;
    private Object TrackOutTime;
    private Object TrackInTime;
    private Object DoneQty;
    private Object WaferSource;
    private Object Tablets;
    private Object LotId;
    private Object ProcessStartTime;
    private Object TotalQty;
    private Object Diagram;
    private Object LotStatus;
    private Object WaferSourceVersion;
    private Object DiagramVersion;
    private Object ProcessStatus;
    private Object EqpId;
    private Object ProcessEndTime;
    private Object DoneTablets;
    private Object YieID;

    public String getLotType() {
        return LotType == null ? "" : String.valueOf(LotType);
    }

    public void setLotType(Object lotType) {
        LotType = lotType;
    }

    public String getRecipeId() {
        return AppUtils.getFormatStr(RecipeId);
    }

    public void setRecipeId(Object recipeId) {
        RecipeId = recipeId;
    }

    public String getTrackOutTime() {
        return TrackOutTime == null ? "" : String.valueOf(TrackOutTime);
    }

    public void setTrackOutTime(Object trackOutTime) {
        TrackOutTime = trackOutTime;
    }

    public String getTrackInTime() {
        return TrackInTime == null ? "" : String.valueOf(TrackInTime);
    }

    public void setTrackInTime(Object trackInTime) {
        TrackInTime = trackInTime;
    }

    public String getDoneQty() {
        return AppUtils.getFormatStr(DoneQty);
    }

    public void setDoneQty(Object doneQty) {
        DoneQty = doneQty;
    }

    public String getWaferSource() {
        return AppUtils.getFormatStr(WaferSource);
    }

    public void setWaferSource(Object waferSource) {
        WaferSource = waferSource;
    }

    public String getTablets() {
        return AppUtils.getFormatStr(Tablets);
    }

    public void setTablets(Object tablets) {
        Tablets = tablets;
    }

    public String getLotId() {
        return AppUtils.getFormatStr(LotId);
    }

    public void setLotId(Object lotId) {
        LotId = lotId;
    }

    public String getProcessStartTime() {
        return ProcessStartTime == null ? "" : String.valueOf(ProcessStartTime);
    }

    public void setProcessStartTime(Object processStartTime) {
        ProcessStartTime = processStartTime;
    }

    public String getTotalQty() {
        return AppUtils.getFormatStr(TotalQty);
    }

    public void setTotalQty(Object totalQty) {
        TotalQty = totalQty;
    }

    public String getDiagram() {
        return AppUtils.getFormatStr(Diagram);
    }

    public void setDiagram(Object diagram) {
        Diagram = diagram;
    }

    public String getLotStatus() {
        return LotStatus == null ? "" : String.valueOf(LotStatus);
    }

    public void setLotStatus(Object lotStatus) {
        LotStatus = lotStatus;
    }

    public String getWaferSourceVersion() {
        return AppUtils.getFormatStr(WaferSourceVersion);
    }

    public void setWaferSourceVersion(Object waferSourceVersion) {
        WaferSourceVersion = waferSourceVersion;
    }

    public String getDiagramVersion() {
        return AppUtils.getFormatStr(DiagramVersion);
    }

    public void setDiagramVersion(Object diagramVersion) {
        DiagramVersion = diagramVersion;
    }

    public String getProcessStatus() {
        return ProcessStatus == null ? "" : String.valueOf(ProcessStatus);
    }

    public void setProcessStatus(Object processStatus) {
        ProcessStatus = processStatus;
    }

    public String getEqpId() {
        return AppUtils.getFormatStr(EqpId);
    }

    public void setEqpId(Object eqpId) {
        EqpId = eqpId;
    }

    public String getProcessEndTime() {
        return ProcessEndTime == null ? "" : String.valueOf(ProcessEndTime);
    }

    public void setProcessEndTime(Object processEndTime) {
        ProcessEndTime = processEndTime;
    }

    public String getDoneTablets() {
        return AppUtils.getFormatStr(DoneTablets);
    }

    public void setDoneTablets(Object doneTablets) {
        DoneTablets = doneTablets;
    }

    public String getYieID() {
        return YieID == null ? "0" : String.valueOf(YieID);
    }

    public void setYieID(Object yieID) {
        YieID = yieID;
    }


}
