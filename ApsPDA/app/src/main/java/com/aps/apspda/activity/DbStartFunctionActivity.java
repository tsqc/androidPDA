package com.aps.apspda.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aps.apspda.R;
import com.aps.apspda.adapter.ChildEqpListAdapter;
import com.aps.apspda.base.BaseActivity;
import com.aps.apspda.callback.DbChildCallback;
import com.aps.apspda.callback.DialogEntityCallBack;
import com.aps.apspda.callback.EntityCallBack;
import com.aps.apspda.callback.StringCommonCallBack;
import com.aps.apspda.dialog.ChooseDieSeqFragmentDialog;
import com.aps.apspda.dialog.DeleteFragmentDialog;
import com.aps.apspda.dialog.GeneralFragmentDialog;
import com.aps.apspda.entity.BaseEntity;
import com.aps.apspda.entity.ChangeStyleEntity;
import com.aps.apspda.entity.ChildEqpEntity;
import com.aps.apspda.entity.DieSeqGridEntity;
import com.aps.apspda.entity.FinishLotEntity;
import com.aps.apspda.entity.LotInfoEntity;
import com.aps.apspda.entity.NewEqpEntity;
import com.aps.apspda.myview.ClearEditText;
import com.aps.apspda.myview.TitleView;
import com.aps.apspda.utils.AntiShake;
import com.aps.apspda.utils.AppUtils;
import com.aps.apspda.utils.NetUtils;
import com.aps.apspda.utils.StaticMembers;
import com.aps.apspda.utils.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.reflect.TypeToken;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class DbStartFunctionActivity extends BaseActivity {

    @BindView(R.id.topView)
    TitleView topView;
    @BindView(R.id.etLotItem)
    EditText etLotItem;
    @BindView(R.id.etNum)
    EditText etNum;
    @BindView(R.id.tvLogId)
    TextView tvLogId;
    @BindView(R.id.tvDevice)
    TextView tvDevice;
    @BindView(R.id.tvWafer)
    TextView tvWafer;
    //    @BindView(R.id.tvRecipeName)
//    TextView tvRecipeName;
//    @BindView(R.id.tvRecipeNo)
//    TextView tvRecipeNo;
//    @BindView(R.id.rvThingList)
//    RecyclerView rvThingList;
    @BindView(R.id.tvDeviceNo)
    TextView tvDeviceNo;
    //    @BindView(R.id.tvStatus)
//    TextView tvStatus;
//    @BindView(R.id.rvEqpList)
//    RecyclerView rvEqpList;
//    @BindView(R.id.ivRight)
//    ImageView ivRight;
    @BindView(R.id.tvPic)
    TextView tvPic;
    @BindView(R.id.tvPicVersion)
    TextView tvPicVersion;
    @BindView(R.id.rlPic)
    RelativeLayout rlPic;
    //    @BindView(R.id.etRemark)
//    EditText etRemark;
//    @BindView(R.id.tvMore)
//    TextView tvMore;
//    @BindView(R.id.rvMessageList)
//    RecyclerView rvMessageList;
//    @BindView(R.id.ivDownArrow)
//    ImageView ivDownArrow;
//    @BindView(R.id.ivDownArrow2)
//    ImageView ivDownArrow2;
//    @BindView(R.id.rvInputList)
//    RecyclerView rvInputList;
//    @BindView(R.id.rlInfo)
//    RelativeLayout rlInfo;
//    @BindView(R.id.tvSubmitRemark)
//    TextView tvSubmitRemark;
//    @BindView(R.id.toggleButton)
//    ToggleButton toggleButton;
    @BindView(R.id.llMain)
    LinearLayout llMain;
    @BindView(R.id.tvLogIdYue)
    TextView tvLogIdYue;
    @BindView(R.id.etNumYue)
    ClearEditText etNumYue;
    @BindView(R.id.tvDeviceYue)
    TextView tvDeviceYue;
    @BindView(R.id.tvWaferYue)
    TextView tvWaferYue;
    //    @BindView(R.id.tvRecipeNameYue)
//    TextView tvRecipeNameYue;
//    @BindView(R.id.tvRecipeNoYue)
//    TextView tvRecipeNoYue;
//    @BindView(R.id.ivRightYue)
//    ImageView ivRightYue;
    @BindView(R.id.tvPicYue)
    TextView tvPicYue;
    @BindView(R.id.tvPicVersionYue)
    TextView tvPicVersionYue;
    @BindView(R.id.rlPicYue)
    RelativeLayout rlPicYue;
    //    @BindView(R.id.ivDownArrowYue)
//    ImageView ivDownArrowYue;
//    @BindView(R.id.rlInfoYue)
//    RelativeLayout rlInfoYue;
//    @BindView(R.id.rvThingListYue)
//    RecyclerView rvThingListYue;
//    @BindView(R.id.llYue)
//    LinearLayout llYue;
//    @BindView(R.id.tvSubmitInput)
//    TextView tvSubmitInput;
//    @BindView(R.id.rlInput)
//    RelativeLayout rlInput;
//    @BindView(R.id.tvRemark)
//    TextView tvRemark;
//    @BindView(R.id.rlMore)
//    RelativeLayout rlMore;
    @BindView(R.id.rvChildEqpList)
    RecyclerView rvChildEqpList;
    @BindView(R.id.tvStyle)
    TextView tvStyle;


    //    private List<MessageEntity> messageEntityList;
//    private Timer timer;
//    private ThingListAdapter thingListAdapter, thingListAdapterYue;
//    private EqpThingListAdapter eqpThingListAdapter;
//    private MessageListAdapter messageListAdapter;
//    private InputDataListAdapter inputDataListAdapter;
    //    private String startMode = "A";
//    private Animation animation, animationDismiss;
//    private List<InputDataEntity.InputObject> inputList;
//    private List<WarnPop> dialogList = new ArrayList<>();
    private ChildEqpListAdapter childEqpListAdapter;
    private List<ChildEqpEntity.EqpObject> childEqpList;

    private LotInfoEntity yueLotInfo, currentLotInfo;
    private String pic, picVersion, picYue, picVersionYue;
    private Handler mHandler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            DbStartFunctionActivity.this.finish();
        }
    };
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            // 将返回值回调到callBack的参数中
            switch (msg.what) {
                case 0:
                    requestData();
                    break;
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
//        if (timer != null) {
//            timer.cancel();
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestData();
//        startTimer();
    }

    private void requestData() {
        EntityCallBack<BaseEntity<NewEqpEntity>> callBack = new DialogEntityCallBack<BaseEntity<NewEqpEntity>>
                (new TypeToken<BaseEntity<NewEqpEntity>>() {
                }.getType(), getSupportFragmentManager(), this) {

            @Override
            public void onSuccess
                    (final Response<BaseEntity<NewEqpEntity>> response) {
                if (response.body().isSuccess(DbStartFunctionActivity.this)) {
                    NewEqpEntity newEqpEntity = response.body().getData();
                    childEqpList = newEqpEntity.getEQPINFOLIST().getEQPINFO();
                    tvDeviceNo.setText(newEqpEntity.getEQPID());
                    if (childEqpList.size() == 0) {
                        ToastUtils.showFreeToast("暂无子设备信息，请联系管理员",
                                DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
                        return;
                    }
                    String first = childEqpList.get(0).getRESERVEFLAG().substring(0, 1);
                    switch (first) {
                        case "5":
                            tvStyle.setVisibility(View.GONE);
                            break;
                        case "1":
                            tvStyle.setVisibility(View.VISIBLE);
                            tvStyle.setText("同型号");
                            break;
                        case "2":
                            tvStyle.setVisibility(View.VISIBLE);
                            tvStyle.setText("不同型号");
                            break;
                    }
//                    for (ChildEqpEntity.EqpObject eqpObject : childEqpList) {
//                        String end = eqpObject.getRESERVEFLAG().substring(1, 2);
//                        if (end.equals("0") || end.equals("1")) {
//                            eqpObject.setLotInfo(newEqpEntity.getLOTINFO());
//                        } else if (end.equals("2")) {
//                            eqpObject.setLotInfo(newEqpEntity.getRESERVELOTINFO());
//                        }
//                    }
                    setData(newEqpEntity);
                    childEqpListAdapter = new ChildEqpListAdapter(childEqpList, new DbChildCallback() {
                        @Override
                        public void onStart(ChildEqpEntity.EqpObject eqpObject) {
                            if (yueLotInfo == null && currentLotInfo.getLotId().length() == 0) {
                                ToastUtils.showFreeToast("无批次信息，请先扫描！",
                                        DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
                                return;
                            }
                            requestStartWork(eqpObject);
                        }

                        @Override
                        public void onChange(final int position) {
                            if (yueLotInfo == null && currentLotInfo.getLotId().length() == 0) {
                                ToastUtils.showFreeToast("无批次信息，请先扫描！",
                                        DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
                                return;
                            }
                            if (childEqpList.get(position).getEQPID().toLowerCase().contains("db")
                                    || childEqpList.get(position).getEQPID().toLowerCase().contains("fd")) {
                                if (yueLotInfo.getDIESEQLIST().getDIESEQ().size() == 0) {
                                    ToastUtils.showFreeToast("暂无DIESEQ信息",
                                            DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
                                } else {
                                    List<DieSeqGridEntity> dieSeqGridEntityList = new ArrayList<>();
                                    for (String object : yueLotInfo.getDIESEQLIST().getDIESEQ()) {
                                        dieSeqGridEntityList.add(new DieSeqGridEntity(object));
                                    }
                                    final ChooseDieSeqFragmentDialog chooseDieSeqFragmentDialog = new ChooseDieSeqFragmentDialog();
                                    chooseDieSeqFragmentDialog.setData(new StringCommonCallBack() {
                                        @Override
                                        public void onCallback(String str) {
                                            requestChangeStyle(childEqpList.get(position).getEQPID(), str);
                                        }
                                    }, dieSeqGridEntityList);
                                    chooseDieSeqFragmentDialog.show(getSupportFragmentManager(), "chooseDieSeq_dialog");
                                }
                            } else if (childEqpList.get(position).getEQPID().toLowerCase().contains("wb")) {
                                requestChangeStyle(childEqpList.get(position).getEQPID(), null);
                            }

                        }
                    });
                    childEqpListAdapter.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
                    childEqpListAdapter.isFirstOnly(true);
                    rvChildEqpList.setAdapter(childEqpListAdapter);

//                    if (newEqpEntity.getMATERIALINFOLIST().getMATERIALINFO().size() == 0) {
//                        ToastUtils.showFreeToast("该设备没有设置BOM,请联系相关人员",
//                                DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
//                    }
//                    inputList = newEqpEntity.getINPUTDATALIST().getINPUTDATA();
//                    inputDataListAdapter = new InputDataListAdapter(inputList);
//                    inputDataListAdapter.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
//                    inputDataListAdapter.isFirstOnly(true);
//                    rvInputList.setAdapter(inputDataListAdapter);
//
//                    eqpThingListAdapter = new EqpThingListAdapter(newEqpEntity.getMATERIALINFOLIST().getMATERIALINFO(), new DeleteCallback() {
//                        @Override
//                        public void onSuccess(MaterialInfoBean bean) {
//                            final EntityCallBack<BaseEntity<FinishLotEntity>> callBack = new DialogEntityCallBack<BaseEntity<FinishLotEntity>>
//                                    (new TypeToken<BaseEntity<FinishLotEntity>>() {
//                                    }.getType(), getSupportFragmentManager(), this) {
//
//                                @Override
//                                public void onSuccess
//                                        (final Response<BaseEntity<FinishLotEntity>> response) {
//                                    if (response.body().isSuccess(DbStartFunctionActivity.this)) {
//                                        ToastUtils.showFreeToast("删除物料成功",
//                                                DbStartFunctionActivity.this, true, Toast.LENGTH_SHORT);
//                                        requestData();
//                                    } else {
//                                        ToastUtils.showFreeToast(response.body().getMessage(),
//                                                DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
//                                    }
//                                }
//
//                                @Override
//                                public void onError
//                                        (Response<BaseEntity<FinishLotEntity>> response) {
//                                    super.onError(response);
//                                    loadError(response.getException(), "UnloadEQPMeterial");
//                                }
//                            };
//
//                            Map<String, String> map = new HashMap<>();
//                            map.put("EqpId", StaticMembers.CUR_EQP_ID);
//                            map.put("MaterialPartNo", bean.getMaterialPartNo());
//                            map.put("OPID", StaticMembers.CUR_USER.getUSERNAME());
//                            map.put("Type", bean.getType());
//                            map.put("SeqNo", bean.getSeqNo());
//                            NetUtils.requestNet(this, "/UnloadEQPMeterial", map, callBack);
//                        }
//                    });
//                    eqpThingListAdapter.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
//                    eqpThingListAdapter.setFragmentManager(getSupportFragmentManager());
//                    eqpThingListAdapter.isFirstOnly(true);
//                    rvEqpList.setAdapter(eqpThingListAdapter);
                } else {
                    ToastUtils.showFreeToast(response.body().getMessage(),
                            DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
                    mHandler.postDelayed(runnable, 1000);
                }
            }

            @Override
            public void onError
                    (Response<BaseEntity<NewEqpEntity>> response) {
                super.onError(response);
                loadError(response.getException(), "ShowReserveEQPInfoC2E");
                mHandler.postDelayed(runnable, 1000);
            }
        };

        Map<String, String> map = new HashMap<>();
        map.put("EQPID", getEqpID());
        NetUtils.requestNet(this, "/ShowReserveEQPInfoC2E", map, callBack);
    }

    private void setData(NewEqpEntity eqpEntity) {
        yueLotInfo = eqpEntity.getRESERVELOTINFO();
        if (yueLotInfo != null) {
//     thingListAdapterYue = new ThingListAdapter(lotInfoEntity.getMaterials().getMesMaterial());
//     thingListAdapterYue.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
//     thingListAdapterYue.isFirstOnly(true);
//     rvThingListYue.setAdapter(thingListAdapterYue);

            picYue = yueLotInfo.getDiagram();
            picVersionYue = yueLotInfo.getDiagramVersion();
            rlPicYue.setVisibility(picYue.length() > 0 && picVersionYue.length() > 0 ? View.VISIBLE : View.GONE);
            tvLogIdYue.setText(yueLotInfo.getLotId());
            tvDeviceYue.setText(yueLotInfo.getDevice());
            tvWaferYue.setText(yueLotInfo.getWaferSource());
//     tvRecipeNameYue.setText(lotInfoEntity.getRecipeId());
//     tvRecipeNoYue.setText(lotInfoEntity.getRecipeNo());
            tvPicYue.setText(yueLotInfo.getDiagram());
            tvPicVersionYue.setText(yueLotInfo.getDiagramVersion());
            etNumYue.setText(yueLotInfo.getTotalQty());
        } else {
            LotInfoEntity tempLotInfoEntity = new LotInfoEntity();
//     thingListAdapterYue = new ThingListAdapter(tempLotInfoEntity.getMaterials().getMesMaterial());
//     thingListAdapterYue.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
//     thingListAdapterYue.isFirstOnly(true);
//     rvThingListYue.setAdapter(thingListAdapterYue);

            picYue = tempLotInfoEntity.getDiagram();
            picVersionYue = tempLotInfoEntity.getDiagramVersion();
            rlPicYue.setVisibility(picYue.length() > 0 && picVersionYue.length() > 0 ? View.VISIBLE : View.GONE);
            tvLogIdYue.setText(tempLotInfoEntity.getLotId());
            tvDeviceYue.setText(tempLotInfoEntity.getDevice());
            tvWaferYue.setText(tempLotInfoEntity.getWaferSource());
//     tvRecipeNameYue.setText(tempLotInfoEntity.getRecipeId());
//     tvRecipeNoYue.setText(tempLotInfoEntity.getRecipeNo());
            tvPicYue.setText(tempLotInfoEntity.getDiagram());
            tvPicVersionYue.setText(tempLotInfoEntity.getDiagramVersion());
            etNumYue.setText(tempLotInfoEntity.getTotalQty());
        }
        currentLotInfo = eqpEntity.getLOTINFO();

        if (currentLotInfo == null || !currentLotInfo.getIsProcessStart()) {
//         tvSubmitRemark.setVisibility(View.GONE);
            topView.setRightListener("", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
            currentLotInfo = new LotInfoEntity();
        } else {
//         tvSubmitRemark.setVisibility(View.VISIBLE);
            String first = childEqpList.get(0).getRESERVEFLAG().substring(0, 1);
            if (!first.equals("2")) {
                topView.setRightListener("结束作业", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (AntiShake.check(v.getId())) {    //判断是否多次点击
                            ToastUtils.showFreeToast("请勿重复点击",
                                    DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
                            return;
                        }
                        Intent intent = new Intent(DbStartFunctionActivity.this, EndFunctionActivity.class);
                        intent.putExtra("eqpID", getEqpID());
                        startActivityForResult(intent, 1);
                    }
                });
            } else {
                topView.setRightListener("", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                });
            }
        }
        //        thingListAdapter = new ThingListAdapter(lotInfoEntity.getMaterials().getMesMaterial());
//        thingListAdapter.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
//        thingListAdapter.isFirstOnly(true);
//        rvThingList.setAdapter(thingListAdapter);
        pic = currentLotInfo.getDiagram();
        picVersion = currentLotInfo.getDiagramVersion();
        rlPic.setVisibility(pic.length() > 0 && picVersion.length() > 0 ? View.VISIBLE : View.GONE);
        tvLogId.setText(currentLotInfo.getLotId());
        tvDevice.setText(currentLotInfo.getDevice());
        tvWafer.setText(currentLotInfo.getWaferSource());
//         tvRecipeName.setText(lotInfoEntity.getRecipeId());
//         tvRecipeNo.setText(lotInfoEntity.getRecipeNo());
        tvPic.setText(currentLotInfo.getDiagram());
        tvPicVersion.setText(currentLotInfo.getDiagramVersion());
        etNum.setText(currentLotInfo.getTotalQty());
    }

    private void requestStartWork(ChildEqpEntity.EqpObject eqpObject) {
        EntityCallBack<BaseEntity<FinishLotEntity>> callBack = new DialogEntityCallBack<BaseEntity<FinishLotEntity>>
                (new TypeToken<BaseEntity<FinishLotEntity>>() {
                }.getType(), getSupportFragmentManager(), this) {

            @Override
            public void onSuccess
                    (final Response<BaseEntity<FinishLotEntity>> response) {
                if (response.body().isSuccess(DbStartFunctionActivity.this)) {
                    ToastUtils.showFreeToast("开始作业成功",
                            DbStartFunctionActivity.this, true, Toast.LENGTH_SHORT);
                    Message message = new Message();
                    message.what = 0;
                    handler.sendMessage(message);
                } else {
                    ToastUtils.showFreeToast(response.body().getMessage(),
                            DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onError
                    (Response<BaseEntity<FinishLotEntity>> response) {
                super.onError(response);
                loadError(response.getException(), "StartProcess");
            }
        };

        Map<String, String> map = new HashMap<>();
        map.put("EqpId", eqpObject.getEQPID());
        if (yueLotInfo != null) {
            map.put("LotId", yueLotInfo.getLotId());
            map.put("TotalQty", etNumYue.getText().toString().trim().length() == 0 ? "0" : etNumYue.getText().toString().trim());
        } else {
            map.put("LotId", currentLotInfo.getLotId());
            map.put("TotalQty", etNum.getText().toString().trim().length() == 0 ? "0" : etNum.getText().toString().trim());
        }
        map.put("USERNAME", StaticMembers.CUR_USER.getUSERNAME());
//        map.put("Remark", etRemark.getText().toString().trim());

//        JSONObject jsonObject = new JSONObject();
//        try {
//            if (inputList.size() == 0) {
//                jsonObject.put("InputDataList", new JSONObject().put("INPUTDATA", ""));
//            } else {
//                JSONArray jsonArray = new JSONArray(new Gson().toJson(inputList));
//                jsonObject.put("InputDataList", new JSONObject().put("INPUTDATA", jsonArray));
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        map.put("InputDataList", jsonObject.toString());
//        map.put("StartMode", startMode);
        NetUtils.requestNet(this, "/StartProcess", map, callBack);
    }

    private void requestChangeStyle(String eqpid, String str) {
        EntityCallBack<BaseEntity<ChangeStyleEntity>> callBack = new DialogEntityCallBack<BaseEntity<ChangeStyleEntity>>
                (new TypeToken<BaseEntity<ChangeStyleEntity>>() {
                }.getType(), getSupportFragmentManager(), this) {

            @Override
            public void onSuccess
                    (final Response<BaseEntity<ChangeStyleEntity>> response) {
                if (response.body().isSuccess(DbStartFunctionActivity.this)) {
                    ToastUtils.showFreeToast("更换型号成功",
                            DbStartFunctionActivity.this, true, Toast.LENGTH_SHORT);
                    Message message = new Message();
                    message.what = 0;
                    handler.sendMessage(message);
                } else {
                    ToastUtils.showFreeToast(response.body().getMessage(),
                            DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onError
                    (Response<BaseEntity<ChangeStyleEntity>> response) {
                super.onError(response);
                AppUtils.saveErrorMessages(response.getException(), "ChangeProductC2E");
                ToastUtils.showFreeToast("更换型号失败",
                        DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
            }
        };

        Map<String, String> map = new HashMap<>();
        map.put("EQPID", eqpid);
        if (yueLotInfo != null) {
            map.put("LOTID", yueLotInfo.getLotId());
        } else {
            map.put("LOTID", currentLotInfo.getLotId());
        }
        map.put("DIESEQ", str);
        NetUtils.requestNet(this, "/ChangeProductC2E", map, callBack);
    }

//    private void requestMessageData() {
//        EntityCallBack<BaseEntity<List<MessageEntity>>> callBack = new EntityCallBack<BaseEntity<List<MessageEntity>>>
//                (new TypeToken<BaseEntity<List<MessageEntity>>>() {
//                }.getType()) {
//
//            @Override
//            public void onSuccess
//                    (final Response<BaseEntity<List<MessageEntity>>> response) {
//                if (response.body().isSuccess(DbStartFunctionActivity.this)) {
//                    messageEntityList.clear();
//                    messageEntityList.addAll(response.body().getData());
//                    messageListAdapter.notifyDataSetChanged();
//                    dealMessage(response.body().getData());
//                } else {
//                    ToastUtils.showFreeToast(response.body().getMessage(),
//                            DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
//                }
//            }
//
//            @Override
//            public void onError
//                    (Response<BaseEntity<List<MessageEntity>>> response) {
//                super.onError(response);
//                AppUtils.saveErrorMessages(response.getException(), "SHOWMSG2PDA");
//                ToastUtils.showFreeToast("获取实时消息失败",
//                        DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
//            }
//        };
//
//        Map<String, String> map = new HashMap<>();
//        map.put("EQPID", StaticMembers.CUR_EQP_ID);
//        map.put("number", "5");
//        NetUtils.requestNet(this, "/SHOWMSG2PDA", map, callBack);
//    }

//    private void dealMessage(List<MessageEntity> data) {
//        dialogList.clear();
//        for (int i = 0; i < data.size(); i++) {
//            if (data.get(i).getISALERT()) {
//                WarnPop warnPop = new WarnPop(this, data.get(i).getMESSAGE());
//                dialogList.add(warnPop);
//                if (timer != null) {
//                    timer.cancel();
//                }
//            }
//            if (data.get(i).getISEQPSTATUS()) {
//                tvStatus.setText(data.get(i).getEQPSTATUS());
//            }
//        }
//        for (int j = 0; j < dialogList.size(); j++) {
//            final WarnPop warnPop = dialogList.get(j);
//            warnPop.showAtLocation(llMain, Gravity.CENTER, 0, 0);
//            if (j == 0) {
//                warnPop.setOnlickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        warnPop.dismiss();
//                        startTimer();
//                    }
//                });
//            } else {
//                warnPop.setOnlickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        warnPop.dismiss();
//                    }
//                });
//            }
//        }
//        Vibrator vibrator = (Vibrator) this.getSystemService(this.VIBRATOR_SERVICE);
//        vibrator.vibrate(1000 * dialogList.size());
//    }
//
//    private void startTimer() {
//        timer = new Timer();
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                requestMessageData();
//            }
//        }, 0, 2000);
//    }

    @OnClick({R.id.tvChangeThing, R.id.tvCancel, R.id.tvSubmitInput, R.id.rlMore,
            R.id.rlPic, R.id.rlInfo, R.id.rlInfoYue, R.id.tvSubmitRemark, R.id.rlInput, R.id.rlPicYue})
    public void onClick(View view) {
        if (AntiShake.check(view.getId())) {    //判断是否多次点击
            ToastUtils.showFreeToast("请勿重复点击",
                    DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
            return;
        }
        switch (view.getId()) {
            case R.id.tvChangeThing:
                if (childEqpListAdapter.getData().size() > 1) {
                    final GeneralFragmentDialog dialog = new GeneralFragmentDialog();
                    dialog.setCallBackAndWhere(new StringCommonCallBack() {
                        @Override
                        public void onCallback(String str) {
                            boolean isOk = false;
                            for (ChildEqpEntity.EqpObject eqpObject : childEqpList) {
                                if (eqpObject.getEQPID().equals(str)) {
                                    isOk = true;
                                    break;
                                }
                            }
                            if (!isOk) {
                                ToastUtils.showFreeToast("请扫描子设备列表中包含的设备ID",
                                        DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
                                return;
                            }
                            Intent intent = new Intent(DbStartFunctionActivity.this, ThingChangeListActivity.class);
                            intent.putExtra("eqpID", str);
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    }, 6);
                    dialog.show(getSupportFragmentManager(), "eqp_change_dialog");
                } else {
                    Intent intent = new Intent(DbStartFunctionActivity.this, ThingChangeListActivity.class);
                    intent.putExtra("eqpID", childEqpListAdapter.getData().get(0).getEQPID());
                    startActivity(intent);
                }
                break;
            case R.id.tvCancel:
                if (yueLotInfo == null) {
                    ToastUtils.showFreeToast("请先扫描LOT信息", DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
                    return;
                }
                EntityCallBack<BaseEntity<FinishLotEntity>> callBack = new DialogEntityCallBack<BaseEntity<FinishLotEntity>>
                        (new TypeToken<BaseEntity<FinishLotEntity>>() {
                        }.getType(), getSupportFragmentManager(), this) {

                    @Override
                    public void onSuccess
                            (final Response<BaseEntity<FinishLotEntity>> response) {
                        if (response.body().isSuccess(DbStartFunctionActivity.this)) {
                            ToastUtils.showFreeToast("取消预约成功",
                                    DbStartFunctionActivity.this, true, Toast.LENGTH_SHORT);
                            Message message = new Message();
                            message.what = 0;
                            handler.sendMessage(message);
                        } else {
                            ToastUtils.showFreeToast(response.body().getMessage(),
                                    DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onError
                            (Response<BaseEntity<FinishLotEntity>> response) {
                        super.onError(response);
                        loadError(response.getException(), "CancelReserveLotC2E");
                    }
                };

                Map<String, String> map = new HashMap<>();
                map.put("EQPID", getEqpID());
                map.put("LOTID", yueLotInfo.getLotId());
                NetUtils.requestNet(this, "/CancelReserveLotC2E", map, callBack);
                break;
//            case R.id.tvSubmitInput:
//                boolean isInputOk = true;
//                for (InputDataEntity.InputObject inputObject : inputList) {
//                    inputObject.setEQPID(StaticMembers.CUR_EQP_ID);
//                    if (isInputOk) {
//                        if (inputObject.getISNULL().equalsIgnoreCase("Y")) {
//                            if (inputObject.getCONTENT().length() == 0) {
//                                ToastUtils.showFreeToast("请填写带*栏数据", DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
//                                isInputOk = false;
//                            }
//                        }
//                    }
//                }
//                if (isInputOk) {
//                    requestInputData();
//                }
//                break;
//            case R.id.rlInfo:
//                if (rvThingList.getVisibility() == View.VISIBLE) {
//                    if (animationDismiss == null) {
//                        animationDismiss = new RotateAnimation(180f, 0f, Animation.RELATIVE_TO_SELF,
//                                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//                        animationDismiss.setDuration(500);
//                        animationDismiss.setFillAfter(true);
//                    }
//                    ivDownArrow.startAnimation(animationDismiss);
//                    rvThingList.setVisibility(View.GONE);
//                } else {
//                    if (animation == null) {
//                        animation = new RotateAnimation(0f, 180f, Animation.RELATIVE_TO_SELF,
//                                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//                        animation.setDuration(500);
//                        animation.setFillAfter(true);
//                    }
//                    ivDownArrow.startAnimation(animation);
//                    rvThingList.setVisibility(View.VISIBLE);
//                }
//                break;
//            case R.id.rlInfoYue:
//                if (rvThingListYue.getVisibility() == View.VISIBLE) {
//                    if (animationDismiss == null) {
//                        animationDismiss = new RotateAnimation(180f, 0f, Animation.RELATIVE_TO_SELF,
//                                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//                        animationDismiss.setDuration(500);
//                        animationDismiss.setFillAfter(true);
//                    }
//                    ivDownArrowYue.startAnimation(animationDismiss);
//                    rvThingListYue.setVisibility(View.GONE);
//                } else {
//                    if (animation == null) {
//                        animation = new RotateAnimation(0f, 180f, Animation.RELATIVE_TO_SELF,
//                                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//                        animation.setDuration(500);
//                        animation.setFillAfter(true);
//                    }
//                    ivDownArrowYue.startAnimation(animation);
//                    rvThingListYue.setVisibility(View.VISIBLE);
//                }
//                break;
//            case R.id.rlInput:
//                if (rvInputList.getVisibility() == View.VISIBLE) {
//                    if (animationDismiss == null) {
//                        animationDismiss = new RotateAnimation(180f, 0f, Animation.RELATIVE_TO_SELF,
//                                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//                        animationDismiss.setDuration(500);
//                        animationDismiss.setFillAfter(true);
//                    }
//                    ivDownArrow2.startAnimation(animationDismiss);
//                    rvInputList.setVisibility(View.GONE);
//                } else {
//                    if (animation == null) {
//                        animation = new RotateAnimation(0f, 180f, Animation.RELATIVE_TO_SELF,
//                                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//                        animation.setDuration(500);
//                        animation.setFillAfter(true);
//                    }
//                    ivDownArrow2.startAnimation(animation);
//                    rvInputList.setVisibility(View.VISIBLE);
//                }
//                break;
            case R.id.rlMore:
                Intent allMessageIntent = new Intent(DbStartFunctionActivity.this, AllMessageActivity.class);
                allMessageIntent.putExtra("eqpID", getEqpID());
                startActivity(allMessageIntent);
                break;
            case R.id.rlPic:
                Intent intent = new Intent(DbStartFunctionActivity.this, PicViewActivity.class);
                intent.putExtra("pic", pic);
                intent.putExtra("picVersion", picVersion);
                startActivity(intent);
                break;
            case R.id.rlPicYue:
                Intent intentYue = new Intent(DbStartFunctionActivity.this, PicViewActivity.class);
                intentYue.putExtra("pic", picYue);
                intentYue.putExtra("picVersion", picVersionYue);
                startActivity(intentYue);
                break;
//            case R.id.tvSubmitRemark:
//                if (etRemark.getText().toString().trim().length() > 0) {
//                    requestSubmitRemark(LotID, etRemark.getText().toString(), DbStartFunctionActivity.this, getSupportFragmentManager());
//                } else {
//                    ToastUtils.showFreeToast("请填写备注后再提交",
//                            DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
//                }
//                break;
        }
    }

//    private void requestInputData() {
//        EntityCallBack<BaseEntity<FinishLotEntity>> callBack = new DialogEntityCallBack<BaseEntity<FinishLotEntity>>
//                (new TypeToken<BaseEntity<FinishLotEntity>>() {
//                }.getType(), getSupportFragmentManager(), this) {
//
//            @Override
//            public void onSuccess
//                    (final Response<BaseEntity<FinishLotEntity>> response) {
//                if (response.body().isSuccess(DbStartFunctionActivity.this)) {
//                    ToastUtils.showFreeToast("提交数据成功",
//                            DbStartFunctionActivity.this, true, Toast.LENGTH_SHORT);
//                } else {
//                    ToastUtils.showFreeToast(response.body().getMessage(),
//                            DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
//                }
//            }
//
//            @Override
//            public void onError
//                    (Response<BaseEntity<FinishLotEntity>> response) {
//                super.onError(response);
//                loadError(response.getException(), "SubmitInputData");
//            }
//        };
//
//        Map<String, String> map = new HashMap<>();
//        JSONObject jsonObject = new JSONObject();
//        try {
//            if (inputList.size() == 0) {
//                jsonObject.put("InputDataList", new JSONObject().put("INPUTDATA", ""));
//            } else {
//                JSONArray jsonArray = new JSONArray(new Gson().toJson(inputList));
//                jsonObject.put("InputDataList", new JSONObject().put("INPUTDATA", jsonArray));
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        map.put("InputDataList", jsonObject.toString());
//        NetUtils.requestNet(this, "/SubmitInputData", map, callBack);
//    }

//    public static void requestSubmitRemark(String lotID, String remark, final Context context, FragmentManager fragmentManager) {
//        EntityCallBack<BaseEntity<FinishLotEntity>> callBack = new DialogEntityCallBack<BaseEntity<FinishLotEntity>>
//                (new TypeToken<BaseEntity<FinishLotEntity>>() {
//                }.getType(), fragmentManager, context) {
//
//            @Override
//            public void onSuccess
//                    (final Response<BaseEntity<FinishLotEntity>> response) {
//                if (response.body().isSuccess(context)) {
//                    ToastUtils.showFreeToast("提交备注成功",
//                            context, true, Toast.LENGTH_SHORT);
//                } else {
//                    ToastUtils.showFreeToast(response.body().getMessage(),
//                            context, false, Toast.LENGTH_SHORT);
//                }
//            }
//
//            @Override
//            public void onError
//                    (Response<BaseEntity<FinishLotEntity>> response) {
//                super.onError(response);
//                AppUtils.saveErrorMessages(response.getException(), "SubmitRemakeC2E");
//                ToastUtils.showFreeToast("连接服务器失败",
//                        context, false, Toast.LENGTH_SHORT);
//            }
//        };
//
//        Map<String, String> map = new HashMap<>();
//        map.put("EQPID", StaticMembers.CUR_EQP_ID);
//        map.put("OPID", StaticMembers.CUR_USER.getUSERNAME());
//        map.put("LOTID", lotID);
//        map.put("REMARK", remark);
//        NetUtils.requestNet(context, "/SubmitRemakeC2E", map, callBack);
//    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_db_start_function;
    }

    @Override
    protected void initView() {
        topView.setTitle("批次开始(" + StaticMembers.CUR_USER.getUSERNAME() + ")");
        topView.setTitleMode(TitleView.NORMAL_TEXT_MODE);
        topView.setRightListener("", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        topView.setLeftListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AntiShake.check(v.getId())) {    //判断是否多次点击
                    ToastUtils.showFreeToast("请勿重复点击",
                            DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
                    return;
                }
                final DeleteFragmentDialog deleteFragmentDialog = new DeleteFragmentDialog();
                deleteFragmentDialog.setData("确定返回主界面吗？", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteFragmentDialog.dismiss();
                        DbStartFunctionActivity.this.finish();
                    }
                });
                deleteFragmentDialog.show(getSupportFragmentManager(), "unbind_dialog");
            }
        });

        rvChildEqpList.setLayoutManager(new LinearLayoutManager(DbStartFunctionActivity.this));
        rvChildEqpList.setItemAnimator(new DefaultItemAnimator());
        rvChildEqpList.addItemDecoration(new DividerItemDecoration(DbStartFunctionActivity.this, 1));

//        rvInputList.setLayoutManager(new LinearLayoutManager(DbStartFunctionActivity.this));
//        rvInputList.setItemAnimator(new DefaultItemAnimator());
//        rvInputList.addItemDecoration(new DividerItemDecoration(DbStartFunctionActivity.this, 1));
//
//        rvThingList.setLayoutManager(new LinearLayoutManager(DbStartFunctionActivity.this));
//        rvThingList.setItemAnimator(new DefaultItemAnimator());
//        rvThingList.addItemDecoration(new DividerItemDecoration(DbStartFunctionActivity.this, 1));
//
//        rvThingListYue.setLayoutManager(new LinearLayoutManager(DbStartFunctionActivity.this));
//        rvThingListYue.setItemAnimator(new DefaultItemAnimator());
//        rvThingListYue.addItemDecoration(new DividerItemDecoration(DbStartFunctionActivity.this, 1));
//
//        rvEqpList.setLayoutManager(new LinearLayoutManager(DbStartFunctionActivity.this));
//        rvEqpList.setItemAnimator(new DefaultItemAnimator());
//        rvEqpList.addItemDecoration(new DividerItemDecoration(DbStartFunctionActivity.this, 1));
//
//        rvMessageList.setLayoutManager(new LinearLayoutManager(DbStartFunctionActivity.this));
//        rvMessageList.setItemAnimator(new DefaultItemAnimator());
//        rvMessageList.addItemDecoration(new DividerItemDecoration(DbStartFunctionActivity.this, 1));
        etLotItem.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER);
            }
        });
        etLotItem.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (start == 0 && before == 0 && count > 1) {
                    if (s.length() > 0) {
                        requestLotInfo(s.toString());
                    } else {
                        ToastUtils.showFreeToast("扫描信息有误", DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

//        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    startMode = "M";
//                } else {
//                    startMode = "A";
//                }
//            }
//        });
    }

    @Override
    protected void initData() {
        etLotItem.postDelayed(new Runnable() {
            @Override
            public void run() {
                etLotItem.requestFocus();
            }
        }, 500);
        setCallback(etLotItem);
//        messageEntityList = new ArrayList<>();
//        messageListAdapter = new MessageListAdapter(messageEntityList);
//        messageListAdapter.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
//        messageListAdapter.isFirstOnly(true);
//        rvMessageList.setAdapter(messageListAdapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        OkGo.getInstance().cancelTag(this);
//        if (timer != null) {
//            timer.cancel();
//        }
        mHandler.removeCallbacks(runnable);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            boolean isAll = true;
//            for (WarnPop warnPop : dialogList) {
//                if (warnPop.isShowing()) {
//                    isAll = false;
//                    break;
//                }
//            }
//            if (isAll) {
            final DeleteFragmentDialog deleteFragmentDialog = new DeleteFragmentDialog();
            deleteFragmentDialog.setData("确定返回主界面吗？", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteFragmentDialog.dismiss();
                    DbStartFunctionActivity.this.finish();
                }
            });
            deleteFragmentDialog.show(getSupportFragmentManager(), "unbind_dialog");
//            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void requestLotInfo(String lotID) {
        EntityCallBack<BaseEntity<LotInfoEntity>> callBack = new DialogEntityCallBack<BaseEntity<LotInfoEntity>>
                (new TypeToken<BaseEntity<LotInfoEntity>>() {
                }.getType(), getSupportFragmentManager(), this) {

            @Override
            public void onSuccess
                    (final Response<BaseEntity<LotInfoEntity>> response) {

                if (response.body().isSuccess(DbStartFunctionActivity.this)) {
                    if (response.body().isNotNull()) {
                        setData(response.body().getData());
                        requestData();
                    } else {
                        ToastUtils.showFreeToast("扫描的LOT信息不存在",
                                DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
                    }
                } else {
                    ToastUtils.showFreeToast(response.body().getMessage(),
                            DbStartFunctionActivity.this, false, Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onError
                    (Response<BaseEntity<LotInfoEntity>> response) {
                super.onError(response);
                loadError(response.getException(), "LotIn");
            }
        };

        Map<String, String> map = new HashMap<>();
        map.put("EQPID", getEqpID());
        map.put("LOTID", lotID);
        map.put("USERNAME", StaticMembers.CUR_USER.getUSERNAME());
        map.put("RESERVEFLAG", "Y");
        NetUtils.requestNet(this, "/LotIn", map, callBack);
    }

    private void setData(LotInfoEntity lotInfoEntity) {
        yueLotInfo = lotInfoEntity;
        if (yueLotInfo != null) {
//     thingListAdapterYue = new ThingListAdapter(lotInfoEntity.getMaterials().getMesMaterial());
//     thingListAdapterYue.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
//     thingListAdapterYue.isFirstOnly(true);
//     rvThingListYue.setAdapter(thingListAdapterYue);

            picYue = yueLotInfo.getDiagram();
            picVersionYue = yueLotInfo.getDiagramVersion();
            rlPicYue.setVisibility(picYue.length() > 0 && picVersionYue.length() > 0 ? View.VISIBLE : View.GONE);
            tvLogIdYue.setText(yueLotInfo.getLotId());
            tvDeviceYue.setText(yueLotInfo.getDevice());
            tvWaferYue.setText(yueLotInfo.getWaferSource());
//     tvRecipeNameYue.setText(lotInfoEntity.getRecipeId());
//     tvRecipeNoYue.setText(lotInfoEntity.getRecipeNo());
            tvPicYue.setText(yueLotInfo.getDiagram());
            tvPicVersionYue.setText(yueLotInfo.getDiagramVersion());
            etNumYue.setText(yueLotInfo.getTotalQty());
        } else {
            LotInfoEntity tempLotInfoEntity = new LotInfoEntity();
//     thingListAdapterYue = new ThingListAdapter(tempLotInfoEntity.getMaterials().getMesMaterial());
//     thingListAdapterYue.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
//     thingListAdapterYue.isFirstOnly(true);
//     rvThingListYue.setAdapter(thingListAdapterYue);

            picYue = tempLotInfoEntity.getDiagram();
            picVersionYue = tempLotInfoEntity.getDiagramVersion();
            rlPicYue.setVisibility(picYue.length() > 0 && picVersionYue.length() > 0 ? View.VISIBLE : View.GONE);
            tvLogIdYue.setText(tempLotInfoEntity.getLotId());
            tvDeviceYue.setText(tempLotInfoEntity.getDevice());
            tvWaferYue.setText(tempLotInfoEntity.getWaferSource());
//     tvRecipeNameYue.setText(tempLotInfoEntity.getRecipeId());
//     tvRecipeNoYue.setText(tempLotInfoEntity.getRecipeNo());
            tvPicYue.setText(tempLotInfoEntity.getDiagram());
            tvPicVersionYue.setText(tempLotInfoEntity.getDiagramVersion());
            etNumYue.setText(tempLotInfoEntity.getTotalQty());
        }

    }
}
