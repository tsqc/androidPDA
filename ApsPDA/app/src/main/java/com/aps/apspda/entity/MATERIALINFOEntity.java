package com.aps.apspda.entity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lx
 * @date 2018/4/8
 * @desc
 */

public class MaterialInfoEntity implements Serializable {
    /**
     * MATERIALINFO : {"Qty":0,"SeqNo":"H20171213BB15T","SawThickness":20,"SawLength":360,"InternalDiameter":{},"ExternalDiameter":{},"Type":"WaferSaw","MaterialPartNo":"ZH05-SD3000-N1-50 BB R1","ID":"H20171213BB15T","EqpID":"2FS07","Size":{}}
     */

    private Object MATERIALINFO;

    public List<MaterialInfoBean> getMATERIALINFO() {
        if (MATERIALINFO == null) {
            return new ArrayList<>();
        } else {
            Gson gson = new Gson();
            String json = gson.toJson(MATERIALINFO);
            List<MaterialInfoBean> list = new ArrayList<>();
            String firstChar = json.substring(0, 1);
            if (firstChar.equals("[")) {
                list = gson.fromJson(json, new TypeToken<List<MaterialInfoBean>>() {
                }.getType());
            } else if (firstChar.equals("{")) {
                list.add((MaterialInfoBean) gson.fromJson(json, new TypeToken<MaterialInfoBean>() {
                }.getType()));
            }
            return list;
        }

    }

    public void setMATERIALINFO(List<MaterialInfoBean> MATERIALINFO) {
        this.MATERIALINFO = MATERIALINFO;
    }

}
