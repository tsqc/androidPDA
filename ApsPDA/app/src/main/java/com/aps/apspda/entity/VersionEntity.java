package com.aps.apspda.entity;

import com.aps.apspda.utils.AppUtils;

import java.io.Serializable;

/**
 * @author jaxhuang
 * @date 2017/10/20
 * @desc
 */

public class VersionEntity implements Serializable {
    /**
     * name : upGradeAPP
     * version : 2.0
     * info : 有新版本请更新后使用!
     * file : updateAPK\aps1.0.apk
     * date : Sat Apr 14 17:34:58 CST 2018
     * alert : true
     * downLoadURL : UpGradeApp/downLoadFile?file=updateAPK\aps1.0.apk
     * code : 200
     */

    private Object name;
    private Object version;
    private Object info;
    private Object file;
    private Object date;
    private Object alert;
    private Object downLoadURL;

    public String getName() {
        return name == null ? "" : String.valueOf(name);
    }

    public void setName(Object name) {
        this.name = name;
    }

    public String getVersion() {
        return AppUtils.getFormatStr(version);
    }

    public void setVersion(Object version) {
        this.version = version;
    }

    public String getInfo() {
        return info == null ? "" : String.valueOf(info);
    }

    public void setInfo(Object info) {
        this.info = info;
    }

    public String getFile() {
        return file == null ? "" : String.valueOf(file);
    }

    public void setFile(Object file) {
        this.file = file;
    }

    public String getDate() {
        return date == null ? "" : String.valueOf(date);
    }

    public void setDate(Object date) {
        this.date = date;
    }

    public String getAlert() {
        return alert == null ? "" : String.valueOf(alert);
    }

    public void setAlert(Object alert) {
        this.alert = alert;
    }

    public String getDownLoadURL() {
        return downLoadURL == null ? "" : String.valueOf(downLoadURL);
    }

    public void setDownLoadURL(Object downLoadURL) {
        this.downLoadURL = downLoadURL;
    }


}
