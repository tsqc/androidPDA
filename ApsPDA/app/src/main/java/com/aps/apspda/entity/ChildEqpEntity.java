package com.aps.apspda.entity;

import com.aps.apspda.utils.AppUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lx
 * @date 2018/7/6
 * @desc
 */

public class ChildEqpEntity implements Serializable {

    private Object EQPINFO;

    public List<EqpObject> getEQPINFO() {
        if (EQPINFO == null) {
            return new ArrayList<>();
        } else {
            Gson gson = new Gson();
            String json = gson.toJson(EQPINFO);
            List<EqpObject> list = new ArrayList<>();
            String firstChar = json.substring(0, 1);
            if (firstChar.equals("[")) {
                list = gson.fromJson(json, new TypeToken<List<EqpObject>>() {
                }.getType());
            } else if (firstChar.equals("{")) {
                list.add((EqpObject) gson.fromJson(json, new TypeToken<EqpObject>() {
                }.getType()));
            }
            return list;
        }
    }

    public void setEQPINFO(Object EQPINFO) {
        this.EQPINFO = EQPINFO;
    }

    public static class EqpObject {

        private Object EQPID;
        private Object EQPTYPE;
        private Object EQPNO;
        private Object LASTEQP;
        private Object CONNECTION;
        private Object JOBSTATUS;
        private Object CONTROLSTATUS;
        private InputDataEntity INPUTDATALIST;
        private MaterialInfoEntity MATERIALINFOLIST;
        private LotInfoEntity lotInfo;
        private Object EQPRECIPEID;
        private Object EQPLOTRECIPEID;
        private Object RESERVEFLAG;
        private Object EQPLOTID;

        public String getEQPLOTID() {
            return AppUtils.getFormatStr(EQPLOTID);
        }

        public void setEQPLOTID(Object EQPLOTID) {
            this.EQPLOTID = EQPLOTID;
        }

        public String getEQPRECIPEID() {
            return AppUtils.getFormatStr(EQPRECIPEID);
        }

        public void setEQPRECIPEID(Object EQPRECIPEID) {
            this.EQPRECIPEID = EQPRECIPEID;
        }

        public String getEQPLOTRECIPEID() {
            return EQPLOTRECIPEID == null ? "" : String.valueOf(EQPLOTRECIPEID);
        }

        public void setEQPLOTRECIPEID(Object EQPLOTRECIPEID) {
            this.EQPLOTRECIPEID = EQPLOTRECIPEID;
        }

        public String getRESERVEFLAG() {
            return RESERVEFLAG == null ? "" : String.valueOf(RESERVEFLAG);
        }

        public void setRESERVEFLAG(Object RESERVEFLAG) {
            this.RESERVEFLAG = RESERVEFLAG;
        }

        public LotInfoEntity getLotInfo() {
            return lotInfo;
        }

        public void setLotInfo(LotInfoEntity lotInfo) {
            this.lotInfo = lotInfo;
        }

        public MaterialInfoEntity getMATERIALINFOLIST() {
            return MATERIALINFOLIST == null ? new MaterialInfoEntity() : MATERIALINFOLIST;
        }

        public void setMATERIALINFOLIST(MaterialInfoEntity MATERIALINFOLIST) {
            this.MATERIALINFOLIST = MATERIALINFOLIST;
        }

        public InputDataEntity getINPUTDATALIST() {
            return INPUTDATALIST == null ? new InputDataEntity() : INPUTDATALIST;
        }

        public void setINPUTDATALIST(InputDataEntity INPUTDATALIST) {
            this.INPUTDATALIST = INPUTDATALIST;
        }

        public String getEQPTYPE() {
            return EQPTYPE == null ? "" : String.valueOf(EQPTYPE);
        }

        public void setEQPTYPE(Object EQPTYPE) {
            this.EQPTYPE = EQPTYPE;
        }

        public String getEQPNO() {
            return AppUtils.getFormatStr(EQPNO);
        }

        public void setEQPNO(Object EQPNO) {
            this.EQPNO = EQPNO;
        }

        public String getLASTEQP() {
            return LASTEQP == null ? "" : String.valueOf(LASTEQP);
        }

        public void setLASTEQP(Object LASTEQP) {
            this.LASTEQP = LASTEQP;
        }

        public String getCONNECTION() {
            return CONNECTION == null ? "" : String.valueOf(CONNECTION);
        }

        public void setCONNECTION(Object CONNECTION) {
            this.CONNECTION = CONNECTION;
        }

        public String getJOBSTATUS() {
            return JOBSTATUS == null ? "" : String.valueOf(JOBSTATUS);
        }

        public void setJOBSTATUS(Object JOBSTATUS) {
            this.JOBSTATUS = JOBSTATUS;
        }

        public String getCONTROLSTATUS() {
            return CONTROLSTATUS == null ? "" : String.valueOf(CONTROLSTATUS);
        }

        public void setCONTROLSTATUS(Object CONTROLSTATUS) {
            this.CONTROLSTATUS = CONTROLSTATUS;
        }

        public String getEQPID() {
            return EQPID == null ? "" : String.valueOf(EQPID);
        }

        public void setEQPID(Object EQPID) {
            this.EQPID = EQPID;
        }


    }
}
