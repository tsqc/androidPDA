package com.aps.apspda.entity;

import android.content.Context;

import java.io.Serializable;

/**
 * @author anyang
 * @date 2017/9/12
 * @desc
 */

public class BaseEntity<T> implements Serializable {
    private int code;
    private String message;
    private T data;
    private String api;
    private String type;
    private int current_time;
    private String total;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCurrent_time() {
        return current_time;
    }

    public void setCurrent_time(int current_time) {
        this.current_time = current_time;
    }


    //获取到对象后，必须要判断是否获取成功
    public boolean isSuccess(Context context) {
        if (code == 200) {
            return true;
        } else {
            return false;
        }
    }

    //获取到对象后，判断RecordDetail是否为null，为null则没有数据
    public boolean isNotNull() {
        return data != null;
    }
}
