package com.aps.apspda.entity;

import com.aps.apspda.utils.AppUtils;

import java.io.Serializable;

/**
 * @author lx
 * @date 2018/4/14
 * @desc
 */

public class LotEndEntity implements Serializable {
    /**
     * RecipeId : M3CG3906--L1-D1-1
     * Materials : {"MesMaterial":[{"Qty":0,"ChName":"刀片编号","Type":"wafersaw","MaterialPartNo":"ZH05-SD3000-N1-50 BB"},{"Qty":0,"ChName":"刀片编号","Type":"wafersaw","MaterialPartNo":"ZH05-SD3000-N1-50 BB R1"},{"Qty":0,"ChName":"刀片编号","Type":"wafersaw","MaterialPartNo":"ZH05-SD3000-N1-50 BB S0"}]}
     * TrackInTime : 2018/4/12 10:23:51
     * Device : LMBT3906LT1G
     * IsProcessStart : true
     * DoneQty : 0
     * IsCompareParameter : false
     * WaferSource : M3CG3906
     * Tablets : 0
     * LotId : EJ1WCR-022
     * ProcessStartTime : 2018/4/12 10:23:51
     * RecipeNo : EAP-SMD0022
     * TotalQty : 211756
     * Diagram : 88ASB02473L
     * LotStatus : Initialized
     * IsStatus : true
     * OPID : G2976
     * ProcessStatus : RUNNING
     * EqpId : 1FS05
     * ProcessEndTime : 2018/4/12 11:23:47
     * IsDownloadRecipe : false
     * DoneTablets : 0
     */

    private Object RecipeId;
    private MaterialsEntity Materials;
    private Object TrackInTime;
    private Object Device;
    private Object IsProcessStart;
    private Object DoneQty;
    private Object IsCompareParameter;
    private Object WaferSource;
    private Object Tablets;
    private Object LotId;
    private Object ProcessStartTime;
    private Object RecipeNo;
    private Object TotalQty;
    private Object Diagram;
    private Object LotStatus;
    private Object IsStatus;
    private Object OPID;
    private Object ProcessStatus;
    private Object EqpId;
    private Object ProcessEndTime;
    private Object IsDownloadRecipe;
    private Object DoneTablets;
    private Object YieID;
    private Object UPH;
    private Object MainAlarms;
    private InputDataEntity InputDataList;

    public InputDataEntity getINPUTDATALIST() {
        return InputDataList == null ? new InputDataEntity() : InputDataList;
    }

    public void setINPUTDATALIST(InputDataEntity INPUTDATALIST) {
        this.InputDataList = INPUTDATALIST;
    }

    public String getRecipeId() {
        return AppUtils.getFormatStr(RecipeId);
    }

    public void setRecipeId(Object recipeId) {
        RecipeId = recipeId;
    }

    public MaterialsEntity getMaterials() {
        return Materials == null ? new MaterialsEntity() : Materials;
    }

    public void setMaterials(MaterialsEntity materials) {
        Materials = materials;
    }

    public String getTrackInTime() {
        return TrackInTime == null ? "" : String.valueOf(TrackInTime);
    }

    public void setTrackInTime(Object trackInTime) {
        TrackInTime = trackInTime;
    }

    public String getDevice() {
        return AppUtils.getFormatStr(Device);
    }

    public void setDevice(Object device) {
        Device = device;
    }

    public boolean getIsProcessStart() {
        return IsProcessStart != null && (Boolean) IsProcessStart;
    }

    public void setIsProcessStart(Object isProcessStart) {
        IsProcessStart = isProcessStart;
    }

    public String getDoneQty() {
        return AppUtils.getFormatStr(DoneQty);
    }

    public void setDoneQty(Object doneQty) {
        DoneQty = doneQty;
    }

    public boolean getIsCompareParameter() {
        return IsCompareParameter != null && (Boolean) IsCompareParameter;
    }

    public void setIsCompareParameter(Object isCompareParameter) {
        IsCompareParameter = isCompareParameter;
    }

    public String getWaferSource() {
        return AppUtils.getFormatStr(WaferSource);
    }

    public void setWaferSource(Object waferSource) {
        WaferSource = waferSource;
    }

    public String getTablets() {
        return AppUtils.getFormatStr(Tablets);
    }

    public void setTablets(Object tablets) {
        Tablets = tablets;
    }

    public String getLotId() {
        return AppUtils.getFormatStr(LotId);
    }

    public void setLotId(Object lotId) {
        LotId = lotId;
    }

    public String getProcessStartTime() {
        return ProcessStartTime == null ? "" : String.valueOf(ProcessStartTime);
    }

    public void setProcessStartTime(Object processStartTime) {
        ProcessStartTime = processStartTime;
    }

    public String getRecipeNo() {
        return AppUtils.getFormatStr(RecipeNo);
    }

    public void setRecipeNo(Object recipeNo) {
        RecipeNo = recipeNo;
    }

    public String getTotalQty() {
        return AppUtils.getFormatStr(TotalQty);
    }

    public void setTotalQty(Object totalQty) {
        TotalQty = totalQty;
    }

    public String getDiagram() {
        return AppUtils.getFormatStr(Diagram);
    }

    public void setDiagram(Object diagram) {
        Diagram = diagram;
    }

    public String getLotStatus() {
        return LotStatus == null ? "" : String.valueOf(LotStatus);
    }

    public void setLotStatus(Object lotStatus) {
        LotStatus = lotStatus;
    }

    public boolean getIsStatus() {
        return IsStatus != null && (Boolean) IsStatus;
    }

    public void setIsStatus(Object isStatus) {
        IsStatus = isStatus;
    }

    public String getOPID() {
        return AppUtils.getFormatStr(OPID);
    }

    public void setOPID(Object OPID) {
        this.OPID = OPID;
    }

    public String getProcessStatus() {
        return ProcessStatus == null ? "" : String.valueOf(ProcessStatus);
    }

    public void setProcessStatus(Object processStatus) {
        ProcessStatus = processStatus;
    }

    public String getEqpId() {
        return AppUtils.getFormatStr(EqpId);
    }

    public void setEqpId(Object eqpId) {
        EqpId = eqpId;
    }

    public String getProcessEndTime() {
        return ProcessEndTime == null ? "" : String.valueOf(ProcessEndTime);
    }

    public void setProcessEndTime(Object processEndTime) {
        ProcessEndTime = processEndTime;
    }

    public boolean getIsDownloadRecipe() {
        return IsDownloadRecipe != null && (Boolean) IsDownloadRecipe;
    }

    public void setIsDownloadRecipe(Object isDownloadRecipe) {
        IsDownloadRecipe = isDownloadRecipe;
    }

    public String getDoneTablets() {
        return AppUtils.getFormatStr(DoneTablets);
    }

    public void setDoneTablets(Object doneTablets) {
        DoneTablets = doneTablets;
    }

    public String getYieID() {
        return YieID == null ? "" : String.valueOf(YieID);
    }

    public void setYieID(Object yieID) {
        YieID = yieID;
    }

    public String getUPH() {
        return UPH == null ? "" : String.valueOf(UPH);
    }

    public void setUPH(Object UPH) {
        this.UPH = UPH;
    }

    public String getMainAlarms() {
        return MainAlarms == null ? "" : String.valueOf(MainAlarms);
    }

    public void setMainAlarms(Object mainAlarms) {
        MainAlarms = mainAlarms;
    }


}
