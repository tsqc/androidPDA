package com.aps.apspda.activity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aps.apspda.R;
import com.aps.apspda.base.BaseActivity;
import com.aps.apspda.callback.DialogEntityCallBack;
import com.aps.apspda.callback.EntityCallBack;
import com.aps.apspda.entity.BaseEntity;
import com.aps.apspda.entity.EqpWorkEntity;
import com.aps.apspda.myview.CirclePercentView;
import com.aps.apspda.myview.TitleView;
import com.aps.apspda.utils.NetUtils;
import com.aps.apspda.utils.StaticMembers;
import com.aps.apspda.utils.ToastUtils;
import com.google.gson.reflect.TypeToken;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

public class CurrentEqpPercentActivity extends BaseActivity {


    @BindView(R.id.topView)
    TitleView topView;
    @BindView(R.id.tvLotID)
    TextView tvLotID;
    @BindView(R.id.tvWarnNum)
    TextView tvWarnNum;
    @BindView(R.id.tvCurrentWork)
    TextView tvCurrentWork;
    @BindView(R.id.tvPlanWork)
    TextView tvPlanWork;
    @BindView(R.id.circleView)
    CirclePercentView circleView;
    @BindView(R.id.etRemark)
    EditText etRemark;
    @BindView(R.id.llContent)
    LinearLayout llContent;
    @BindView(R.id.tvPassRate)
    TextView tvPassRate;


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_current_eqp_percent;
    }

    @Override
    protected void initView() {
        topView.setTitle("设备作业量(" + StaticMembers.CUR_USER.getUSERNAME() + ")");
        etRemark.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER);
            }
        });
        etRemark.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (start == 0 && before == 0 && count > 1) {
                    if (s.length() > 0) {
                        String[] eqpList = StaticMembers.CUR_USER.getEQPLIST().split(";");
                        boolean isHave = false;
                        for (String str : eqpList) {
                            if (str.equals(s.toString().trim())) {
                                isHave = true;
                                break;
                            }
                        }
                        if (!isHave) {
                            llContent.setVisibility(View.GONE);
                            ToastUtils.showFreeToast("设备不存在或没有权限", CurrentEqpPercentActivity.this, false, Toast.LENGTH_SHORT);
                        } else {
                            requestData(s.toString());
                        }
                    } else {
                        llContent.setVisibility(View.GONE);
                        ToastUtils.showFreeToast("请扫描有内容的条码", CurrentEqpPercentActivity.this, false, Toast.LENGTH_SHORT);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private void requestData(String eqpId) {
        EntityCallBack<BaseEntity<EqpWorkEntity>> callBack = new DialogEntityCallBack<BaseEntity<EqpWorkEntity>>
                (new TypeToken<BaseEntity<EqpWorkEntity>>() {
                }.getType(), getSupportFragmentManager(), this) {

            @Override
            public void onSuccess
                    (final Response<BaseEntity<EqpWorkEntity>> response) {
                if (response.body().isSuccess(CurrentEqpPercentActivity.this)) {
                    llContent.setVisibility(View.VISIBLE);
                    EqpWorkEntity eqpWorkEntity = response.body().getData();
                    tvLotID.setText(eqpWorkEntity.getLotId());
                    tvWarnNum.setText(eqpWorkEntity.getTotalQty());
                    tvPlanWork.setText(eqpWorkEntity.getTotalQty());
                    tvCurrentWork.setText(eqpWorkEntity.getDoneQty());
                    tvPassRate.setText(eqpWorkEntity.getYieID() + "%");
                    List<Double> list = new ArrayList<>();
                    list.add(Double.parseDouble(eqpWorkEntity.getYieID()));
                    list.add(100 - Double.parseDouble(eqpWorkEntity.getYieID()));
                    List<Integer> list2 = new ArrayList<>();
                    list2.add(getResources().getColor(R.color.theme_gold));
                    list2.add(getResources().getColor(R.color.text_gray));
                    circleView.setData(list, list2, "100");
                } else {
                    llContent.setVisibility(View.GONE);
                    ToastUtils.showFreeToast(response.body().getMessage(),
                            CurrentEqpPercentActivity.this, false, Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onError
                    (Response<BaseEntity<EqpWorkEntity>> response) {
                super.onError(response);
                llContent.setVisibility(View.GONE);
                loadError(response.getException(), "SearchEQPInfo");
            }
        };

        Map<String, String> map = new HashMap<>();
        map.put("EqpId", eqpId);
        NetUtils.requestNet(this, "/SearchEQPInfo", map, callBack);
    }

    @Override
    protected void initData() {
        setCallback(etRemark);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        OkGo.getInstance().cancelTag(this);
    }
}
