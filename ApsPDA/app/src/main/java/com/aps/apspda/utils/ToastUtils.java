package com.aps.apspda.utils;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aps.apspda.R;
import com.aps.apspda.base.App;


/**
 * @date 2017/9/12
 * @desc
 */

public class ToastUtils {

    /**
     * 显示toast
     */
    private static Toast toast;
    private static View toastRoot;
    private static TextView mTextView;
    private static ImageView imageView;

    //    public static void showToast(String content) {
//        if (toast == null) {
//            toast = Toast.makeText(App.appContext,
//                    content,
//                    Toast.LENGTH_SHORT);
//        } else {
//            toast.setText(content);
//        }
//        toast.show();
//    }

    private static void initBottomView(Context context) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                (int) (StaticMembers.SCREEN_WIDTH * 0.5),
                ViewGroup.LayoutParams.WRAP_CONTENT);
        //加载Toast布局
        toastRoot = LayoutInflater.from(context).inflate(R.layout.toast_genaral, null);
        //初始化布局控件
        mTextView = toastRoot.findViewById(R.id.tvTips);
        mTextView.setLayoutParams(layoutParams);
        //初始化布局控件
        imageView = toastRoot.findViewById(R.id.ivStatus);
    }


    public static void showConnectFail(Context context) {
        if (toast == null) {
            initBottomView(context);
            toast = new Toast(App.appContext);
            toast.setGravity(Gravity.CENTER, 0, 0);
        }
        imageView.setImageResource(R.drawable.fail);
        mTextView.setText("连接服务器失败");
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastRoot);
        toast.show();
    }

    public static void showNoMore(Context context) {
        if (toast == null) {
            initBottomView(context);
            toast = new Toast(App.appContext);
            toast.setGravity(Gravity.CENTER, 0, 0);
        }
        imageView.setImageResource(R.drawable.fail);
        mTextView.setText("没有更多内容了");
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastRoot);
        toast.show();
    }

//    public static void showFreeBottomToast(String content, Context context,
//                                     boolean isSuccess, int duration) {
//        if (bottomToast == null) {
//            initBottomView(context);
//            bottomToast = new Toast(App.appContext);
//            bottomToast.setGravity(Gravity.BOTTOM, 0, 0);
//        }
//        mTextView.setText(content);
//        if (isSuccess) {
//            imageView.setImageResource(R.drawable.success);
//        } else {
//            imageView.setImageResource(R.drawable.fail);
//        }
//        bottomToast.setDuration(duration);
//        bottomToast.setView(toastRoot);
//        bottomToast.show();
//    }


    public static void showFreeToast(String content, Context context,
                                     boolean isSuccess, int duration) {
        if (toast == null) {
            initBottomView(context);
            toast = new Toast(App.appContext);
            toast.setGravity(Gravity.CENTER, 0, 0);
        }
        mTextView.setText(content);
        if (isSuccess) {
            imageView.setImageResource(R.drawable.success);
        } else {
            imageView.setImageResource(R.drawable.fail);
        }
        toast.setDuration(duration);
        toast.setView(toastRoot);
        toast.show();
    }

    public static void cancel() {
        if (toast != null) {
            toast.cancel();
        }
    }
}
