package com.aps.apspda.entity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lx
 * @date 2018/7/6
 * @desc
 */

public class DieSeqListEntity implements Serializable {

    private Object DIESEQ;

    public List<String> getDIESEQ() {
        if (DIESEQ == null) {
            return new ArrayList<>();
        } else {
            Gson gson = new Gson();
            String json = gson.toJson(DIESEQ);
            List<String> list = new ArrayList<>();
            String firstChar = json.substring(0, 1);
            if (firstChar.equals("[")) {
                list = gson.fromJson(json, new TypeToken<List<String>>() {
                }.getType());
            } else {
                String dieSeqListEntity = gson.fromJson(json, new TypeToken<String>() {
                }.getType());
                list.add(dieSeqListEntity);
            }
            return list;
        }
    }

    public void setDIESEQ(Object DIESEQ) {
        this.DIESEQ = DIESEQ;
    }
}
