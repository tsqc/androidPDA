package com.aps.apspda.entity;

import com.aps.apspda.utils.AppUtils;

import java.io.Serializable;

/**
 * @author lx
 * @date 2018/4/13
 * @desc
 */

public class LoginEntity implements Serializable {
    /**
     * AREA : FAB-A
     * FEATURESCODE : F0;F1;F2;I0;I1;I2
     * ROLENAME : Administrator
     * LOGINRESULT : true
     * USERNAME : 180
     * PASSWORD : 123
     * EQPLIST : 1FS01;1FS02;.....
     * ISACTIVE : true
     */

    private Object AREA;
    private Object FEATURESCODE;
    private Object ROLENAME;
    private Object LOGINRESULT;
    private Object USERNAME;
    private Object PASSWORD;
    private Object EQPLIST;
    private Object ISACTIVE;
    private Object MESSAGE;

    public String getMESSAGE() {
        return MESSAGE == null ? "" : String.valueOf(MESSAGE);
    }

    public void setMESSAGE(Object MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public String getAREA() {
        return AREA == null ? "" : String.valueOf(AREA);
    }

    public void setAREA(Object AREA) {
        this.AREA = AREA;
    }

    public String getFEATURESCODE() {
        return FEATURESCODE == null ? "" : String.valueOf(FEATURESCODE);
    }

    public void setFEATURESCODE(Object FEATURESCODE) {
        this.FEATURESCODE = FEATURESCODE;
    }

    public String getROLENAME() {
        return ROLENAME == null ? "" : String.valueOf(ROLENAME);
    }

    public void setROLENAME(Object ROLENAME) {
        this.ROLENAME = ROLENAME;
    }

    public boolean getLOGINRESULT() {
        return LOGINRESULT != null && (Boolean) LOGINRESULT;
    }

    public void setLOGINRESULT(Object LOGINRESULT) {
        this.LOGINRESULT = LOGINRESULT;
    }

    public String getUSERNAME() {
        return AppUtils.getFormatStr(USERNAME);
    }

    public void setUSERNAME(Object USERNAME) {
        this.USERNAME = USERNAME;
    }

    public String getPASSWORD() {
        return AppUtils.getFormatStr(PASSWORD);
    }

    public void setPASSWORD(Object PASSWORD) {
        this.PASSWORD = PASSWORD;
    }

    public String getEQPLIST() {
        return EQPLIST == null ? "" : String.valueOf(EQPLIST);
    }

    public void setEQPLIST(Object EQPLIST) {
        this.EQPLIST = EQPLIST;
    }

    public boolean getISACTIVE() {
        return ISACTIVE != null && (Boolean) ISACTIVE;
    }

    public void setISACTIVE(Object ISACTIVE) {
        this.ISACTIVE = ISACTIVE;
    }


}
