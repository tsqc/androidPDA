package com.aps.apspda.adapter;

import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aps.apspda.R;
import com.aps.apspda.entity.InputDataEntity;
import com.aps.apspda.utils.AppUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.Arrays;
import java.util.List;

/**
 * @author
 * @date 2017/9/11
 * @desc
 */

public class InputDataListAdapter extends BaseQuickAdapter<InputDataEntity.InputObject, BaseViewHolder> {
    private Animation animation, animationDismiss;

    public InputDataListAdapter(@Nullable List<InputDataEntity.InputObject> data) {
        super(R.layout.adapter_input_list, data);
    }


    @Override
    protected void convert(BaseViewHolder helper, final InputDataEntity.InputObject item) {
        TextView textView = helper.getView(R.id.tvTitle);
        if (item.getISNULL().equalsIgnoreCase("Y")) {
            helper.setText(R.id.tvTitle, "*" + item.getDATACNNAME());
            textView.getPaint().setFakeBoldText(true);//加粗
        } else {
            helper.setText(R.id.tvTitle, item.getDATACNNAME());
            textView.getPaint().setFakeBoldText(false);//加粗
        }
        EditText editText = helper.getView(R.id.etContent);
        LinearLayout llCheck = helper.getView(R.id.llCheck);
        final FrameLayout flChoose = helper.getView(R.id.flChoose);
        switch (item.getDATATYPE()) {
            case "1":
                editText.setVisibility(View.VISIBLE);
                llCheck.setVisibility(View.GONE);
                flChoose.setVisibility(View.GONE);
                if (editText.getTag() instanceof TextWatcher) {
                    editText.removeTextChangedListener((TextWatcher) editText.getTag());
                }
                helper.setText(R.id.etContent, item.getDATAVALUE());
                TextWatcher watcher = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        item.setDATAVALUE(s.length() > 0 ? s.toString() : "");
                    }
                };
                editText.addTextChangedListener(watcher);
                editText.setTag(watcher);
                break;
            case "2":
                editText.setVisibility(View.GONE);
                llCheck.setVisibility(View.VISIBLE);
                flChoose.setVisibility(View.GONE);
                final CheckBox cbOK = helper.getView(R.id.cbOK);
                final CheckBox cbNG = helper.getView(R.id.cbNG);
                cbOK.setChecked(item.getDATAVALUE().equals("OK"));
                cbNG.setChecked(item.getDATAVALUE().equals("NG"));
                cbOK.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean isChecked = ((CheckBox) v).isChecked();
                        if (isChecked) {
                            item.setDATAVALUE("OK");
                            if (cbNG.isChecked()) {
                                cbNG.setChecked(false);
                            }
                        } else {
                            if (cbNG.isChecked()) {
                                item.setDATAVALUE("NG");
                            } else {
                                item.setDATAVALUE("");
                            }
                        }
                    }
                });

                cbNG.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean isChecked = ((CheckBox) v).isChecked();
                        if (isChecked) {
                            item.setDATAVALUE("NG");
                            if (cbOK.isChecked()) {
                                cbOK.setChecked(false);
                            }
                        } else {
                            if (cbOK.isChecked()) {
                                item.setDATAVALUE("OK");
                            } else {
                                item.setDATAVALUE("");
                            }
                        }
                    }
                });
                break;
            case "3":
                editText.setVisibility(View.GONE);
                llCheck.setVisibility(View.GONE);
                flChoose.setVisibility(View.VISIBLE);
                final TextView tvChoose = helper.getView(R.id.tvChoose);
                final ImageView ivArrow = helper.getView(R.id.ivArrow);
                tvChoose.setText(item.getDATAVALUE());
                String[] strings = item.getDATADETAIL().split(";");
                final List<String> list = Arrays.asList(strings);
                LayoutInflater inflater = LayoutInflater.from(mContext);
                View popView = inflater.inflate(R.layout.popupwindow_down_list_select, null);
                RecyclerView lvDownList = popView.findViewById(R.id.lvDownList);
                lvDownList.setLayoutManager(new LinearLayoutManager(mContext));
                lvDownList.setItemAnimator(new DefaultItemAnimator());
                final PopDownAreaListAdapter popDownAreaListAdapter = new PopDownAreaListAdapter(list);
                popDownAreaListAdapter.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
                popDownAreaListAdapter.isFirstOnly(false);
                popDownAreaListAdapter.setCurrentSelect(0);
                lvDownList.setAdapter(popDownAreaListAdapter);
                final PopupWindow titlePop = new PopupWindow(popView, flChoose.getWidth(), RelativeLayout.LayoutParams.MATCH_PARENT, true);
                titlePop.setAnimationStyle(R.style.PopupWindowCenterAnimation);
                titlePop.setBackgroundDrawable(new BitmapDrawable());
                titlePop.setFocusable(true);
                titlePop.setOutsideTouchable(true);
                titlePop.setOnDismissListener(new PopupWindow.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        if (animationDismiss == null) {
                            animationDismiss = new RotateAnimation(180f, 0f, Animation.RELATIVE_TO_SELF,
                                    0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                            animationDismiss.setDuration(500);
                            animationDismiss.setFillAfter(true);
                        }
                        ivArrow.startAnimation(animationDismiss);
                    }
                });
                popDownAreaListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                        popDownAreaListAdapter.setCurrentSelect(position);
                        popDownAreaListAdapter.notifyDataSetChanged();
                        tvChoose.setText(list.get(position));
                        item.setDATAVALUE(list.get(position));
                        titlePop.dismiss();
                    }
                });
                flChoose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (titlePop.isShowing()) {
                            titlePop.dismiss();
                        } else {
                            if (animation == null) {
                                animation = new RotateAnimation(0f, 180f, Animation.RELATIVE_TO_SELF,
                                        0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                animation.setDuration(500);
                                animation.setFillAfter(true);
                            }
                            ivArrow.startAnimation(animation);
                            AppUtils.showAsDropDown(titlePop, flChoose, 0, 0);
                        }
                    }
                });
                break;
        }
    }
}
