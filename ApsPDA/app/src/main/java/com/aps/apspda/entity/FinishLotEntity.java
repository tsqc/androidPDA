package com.aps.apspda.entity;

import com.aps.apspda.utils.AppUtils;

import java.io.Serializable;

/**
 * @author lx
 * @date 2018/4/14
 * @desc
 */

public class FinishLotEntity implements Serializable {
    /**
     * MESSAGE : 进站成功!可以开始作业!
     * COMPARERESULT : true
     * EQPID : 1FS04
     * COLOR : black
     * ISSHOW : false
     */

    private Object MESSAGE;
    private Object COMPARERESULT;
    private Object EQPID;
    private Object COLOR;
    private Object ISSHOW;

    public String getMESSAGE() {
        return MESSAGE == null ? "" : String.valueOf(MESSAGE);
    }

    public void setMESSAGE(Object MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public String getCOMPARERESULT() {
        return COMPARERESULT == null ? "" : String.valueOf(COMPARERESULT);
    }

    public void setCOMPARERESULT(Object COMPARERESULT) {
        this.COMPARERESULT = COMPARERESULT;
    }

    public String getEQPID() {
        return AppUtils.getFormatStr(EQPID);
    }

    public void setEQPID(Object EQPID) {
        this.EQPID = EQPID;
    }

    public String getCOLOR() {
        return COLOR == null ? "" : String.valueOf(COLOR);
    }

    public void setCOLOR(Object COLOR) {
        this.COLOR = COLOR;
    }

    public boolean getISSHOW() {
        return ISSHOW != null && (Boolean) ISSHOW;
    }

    public void setISSHOW(Object ISSHOW) {
        this.ISSHOW = ISSHOW;
    }


}
