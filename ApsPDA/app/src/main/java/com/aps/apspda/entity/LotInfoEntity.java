package com.aps.apspda.entity;

import com.aps.apspda.utils.AppUtils;

import java.io.Serializable;

/**
 * @author lx
 * @date 2018/4/8
 * @desc
 */

public class LotInfoEntity implements Serializable {

    /**
     * RecipeId : M3CG3906--L1-D1-1
     * Materials : {"MesMaterial":[{"Qty":0,"ChName":"刀片编号","Type":"wafersaw","MaterialPartNo":"ZH05-SD3000-N1-50BB"},{"Qty":0,"ChName":"刀片编号","Type":"wafersaw","MaterialPartNo":"ZH05-SD3000-N1-50BBR1"},{"Qty":0,"ChName":"刀片编号","Type":"wafersaw","MaterialPartNo":"ZH05-SD3000-N1-50BBS0"}]}
     * Device : LMBT3906LT1G
     * CurrentStep : {}
     * <p>
     * Tablets : 0
     * IsCompareParameter : false
     * RecipeNo : EAP-SMD0022
     * TotalQty : 211756
     * Diagram : 88ASB02473L
     * IsStatus : false
     * WaferSourceVersion : {}
     * LeadFrame : {}
     * DiagramVersion : {}
     * OPID : G2976
     * ProcessStatus : UNKNOWN
     * EqpId : 1FS04
     * DoneTablets : 0
     * LotType : {}
     * Product : {}
     * IsProcessStart : false
     * DoneQty : 0
     * WaferSource : M3CG3906
     * LotId : EJ1WCR-021
     * LotStatus : Initialized
     * Carrier : {}
     * IsDownloadRecipe : false
     */

    private Object RecipeId;
    private MaterialsEntity Materials;
    private Object Device;
    private Object CurrentStep;
    private Object Tablets;
    private Object IsCompareParameter;
    private Object RecipeNo;
    private Object TotalQty;
    private Object Diagram;
    private Object IsStatus;
    private Object WaferSourceVersion;
    private Object LeadFrame;
    private Object DiagramVersion;
    private Object OPID;
    private Object ProcessStatus;
    private Object EqpId;
    private Object DoneTablets;
    private Object LotType;
    private Object Product;
    private Object IsProcessStart;
    private Object DoneQty;
    private Object WaferSource;
    private Object LotId;
    private Object LotStatus;
    private Object Carrier;
    private Object IsDownloadRecipe;
    private Object Block;
    private Object PIECENO;
    private Object Package;
    private DieSeqListEntity DIESEQLIST;

    public DieSeqListEntity getDIESEQLIST() {
        return DIESEQLIST == null ? new DieSeqListEntity() : DIESEQLIST;
    }

    public void setDIESEQLIST(DieSeqListEntity DIESEQLIST) {
        this.DIESEQLIST = DIESEQLIST;
    }

    public String getPackage() {
        return AppUtils.getFormatStr(Package);
    }

    public void setPackage(Object aPackage) {
        Package = aPackage;
    }

    public String getBlock() {
        return AppUtils.getFormatStr(Block);
    }

    public void setBlock(Object block) {
        Block = block;
    }

    public String getPIECENO() {
        return AppUtils.getFormatStr(PIECENO);
    }

    public void setPIECENO(Object PIECENO) {
        this.PIECENO = PIECENO;
    }


    public String getRecipeId() {
        return AppUtils.getFormatStr(RecipeId);
    }

    public void setRecipeId(Object recipeId) {
        RecipeId = recipeId;
    }

    public MaterialsEntity getMaterials() {
        return Materials == null ? new MaterialsEntity() : Materials;
    }

    public void setMaterials(MaterialsEntity materials) {
        Materials = materials;
    }

    public String getDevice() {
        return AppUtils.getFormatStr(Device);
    }

    public void setDevice(Object device) {
        Device = device;
    }

    public String getCurrentStep() {
        return AppUtils.getFormatStr(CurrentStep);
    }

    public void setCurrentStep(Object currentStep) {
        CurrentStep = currentStep;
    }

    public String getTablets() {
        return AppUtils.getFormatStr(Tablets);
    }

    public void setTablets(Object tablets) {
        Tablets = tablets;
    }

    public boolean getIsCompareParameter() {
        return IsCompareParameter != null && (Boolean) IsCompareParameter;
    }

    public void setIsCompareParameter(Object isCompareParameter) {
        IsCompareParameter = isCompareParameter;
    }

    public String getRecipeNo() {
        return AppUtils.getFormatStr(RecipeNo);
    }

    public void setRecipeNo(Object recipeNo) {
        RecipeNo = recipeNo;
    }

    public String getTotalQty() {
        return AppUtils.getFormatStr(TotalQty);
    }

    public void setTotalQty(Object totalQty) {
        TotalQty = totalQty;
    }

    public String getDiagram() {
        return AppUtils.getFormatStr(Diagram);
    }

    public void setDiagram(Object diagram) {
        Diagram = diagram;
    }

    public boolean getIsStatus() {
        return IsStatus != null && (Boolean) IsStatus;
    }

    public void setIsStatus(Object isStatus) {
        IsStatus = isStatus;
    }

    public String getWaferSourceVersion() {
        return AppUtils.getFormatStr(WaferSourceVersion);
    }

    public void setWaferSourceVersion(Object waferSourceVersion) {
        WaferSourceVersion = waferSourceVersion;
    }

    public String getLeadFrame() {
        return LeadFrame == null ? "" : String.valueOf(LeadFrame);
    }

    public void setLeadFrame(Object leadFrame) {
        LeadFrame = leadFrame;
    }

    public String getDiagramVersion() {
        return AppUtils.getFormatStr(DiagramVersion);
    }

    public void setDiagramVersion(Object diagramVersion) {
        DiagramVersion = diagramVersion;
    }

    public String getOPID() {
        return AppUtils.getFormatStr(OPID);
    }

    public void setOPID(Object OPID) {
        this.OPID = OPID;
    }

    public String getProcessStatus() {
        return ProcessStatus == null ? "" : String.valueOf(ProcessStatus);
    }

    public void setProcessStatus(Object processStatus) {
        ProcessStatus = processStatus;
    }

    public String getEqpId() {
        return AppUtils.getFormatStr(EqpId);
    }

    public void setEqpId(Object eqpId) {
        EqpId = eqpId;
    }

    public String getDoneTablets() {
        return AppUtils.getFormatStr(DoneTablets);
    }

    public void setDoneTablets(Object doneTablets) {
        DoneTablets = doneTablets;
    }

    public String getLotType() {
        return LotType == null ? "" : String.valueOf(LotType);
    }

    public void setLotType(Object lotType) {
        LotType = lotType;
    }

    public String getProduct() {
        return Product == null ? "" : String.valueOf(Product);
    }

    public void setProduct(Object product) {
        Product = product;
    }

    public boolean getIsProcessStart() {
        return IsProcessStart != null && (Boolean) IsProcessStart;
    }

    public void setIsProcessStart(Object isProcessStart) {
        IsProcessStart = isProcessStart;
    }

    public String getDoneQty() {
        return AppUtils.getFormatStr(DoneQty);
    }

    public void setDoneQty(Object doneQty) {
        DoneQty = doneQty;
    }

    public String getWaferSource() {
        return AppUtils.getFormatStr(WaferSource);
    }

    public void setWaferSource(Object waferSource) {
        WaferSource = waferSource;
    }

    public String getLotId() {
        return AppUtils.getFormatStr(LotId);
    }

    public void setLotId(Object lotId) {
        LotId = lotId;
    }

    public String getLotStatus() {
        return LotStatus == null ? "" : String.valueOf(LotStatus);
    }

    public void setLotStatus(Object lotStatus) {
        LotStatus = lotStatus;
    }

    public String getCarrier() {
        return Carrier == null ? "" : String.valueOf(Carrier);
    }

    public void setCarrier(Object carrier) {
        Carrier = carrier;
    }

    public boolean getIsDownloadRecipe() {
        return IsDownloadRecipe != null && (Boolean) IsDownloadRecipe;
    }

    public void setIsDownloadRecipe(Object isDownloadRecipe) {
        IsDownloadRecipe = isDownloadRecipe;
    }


}
