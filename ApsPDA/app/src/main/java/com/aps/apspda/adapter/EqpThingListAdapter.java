package com.aps.apspda.adapter;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Toast;

import com.aps.apspda.R;
import com.aps.apspda.activity.ThingChangeActivity;
import com.aps.apspda.callback.DeleteCallback;
import com.aps.apspda.dialog.DeleteFragmentDialog;
import com.aps.apspda.entity.MaterialInfoBean;
import com.aps.apspda.utils.AntiShake;
import com.aps.apspda.utils.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * @author
 * @date 2017/9/11
 * @desc
 */

public class EqpThingListAdapter extends BaseQuickAdapter<MaterialInfoBean, BaseViewHolder> {
    private boolean isShow = true;
    private DeleteCallback deleteCallback;
    private FragmentManager fragmentManager;

    public EqpThingListAdapter(@Nullable List<MaterialInfoBean> data, DeleteCallback deleteCallback) {
        super(R.layout.adapter_eqp_thing_list, data);
        this.deleteCallback = deleteCallback;
    }

    public void setIsShowButton(boolean isShow) {
        this.isShow = isShow;
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }


    @Override
    protected void convert(final BaseViewHolder helper, final MaterialInfoBean item) {
        helper.getView(R.id.btnChange).setVisibility(isShow ? View.VISIBLE : View.GONE);
        helper.getView(R.id.btnDelete).setVisibility(isShow ? View.VISIBLE : View.GONE);
        final String type = item.getType();
        if (type.equals("HubBlade") || type.equals("HubBladeYZ1") || type.equals("HubBladeYZ2")) {
            helper.getView(R.id.llHubBlade).setVisibility(View.VISIBLE);
            helper.getView(R.id.llWafer).setVisibility(View.GONE);
            helper.getView(R.id.llCapillary).setVisibility(View.GONE);
            helper.getView(R.id.llExpoxy).setVisibility(View.GONE);
            helper.getView(R.id.llHublessBlade).setVisibility(View.GONE);
            helper.getView(R.id.llWire).setVisibility(View.GONE);
            helper.getView(R.id.llBlock).setVisibility(View.GONE);
            if (type.equals("HubBlade")) {
                helper.setText(R.id.tvTitle, "硬刀");
            } else if (type.equals("HubBladeYZ1")) {
                helper.setText(R.id.tvTitle, "硬刀YZ1");
            } else if (type.equals("HubBladeYZ2")) {
                helper.setText(R.id.tvTitle, "硬刀YZ2");
            }
            helper.setText(R.id.tvHubBladeMaterialPartNo, item.getMaterialPartNo());
            helper.setText(R.id.tvHubBladeSawLength, item.getSawLength());
            helper.setText(R.id.tvHubBladeSeqNo, item.getSeqNo());
            helper.setText(R.id.tvHubBladeThickness, item.getSawThickness());
        } else if (type.equals("Wafer")) {
            helper.getView(R.id.llHubBlade).setVisibility(View.GONE);
            helper.getView(R.id.llWafer).setVisibility(View.VISIBLE);
            helper.getView(R.id.llCapillary).setVisibility(View.GONE);
            helper.getView(R.id.llExpoxy).setVisibility(View.GONE);
            helper.getView(R.id.llHublessBlade).setVisibility(View.GONE);
            helper.getView(R.id.llWire).setVisibility(View.GONE);
            helper.getView(R.id.llBlock).setVisibility(View.GONE);
            helper.setText(R.id.tvTitle, "晶圆");
            helper.setText(R.id.tvWaferDevice, item.getDevice());
            helper.setText(R.id.tvWaferWaferSource, item.getWaferSource());
            helper.setText(R.id.tvWaferDiagram, item.getDiagram());
            helper.setText(R.id.tvWaferMaterialPartNo, item.getMaterialPartNo());
        } else if (type.equals("Leadframe")) {
            helper.getView(R.id.llHubBlade).setVisibility(View.GONE);
            helper.getView(R.id.llWafer).setVisibility(View.GONE);
            helper.getView(R.id.llCapillary).setVisibility(View.GONE);
            helper.getView(R.id.llExpoxy).setVisibility(View.GONE);
            helper.getView(R.id.llHublessBlade).setVisibility(View.GONE);
            helper.getView(R.id.llWire).setVisibility(View.GONE);
            helper.getView(R.id.llBlock).setVisibility(View.GONE);
            helper.getView(R.id.llLeadframe).setVisibility(View.VISIBLE);
            helper.setText(R.id.tvTitle, "框架");
            helper.setText(R.id.tvLeadframeSeqNo, item.getSeqNo());
            helper.setText(R.id.tvLeadframeMaterialLotNo, item.getMaterialLotNo());
            helper.setText(R.id.tvLeadframeStartTime, item.getStartTime());
            helper.setText(R.id.tvLeadframeMaterialPartNo, item.getMaterialPartNo());
        } else if (type.equals("Capillary") || type.equals("Nozzle") ||
                type.equals("Thimble") || type.equals("StampingPin") || type.equals("DispenseTool")) {
            helper.getView(R.id.llHubBlade).setVisibility(View.GONE);
            helper.getView(R.id.llWafer).setVisibility(View.GONE);
            helper.getView(R.id.llCapillary).setVisibility(View.VISIBLE);
            helper.getView(R.id.llExpoxy).setVisibility(View.GONE);
            helper.getView(R.id.llHublessBlade).setVisibility(View.GONE);
            helper.getView(R.id.llWire).setVisibility(View.GONE);
            helper.getView(R.id.llBlock).setVisibility(View.GONE);
            helper.setText(R.id.tvCapillaryID, item.getSeqNo());
            helper.setText(R.id.tvCapillaryMaterialPartNo, item.getMaterialPartNo());
            switch (type) {
                case "Capillary":
                    helper.setText(R.id.tvTitle, "劈刀");
                    helper.setText(R.id.tvSameTitle1, "劈刀批次：");
                    helper.setText(R.id.tvSameTitle2, "劈刀PartNo：");
                    break;
                case "Nozzle":
                    helper.setText(R.id.tvTitle, "吸嘴");
                    helper.setText(R.id.tvSameTitle1, "吸嘴批次：");
                    helper.setText(R.id.tvSameTitle2, "吸嘴PartNo：");
                    break;
                case "Thimble":
                    helper.setText(R.id.tvTitle, "顶针");
                    helper.setText(R.id.tvSameTitle1, "顶针批次：");
                    helper.setText(R.id.tvSameTitle2, "顶针PartNo：");
                    break;
                case "StampingPin":
                    helper.setText(R.id.tvTitle, "蘸胶针");
                    helper.setText(R.id.tvSameTitle1, "蘸胶针批次：");
                    helper.setText(R.id.tvSameTitle2, "蘸胶针PartNo：");
                    break;
                case "DispenseTool":
                    helper.setText(R.id.tvTitle, "点胶头");
                    helper.setText(R.id.tvSameTitle1, "点胶头批次：");
                    helper.setText(R.id.tvSameTitle2, "点胶头PartNo：");
                    break;
            }
        } else if (type.equals("Epoxy")) {
            helper.getView(R.id.llHubBlade).setVisibility(View.GONE);
            helper.getView(R.id.llWafer).setVisibility(View.GONE);
            helper.getView(R.id.llCapillary).setVisibility(View.GONE);
            helper.getView(R.id.llExpoxy).setVisibility(View.VISIBLE);
            helper.getView(R.id.llHublessBlade).setVisibility(View.GONE);
            helper.getView(R.id.llWire).setVisibility(View.GONE);
            helper.getView(R.id.llBlock).setVisibility(View.GONE);
            helper.setText(R.id.tvTitle, "银浆");
            helper.setText(R.id.tvExpoxyMaterialPartNo, item.getMaterialPartNo());
            helper.setText(R.id.tvExpoxyThawEndTime, item.getThawEndTime());
            helper.setText(R.id.tvExpoxySeqNo, item.getSeqNo());
            helper.setText(R.id.tvExpoxyEndTime, item.getEndTime());
        } else if (type.equals("HublessBlade")) {
            helper.getView(R.id.llHubBlade).setVisibility(View.GONE);
            helper.getView(R.id.llWafer).setVisibility(View.GONE);
            helper.getView(R.id.llCapillary).setVisibility(View.GONE);
            helper.getView(R.id.llExpoxy).setVisibility(View.GONE);
            helper.getView(R.id.llHublessBlade).setVisibility(View.VISIBLE);
            helper.getView(R.id.llWire).setVisibility(View.GONE);
            helper.getView(R.id.llBlock).setVisibility(View.GONE);
            helper.setText(R.id.tvTitle, "软刀");
            helper.setText(R.id.tvHublessBladeID, item.getMaterialPartNo());
            helper.setText(R.id.tvHublessBladeExternalDiameter, item.getExternalDiameter());
            helper.setText(R.id.tvHublessBladeInternalDiameter, item.getInternalDiameter());
            helper.setText(R.id.tvHublessBladeThickness, item.getSawThickness());
            helper.setText(R.id.tvHublessBladeSeqNo, item.getSeqNo());
        } else if (type.equals("Wire")) {
            helper.getView(R.id.llHubBlade).setVisibility(View.GONE);
            helper.getView(R.id.llWafer).setVisibility(View.GONE);
            helper.getView(R.id.llCapillary).setVisibility(View.GONE);
            helper.getView(R.id.llExpoxy).setVisibility(View.GONE);
            helper.getView(R.id.llHublessBlade).setVisibility(View.GONE);
            helper.getView(R.id.llWire).setVisibility(View.VISIBLE);
            helper.getView(R.id.llBlock).setVisibility(View.GONE);
            helper.setText(R.id.tvTitle, "铜线");
            helper.setText(R.id.tvWireID, item.getSeqNo());
            helper.setText(R.id.tvWireMaterialPartNo, item.getMaterialPartNo());
            helper.setText(R.id.tvWireSize, item.getSize());
            helper.setText(R.id.tvWireTime, item.getRemainTime());
        } else if (type.equals("Block")) {
            helper.getView(R.id.llHubBlade).setVisibility(View.GONE);
            helper.getView(R.id.llWafer).setVisibility(View.GONE);
            helper.getView(R.id.llCapillary).setVisibility(View.GONE);
            helper.getView(R.id.llExpoxy).setVisibility(View.GONE);
            helper.getView(R.id.llHublessBlade).setVisibility(View.GONE);
            helper.getView(R.id.llWire).setVisibility(View.GONE);
            helper.getView(R.id.llBlock).setVisibility(View.VISIBLE);
            helper.setText(R.id.tvTitle, "BLOCK");
            helper.setText(R.id.tvBlockMaterialPartNo, item.getMaterialPartNo());
        }
        helper.getView(R.id.btnChange).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (AntiShake.check(v.getId())) {    //判断是否多次点击
//                    ToastUtils.showFreeToast("请勿重复点击",
//                            mContext, false, Toast.LENGTH_SHORT);
//                    return;
//                }
                Intent intent = new Intent(mContext, ThingChangeActivity.class);
                intent.putExtra("mEntity", item);
                intent.putExtra("eqpID", item.getEqpID());
                mContext.startActivity(intent);
            }
        });

        helper.getView(R.id.btnDelete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AntiShake.check(v.getId())) {    //判断是否多次点击
                    ToastUtils.showFreeToast("请勿重复点击",
                            mContext, false, Toast.LENGTH_SHORT);
                    return;
                }
                final DeleteFragmentDialog deleteFragmentDialog = new DeleteFragmentDialog();
                deleteFragmentDialog.setData("确定要删除吗？", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (AntiShake.check(v.getId())) {    //判断是否多次点击
                            ToastUtils.showFreeToast("请勿重复点击",
                                    mContext, false, Toast.LENGTH_SHORT);
                            return;
                        }
                        deleteFragmentDialog.dismiss();
                        deleteCallback.onSuccess(item);
                    }
                });
                deleteFragmentDialog.show(fragmentManager, "delete_dialog");
            }
        });
    }
}
