package com.aps.apspda.activity;

import android.os.Bundle;
import android.view.View;

import com.aps.apspda.base.BaseActivity;
import com.aps.apspda.dialog.WarnFragmentDialog;

public class DialogActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutResId() {
        return 0;
    }

    @Override
    protected void initView() {
        int hour = getIntent().getIntExtra("hour", 0);
        final WarnFragmentDialog dialog = new WarnFragmentDialog();
        dialog.setData("系统将在 " + hour + "点15分进行重新登录操作，请尽快完成当前工作");
        dialog.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                DialogActivity.this.finish();
            }
        });
        dialog.show(getSupportFragmentManager(), "time_out_tips");
    }

    @Override
    protected void initData() {

    }
}
