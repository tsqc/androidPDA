package com.aps.apspda.myview;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aps.apspda.R;


/**
 * @author jaxhuang
 * @date 2017/9/19
 * @desc
 */

public class TitleView extends RelativeLayout {
    public final static int NORMAL_MODE = 1;
    public final static int NORMAL_TEXT_MODE = 2;
    public final static int NORMAL_IMAGE_MODE = 3;
    public final static int NORMAL_WITH_DOWN_MODE = 4;
    public final static int PURE_MODE = 5;
    public final static int RIGHT_TEXT_NO_BACK = 6;
    public final static int RIGHT_IMG_NO_BACK = 7;

    private ImageView ivBack, ivRight, ivArrow;
    private TextView tvTitle, tvArrowTitle;
    private LinearLayout llTitle;
    private Button tvRight;
    private int current_mode = NORMAL_MODE;

    public TitleView(Context context) {
        super(context);
        initView(context);
    }

    private void initView(final Context context) {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        RelativeLayout mContainer = (RelativeLayout) LayoutInflater.from(context).inflate(
                R.layout.top_title, null);
        addView(mContainer, lp);
        tvTitle = findViewById(R.id.tvTitle);
        tvRight = findViewById(R.id.tvRight);
        tvArrowTitle = findViewById(R.id.tvArrowTitle);
        ivBack = findViewById(R.id.ivBack);
        ivRight = findViewById(R.id.ivRight);
        ivArrow = findViewById(R.id.ivArrow);
        llTitle = findViewById(R.id.llTitle);
        ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity) context).finish();
            }
        });
        setTitleMode(current_mode);
    }

    public void setTitleMode(int mode) {
        current_mode = mode;
        switch (mode) {
            case NORMAL_MODE:
                tvTitle.setVisibility(View.VISIBLE);
                ivBack.setVisibility(View.VISIBLE);
                llTitle.setVisibility(View.GONE);
                ivRight.setVisibility(View.GONE);
                tvRight.setVisibility(View.GONE);
                break;
            case NORMAL_TEXT_MODE:
                tvTitle.setVisibility(View.VISIBLE);
                ivBack.setVisibility(View.VISIBLE);
                llTitle.setVisibility(View.GONE);
                ivRight.setVisibility(View.GONE);
                tvRight.setVisibility(View.VISIBLE);
                break;
            case NORMAL_IMAGE_MODE:
                tvTitle.setVisibility(View.VISIBLE);
                ivBack.setVisibility(View.VISIBLE);
                llTitle.setVisibility(View.GONE);
                ivRight.setVisibility(View.VISIBLE);
                tvRight.setVisibility(View.GONE);
                break;
            case NORMAL_WITH_DOWN_MODE:
                tvTitle.setVisibility(View.GONE);
                ivBack.setVisibility(View.VISIBLE);
                llTitle.setVisibility(View.VISIBLE);
                ivRight.setVisibility(View.GONE);
                tvRight.setVisibility(View.GONE);
                break;
            case PURE_MODE:
                tvTitle.setVisibility(View.VISIBLE);
                ivBack.setVisibility(View.GONE);
                llTitle.setVisibility(View.GONE);
                ivRight.setVisibility(View.GONE);
                tvRight.setVisibility(View.GONE);
                break;
            case RIGHT_TEXT_NO_BACK:
                tvTitle.setVisibility(View.VISIBLE);
                ivBack.setVisibility(View.GONE);
                llTitle.setVisibility(View.GONE);
                ivRight.setVisibility(View.GONE);
                tvRight.setVisibility(View.VISIBLE);
                break;
            case RIGHT_IMG_NO_BACK:
                tvTitle.setVisibility(View.VISIBLE);
                ivBack.setVisibility(View.GONE);
                llTitle.setVisibility(View.GONE);
                ivRight.setVisibility(View.VISIBLE);
                tvRight.setVisibility(View.GONE);
                break;

        }
    }

    public TitleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    public void setTitle(String title, OnClickListener listener) {
        tvArrowTitle.setText(title);
        llTitle.setOnClickListener(listener);
    }

    public void setLeftListener(OnClickListener listener) {
        ivBack.setOnClickListener(listener);
    }

    public void setRightListener(String title, OnClickListener listener) {
        if (title.length() > 0) {
            tvRight.setVisibility(VISIBLE);
            tvRight.setText(title);
            tvRight.setOnClickListener(listener);
        } else {
            tvRight.setVisibility(GONE);
        }
    }

    public void setRightListener(int resID, OnClickListener listener) {
        ivRight.setImageResource(resID);
        ivRight.setOnClickListener(listener);
    }
}
