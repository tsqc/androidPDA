package com.aps.apspda.activity;

import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aps.apspda.R;
import com.aps.apspda.base.BaseActivity;
import com.aps.apspda.callback.DialogEntityCallBack;
import com.aps.apspda.callback.EntityCallBack;
import com.aps.apspda.entity.BaseEntity;
import com.aps.apspda.entity.CurrentEqpEntity;
import com.aps.apspda.myview.ClearEditText;
import com.aps.apspda.myview.TitleView;
import com.aps.apspda.utils.AntiShake;
import com.aps.apspda.utils.NetUtils;
import com.aps.apspda.utils.StaticMembers;
import com.aps.apspda.utils.ToastUtils;
import com.google.gson.reflect.TypeToken;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

public class SearchInfoActivity extends BaseActivity {


    @BindView(R.id.topView)
    TitleView topView;
    @BindView(R.id.etRemark)
    ClearEditText etRemark;
    @BindView(R.id.tvStatus)
    TextView tvStatus;
    @BindView(R.id.tvStartNum)
    TextView tvStartNum;
    @BindView(R.id.tvDoneNum)
    TextView tvDoneNum;
    @BindView(R.id.tvPerson)
    TextView tvPerson;
    @BindView(R.id.tvEqp)
    TextView tvEqp;
    @BindView(R.id.ivRight)
    ImageView ivRight;
    @BindView(R.id.tvPic)
    TextView tvPic;
    @BindView(R.id.tvPicVersion)
    TextView tvPicVersion;
    @BindView(R.id.rlPic)
    RelativeLayout rlPic;
    @BindView(R.id.llContent)
    LinearLayout llContent;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_search_info;
    }

    @Override
    protected void initView() {
        topView.setTitle("批次信息(" + StaticMembers.CUR_USER.getUSERNAME() + ")");
        etRemark.requestFocus();

        etRemark.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER);
            }
        });
        etRemark.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (start == 0 && before == 0 && count > 1) {
                    if (s.length() > 0) {
                        requestData(s.toString());
                    } else {
                        ToastUtils.showFreeToast("扫描信息有误", SearchInfoActivity.this, false, Toast.LENGTH_SHORT);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    protected void initData() {
        setCallback(etRemark);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        OkGo.getInstance().cancelTag(this);
    }

    private void requestData(String lotID) {
        EntityCallBack<BaseEntity<CurrentEqpEntity>> callBack = new DialogEntityCallBack<BaseEntity<CurrentEqpEntity>>
                (new TypeToken<BaseEntity<CurrentEqpEntity>>() {
                }.getType(), getSupportFragmentManager(), this) {

            @Override
            public void onSuccess
                    (final Response<BaseEntity<CurrentEqpEntity>> response) {

                if (response.body().isSuccess(SearchInfoActivity.this)) {
                    llContent.setVisibility(View.VISIBLE);
                    final CurrentEqpEntity currentEqpEntity = response.body().getData();
                    tvEqp.setText(currentEqpEntity.getEqpId());
                    tvStartNum.setText(currentEqpEntity.getTotalQty());
                    tvDoneNum.setText(currentEqpEntity.getDoneQty());
                    tvPerson.setText(currentEqpEntity.getOPID());
                    tvStatus.setText(currentEqpEntity.getLotStatus());
                    rlPic.setVisibility(currentEqpEntity.getDiagram().length() > 0
                            && currentEqpEntity.getDiagramVersion().length() > 0 ? View.VISIBLE : View.GONE);
                    tvPic.setText(currentEqpEntity.getDiagram());
                    tvPicVersion.setText(currentEqpEntity.getDiagramVersion());
                    rlPic.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (AntiShake.check(v.getId())) {    //判断是否多次点击
                                ToastUtils.showFreeToast("请勿重复点击",
                                        SearchInfoActivity.this, false, Toast.LENGTH_SHORT);
                                return;
                            }
                            Intent intent = new Intent(SearchInfoActivity.this, PicViewActivity.class);
                            intent.putExtra("pic", currentEqpEntity.getDiagram());
                            intent.putExtra("picVersion", currentEqpEntity.getDiagramVersion());
                            startActivity(intent);
                        }
                    });
                } else {
                    llContent.setVisibility(View.VISIBLE);
                    ToastUtils.showFreeToast(response.body().getMessage(),
                            SearchInfoActivity.this, false, Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onError
                    (Response<BaseEntity<CurrentEqpEntity>> response) {
                super.onError(response);
                loadError(response.getException(), "SearchLotInfo");
            }
        };

        Map<String, String> map = new HashMap<>();
        map.put("LotId", lotID);
        NetUtils.requestNet(this, "/SearchLotInfo", map, callBack);
    }
}
