package com.aps.apspda.adapter;

import android.support.annotation.Nullable;

import com.aps.apspda.R;
import com.aps.apspda.entity.MaterialsEntity;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * @author
 * @date 2017/9/11
 * @desc
 */

public class ThingListAdapter extends BaseQuickAdapter<MaterialsEntity.MesMaterialBean, BaseViewHolder> {

    public ThingListAdapter(@Nullable List<MaterialsEntity.MesMaterialBean> data) {
        super(R.layout.adapter_thing_list, data);
    }


    @Override
    protected void convert(BaseViewHolder helper, MaterialsEntity.MesMaterialBean item) {
        helper.setText(R.id.tvTitle, item.getChName());
        helper.setText(R.id.tvContent, item.getMaterialPartNo());
    }
}
