package com.aps.apspda.myview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.aps.apspda.R;
import com.aps.apspda.utils.DisplayUtil;
import com.aps.apspda.utils.NumberUtils;

import java.util.List;

/**
 * @author jaxhuang
 * @date 2017/11/28
 * @desc
 */

public class CirclePercentView extends View {
    //分配比例大小，总比例大小为100,由于经过运算后最后会是99.55左右的数值，导致圆不能够重合，会留出点空白，所以这里的总比例大小我们用101
    private List<Double> strPercent;
    //圆的直径
    private float mRadius;
    //文字大小
    private int bigTextSize = 14;
    private int smallTextSize = 14;
    private int textMargin = 8;
    //外圈直径
    private int bigCircle = 160;
    //内圈直径
    private int smallCircle = 100;
    //圆环的画笔
    private Paint percentPaint;
    //圆的画笔
    private Paint cyclePaint;
    //文字的画笔
    private Paint textPaint1, textPaint2;
    //边框颜色和标注颜色
    private List<Integer> mColor;
    //View自身的宽和高
    private int mHeight;
    private int mWidth;
    private String total;


    public CirclePercentView(Context context) {
        super(context);
    }

    public CirclePercentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CirclePercentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setData(List<Double> strPercent, List<Integer> mColor, String total) {
        this.strPercent = strPercent;
        this.mColor = mColor;
        this.total = total;
        invalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
        mRadius = DisplayUtil.dip2px(bigCircle);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (strPercent != null && strPercent.size() > 0) {
            //移动画布到圆环的左上角
            canvas.translate(mWidth / 2 - mRadius / 2, mHeight / 2 - mRadius / 2);
            //初始化画笔
            initPaint();
            //画圆环
            drawView(canvas);
        }
    }

    /**
     * 初始化画笔
     */
    private void initPaint() {
        //圆环画笔
        percentPaint = new Paint();
        percentPaint.setAntiAlias(true);
        percentPaint.setStrokeWidth(1);
        //圆画笔
        cyclePaint = new Paint();
        cyclePaint.setAntiAlias(true);
        cyclePaint.setColor(getResources().getColor(R.color.theme_black));
        cyclePaint.setStyle(Paint.Style.FILL);
        cyclePaint.setStrokeWidth(1);
        //文字画笔
        textPaint1 = new Paint();
        textPaint1.setAntiAlias(true);
        textPaint1.setStyle(Paint.Style.FILL);
        textPaint1.setStrokeWidth(1);
        textPaint1.setColor(getResources().getColor(R.color.theme_gold));
        textPaint1.setTextSize(DisplayUtil.dip2px(bigTextSize));

        textPaint2 = new Paint();
        textPaint2.setAntiAlias(true);
        textPaint2.setStyle(Paint.Style.FILL);
        textPaint2.setStrokeWidth(1);
        textPaint2.setColor(getResources().getColor(R.color.theme_gold));
        textPaint2.setTextSize(DisplayUtil.dip2px(smallTextSize));
    }

    /**
     * 画圆环
     *
     * @param canvas
     */
    private void drawView(Canvas canvas) {
        //画扇形
        RectF oval = new RectF(0, 0, mRadius, mRadius);
        if (NumberUtils.toDouble(total) == 0) {
            percentPaint.setColor(getResources().getColor(R.color.theme_black));
            canvas.drawArc(oval, 0, 360, true, percentPaint);
        } else {
            float startPercent = -90;
            float sweepPercent = 0;
            for (int i = 0; i < strPercent.size(); i++) {
                percentPaint.setColor(mColor.get(i));
                startPercent = sweepPercent + startPercent;
                //这里采用比例占100的百分比乘于360的来计算出占用的角度，使用先乘再除可以算出值
                sweepPercent = Math.round(strPercent.get(i) * 360f / NumberUtils.toDouble(total));
                canvas.drawArc(oval, startPercent, sweepPercent, true, percentPaint);
            }
        }
        //画中心白色的圆
        canvas.drawCircle(oval.right / 2, oval.bottom / 2, DisplayUtil.dip2px(smallCircle / 2), cyclePaint);
        //画文字
        Rect mBound = new Rect();
        float startX;
        float startY;
        String ss1  = strPercent.get(0)+"%";
        textPaint1.getTextBounds(ss1, 0, ss1.length(), mBound);
        startX = oval.right / 2 - mBound.width() / 2;
        startY = oval.bottom / 2 + mBound.height() / 2;
        canvas.drawText(ss1, startX, startY - DisplayUtil.dip2px(textMargin), textPaint1);

        String ss2 = "完成率";
        textPaint2.getTextBounds(ss2, 0, ss2.length(), mBound);
        startX = oval.right / 2 - mBound.width() / 2;
        startY = oval.bottom / 2 + mBound.height() / 2;
        canvas.drawText(ss2, startX, startY + DisplayUtil.dip2px(textMargin), textPaint2);
    }
}
