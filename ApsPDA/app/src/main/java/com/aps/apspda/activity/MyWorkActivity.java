package com.aps.apspda.activity;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.aps.apspda.R;
import com.aps.apspda.adapter.MyWorkListAdapter;
import com.aps.apspda.base.BaseActivity;
import com.aps.apspda.callback.DialogEntityCallBack;
import com.aps.apspda.callback.EntityCallBack;
import com.aps.apspda.entity.BaseEntity;
import com.aps.apspda.entity.MyWorkEntity;
import com.aps.apspda.myview.TitleView;
import com.aps.apspda.utils.NetUtils;
import com.aps.apspda.utils.StaticMembers;
import com.aps.apspda.utils.ToastUtils;
import com.google.gson.reflect.TypeToken;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

public class MyWorkActivity extends BaseActivity {

    @BindView(R.id.topView)
    TitleView topView;
    @BindView(R.id.rvWorkList)
    RecyclerView rvWorkList;

    private MyWorkListAdapter myWorkListAdapter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_my_work;
    }

    @Override
    protected void initView() {
        topView.setTitle("作业量汇总(" + StaticMembers.CUR_USER.getUSERNAME() + ")");
        rvWorkList.setLayoutManager(new LinearLayoutManager(MyWorkActivity.this));
        rvWorkList.setItemAnimator(new DefaultItemAnimator());
        rvWorkList.addItemDecoration(new DividerItemDecoration(MyWorkActivity.this, 1));
    }

    @Override
    protected void initData() {
        requestData();
    }

    private void requestData() {
        EntityCallBack<BaseEntity<MyWorkEntity>> callBack = new DialogEntityCallBack<BaseEntity<MyWorkEntity>>
                (new TypeToken<BaseEntity<MyWorkEntity>>() {
                }.getType(), getSupportFragmentManager(), this) {

            @Override
            public void onSuccess
                    (final Response<BaseEntity<MyWorkEntity>> response) {
                if (response.body().isSuccess(MyWorkActivity.this)) {
                    myWorkListAdapter = new MyWorkListAdapter(response.body().getData().getSendQtyList().getSendQty());
                    rvWorkList.setAdapter(myWorkListAdapter);
                } else {
                    ToastUtils.showFreeToast(response.body().getMessage(),
                            MyWorkActivity.this, false, Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onError
                    (Response<BaseEntity<MyWorkEntity>> response) {
                super.onError(response);
                loadError(response.getException(), "SearchSelfOutPut");
            }
        };

        Map<String, String> map = new HashMap<>();
        map.put("USERNAME", StaticMembers.CUR_USER.getUSERNAME());
        NetUtils.requestNet(this, "/SearchSelfOutPut", map, callBack);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        OkGo.getInstance().cancelTag(this);
    }
}
