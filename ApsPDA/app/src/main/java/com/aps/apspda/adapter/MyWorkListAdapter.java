package com.aps.apspda.adapter;

import android.support.annotation.Nullable;

import com.aps.apspda.R;
import com.aps.apspda.entity.MyWorkEntity;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * @author
 * @date 2017/9/11
 * @desc
 */

public class MyWorkListAdapter extends BaseQuickAdapter<MyWorkEntity.SendQtyListBean.SendQtyBean, BaseViewHolder> {

    public MyWorkListAdapter(@Nullable List<MyWorkEntity.SendQtyListBean.SendQtyBean> data) {
        super(R.layout.adapter_my_work_list, data);
    }


    @Override
    protected void convert(BaseViewHolder helper, MyWorkEntity.SendQtyListBean.SendQtyBean item) {
        helper.setText(R.id.tvEqp, item.getEQPID());
        helper.setText(R.id.tvWork, item.getDoneQty());
    }
}
