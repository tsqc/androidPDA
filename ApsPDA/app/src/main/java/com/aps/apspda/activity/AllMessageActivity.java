package com.aps.apspda.activity;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import com.aps.apspda.R;
import com.aps.apspda.adapter.MessageListAdapter;
import com.aps.apspda.base.BaseActivity;
import com.aps.apspda.callback.DialogEntityCallBack;
import com.aps.apspda.callback.EntityCallBack;
import com.aps.apspda.entity.BaseEntity;
import com.aps.apspda.entity.MessageEntity;
import com.aps.apspda.myview.TitleView;
import com.aps.apspda.utils.NetUtils;
import com.aps.apspda.utils.StaticMembers;
import com.aps.apspda.utils.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.reflect.TypeToken;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

public class AllMessageActivity extends BaseActivity {


    @BindView(R.id.topView)
    TitleView topView;
    @BindView(R.id.tvEqpId)
    TextView tvEqpId;
    @BindView(R.id.rvMessageList)
    RecyclerView rvMessageList;

    private MessageListAdapter messageListAdapter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_all_message;
    }

    @Override
    protected void initView() {
        tvEqpId.setText(getEqpID());
        topView.setTitle("实时消息(" + StaticMembers.CUR_USER.getUSERNAME() + ")");
        rvMessageList.setLayoutManager(new LinearLayoutManager(AllMessageActivity.this));
        rvMessageList.setItemAnimator(new DefaultItemAnimator());
        rvMessageList.addItemDecoration(new DividerItemDecoration(AllMessageActivity.this, 1));
    }

    @Override
    protected void initData() {
        requestMessageData();
    }

    private void requestMessageData() {
        EntityCallBack<BaseEntity<List<MessageEntity>>> callBack = new DialogEntityCallBack<BaseEntity<List<MessageEntity>>>(
                (new TypeToken<BaseEntity<List<MessageEntity>>>() {
        }.getType()), getSupportFragmentManager(), this) {

            @Override
            public void onSuccess
                    (final Response<BaseEntity<List<MessageEntity>>> response) {

                if (response.body().isSuccess(AllMessageActivity.this)) {
                    messageListAdapter = new MessageListAdapter(response.body().getData());
                    messageListAdapter.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
                    messageListAdapter.isFirstOnly(true);
                    rvMessageList.setAdapter(messageListAdapter);
                } else {
                    ToastUtils.showFreeToast(response.body().getMessage(),
                            AllMessageActivity.this, false, Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onError
                    (Response<BaseEntity<List<MessageEntity>>> response) {
                super.onError(response);
                loadError(response.getException(), "SHOWMSG2PDA");
            }
        };

        Map<String, String> map = new HashMap<>();
        map.put("EQPID", getEqpID());
        map.put("number", "30");
        NetUtils.requestNet(this, "/SHOWMSG2PDA", map, callBack);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        OkGo.getInstance().cancelTag(this);
    }
}
