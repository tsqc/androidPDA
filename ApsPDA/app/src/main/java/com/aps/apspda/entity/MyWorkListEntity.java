package com.aps.apspda.entity;

import java.io.Serializable;

/**
 * @author lx
 * @date 2018/4/18
 * @desc
 */

public class MyWorkListEntity implements Serializable {
    private MyWorkEntity SendQtyList;

    public MyWorkEntity getSendQtyList() {
        return SendQtyList == null ? new MyWorkEntity() : SendQtyList;
    }

    public void setMaterials(MyWorkEntity materials) {
        SendQtyList = materials;
    }
}
