package com.aps.apspda.activity;

import android.os.Handler;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import com.aps.apspda.R;
import com.aps.apspda.adapter.EqpThingListAdapter;
import com.aps.apspda.base.BaseActivity;
import com.aps.apspda.callback.DeleteCallback;
import com.aps.apspda.callback.DialogEntityCallBack;
import com.aps.apspda.callback.EntityCallBack;
import com.aps.apspda.entity.BaseEntity;
import com.aps.apspda.entity.FinishLotEntity;
import com.aps.apspda.entity.MaterialInfo2Entity;
import com.aps.apspda.entity.MaterialInfoBean;
import com.aps.apspda.myview.TitleView;
import com.aps.apspda.utils.NetUtils;
import com.aps.apspda.utils.StaticMembers;
import com.aps.apspda.utils.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.reflect.TypeToken;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Response;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

public class ThingChangeListActivity extends BaseActivity {

    @BindView(R.id.topView)
    TitleView topView;
    @BindView(R.id.tvEqpId)
    TextView tvEqpId;
    @BindView(R.id.rvEqpList)
    RecyclerView rvEqpList;

    private EqpThingListAdapter eqpThingListAdapter;
    private String eid;
    private Handler mHandler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            ThingChangeListActivity.this.finish();
        }
    };

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_thing_change_list;
    }

    @Override
    protected void initView() {
        topView.setTitle("材料列表(" + StaticMembers.CUR_USER.getUSERNAME() + ")");
        rvEqpList.setLayoutManager(new LinearLayoutManager(ThingChangeListActivity.this));
        rvEqpList.setItemAnimator(new DefaultItemAnimator());
        rvEqpList.addItemDecoration(new DividerItemDecoration(ThingChangeListActivity.this, 1));
    }

    @Override
    protected void initData() {
        eid = getIntent().getStringExtra("eqpID");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacks(runnable);
        OkGo.getInstance().cancelTag(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestData();
    }

    private void requestData() {
        EntityCallBack<BaseEntity<MaterialInfo2Entity>> callBack = new DialogEntityCallBack<BaseEntity<MaterialInfo2Entity>>
                (new TypeToken<BaseEntity<MaterialInfo2Entity>>() {
                }.getType(), getSupportFragmentManager(), this) {

            @Override
            public void onSuccess
                    (final Response<BaseEntity<MaterialInfo2Entity>> response) {

                if (response.body().isSuccess(ThingChangeListActivity.this)) {
                    tvEqpId.setText(response.body().getData().getEqpId());
                    if (response.body().getData().getMaterials().getMesMaterial().size() == 0) {
                        ToastUtils.showFreeToast("该设备没有设置BOM,请联系相关人员",
                                ThingChangeListActivity.this, false, Toast.LENGTH_SHORT);
                    }
                    eqpThingListAdapter = new EqpThingListAdapter(response.body().getData().getMaterials().getMesMaterial(), new DeleteCallback() {
                        @Override
                        public void onSuccess(MaterialInfoBean bean) {
                            final EntityCallBack<BaseEntity<FinishLotEntity>> callBack = new DialogEntityCallBack<BaseEntity<FinishLotEntity>>
                                    (new TypeToken<BaseEntity<FinishLotEntity>>() {
                                    }.getType(), getSupportFragmentManager(), this) {

                                @Override
                                public void onSuccess
                                        (final Response<BaseEntity<FinishLotEntity>> response) {
                                    if (response.body().isSuccess(ThingChangeListActivity.this)) {
                                        ToastUtils.showFreeToast("删除物料成功",
                                                ThingChangeListActivity.this, true, Toast.LENGTH_SHORT);
                                        requestData();
                                    } else {
                                        ToastUtils.showFreeToast(response.body().getMessage(),
                                                ThingChangeListActivity.this, false, Toast.LENGTH_SHORT);
                                    }
                                }

                                @Override
                                public void onError
                                        (Response<BaseEntity<FinishLotEntity>> response) {
                                    super.onError(response);
                                    loadError(response.getException(), "UnloadEQPMeterial");
                                }
                            };

                            Map<String, String> map = new HashMap<>();
                            map.put("EqpId", getEqpID());
                            map.put("MaterialPartNo", bean.getMaterialPartNo());
                            map.put("OPID", StaticMembers.CUR_USER.getUSERNAME());
                            map.put("Type", bean.getType());
                            map.put("SeqNo", bean.getSeqNo());
                            NetUtils.requestNet(this, "/UnloadEQPMeterial", map, callBack);
                        }
                    });
                    eqpThingListAdapter.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
                    eqpThingListAdapter.setFragmentManager(getSupportFragmentManager());
                    eqpThingListAdapter.isFirstOnly(true);
                    rvEqpList.setAdapter(eqpThingListAdapter);
                } else {
                    ToastUtils.showFreeToast(response.body().getMessage(),
                            ThingChangeListActivity.this, false, Toast.LENGTH_SHORT);
                    mHandler.postDelayed(runnable, 1000);
                }
            }

            @Override
            public void onError
                    (Response<BaseEntity<MaterialInfo2Entity>> response) {
                super.onError(response);
                loadError(response.getException(), "ShowMeterial");
                mHandler.postDelayed(runnable, 1000);
            }
        };

        Map<String, String> map = new HashMap<>();
        map.put("EqpId", eid);
        NetUtils.requestNet(this, "/ShowMeterial", map, callBack);
    }
}
